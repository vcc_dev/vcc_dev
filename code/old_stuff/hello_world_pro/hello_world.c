//Copyright (C) 2012 Brundo Salvatore
//salvo85[at]gmail[dot]com
//http://www.brundo.org

/*
hello_world.c : basic hello world atmega8

Copyright (C) 2012 Brundo Salvatore
salvo85[at]gmail[dot]com
http://www.brundo.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <avr/io.h>
#define F_CPU 1000000 // 1 Mhz is default frequency clock of atmega168
#include <util/delay.h>

#include <avr/io.h>
#include <avr/interrupt.h>

int main (void){
	DDRD = 0xFF;			// set up pin directions 
	TCCR1B = (1<<CS10);	// set prescaler to /1 
  
// set interrupt mask register 
//TIMSK1|=(1<<TOIE1);	// mega48 88 or 168 to enable overflow interrupt
	TIMSK|=(1<<TOIE1);  // mega32 or mega8 to enable overflow interrupt

	sei();					// turn interrupts on

	while(1);				// chase tail

}

ISR(TIMER1_OVF_vect)
{
	PORTD ^= 0xFF;		//toggle portB
}
