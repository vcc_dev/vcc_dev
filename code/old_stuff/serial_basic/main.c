#include <avr/io.h>

#define F_CPU 1000000


#define led_init() DDRD = 0b01000000;
#define led_on() PORTD = 0b01000000;
#define led_off() PORTD = 0b00000000;


#define USART_BAUDRATE 1200 // VERY SLOW WITHOUT CRYSTAL!
#define BAUD_PRESCALE (((F_CPU / (USART_BAUDRATE * 16UL))) - 1)
#define cr() serial_tx('\n');
void serial_loopback(); // simple serial loopback to test connections
unsigned char serial_rx();
void serial_tx(unsigned char tx_byte);

void delay_ms(uint16_t x); // general purpose delay

int main (void)
{
   led_init();
   serial_init();
   
   for (;;) // Loop forever
   {
     led_on();
     serial_tx('C');
     //~ delay_ms(5);
     serial_tx('i');
     //~ delay_ms(5);
     serial_tx('a');
     //~ delay_ms(5);
     serial_tx('o');
     //~ delay_ms(5);
     serial_tx('\n');
     led_off();
     delay_ms(5);
     
     serial_loopback();
     serial_tx('\n');
     cr();
     delay_ms(5);
     
   }   
} 


//General short delays
void delay_ms(uint16_t x)
{
  uint8_t y, z;
  for ( ; x > 0 ; x--){
    for ( y = 0 ; y < 80 ; y++){
      for ( z = 0 ; z < 40 ; z++){
        asm volatile ("nop");
      }
    }
  }
}

void serial_init()
{
  UCSRB |= (1 << RXEN) | (1 << TXEN);   // Turn on the transmission and reception circuitry
  UCSRC |= (1 << URSEL) | (1 << UCSZ0) | (1 << UCSZ1); // Use 8-bit character sizes

  UBRRH = (BAUD_PRESCALE >> 8); // Load upper 8-bits of the baud rate value into the high byte of the UBRR register
  UBRRL = BAUD_PRESCALE; // Load lower 8-bits of the baud rate value into the low byte of the UBRR register

}




void serial_loopback()
{
  char rx_byte;
  while ((UCSRA & (1 << RXC)) == 0) {}; // Do nothing until data have been received and is ready to be read from UDR
  rx_byte = UDR; // Fetch the received byte value into the variable "ByteReceived"
      
  while ((UCSRA & (1 << UDRE)) == 0) {}; // Do nothing until UDR is ready for more data to be written to it
  UDR = rx_byte;
  
}

void serial_tx(unsigned char tx_byte)
{
  while ((UCSRA & (1 << UDRE)) == 0) {}; // Do nothing until UDR is ready for more data to be written to it
  UDR = tx_byte;
}

unsigned char serial_rx()
{
  unsigned char rx_byte;
  while ((UCSRA & (1 << RXC)) == 0) {}; // Do nothing until data have been received and is ready to be read from UDR
  rx_byte = UDR;
  return rx_byte;
}
