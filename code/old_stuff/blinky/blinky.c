// blinky.c : led blinking like kitt in supercar (aka knight rider)
// for more semplicity i'm going to use a 7-segment display
// hopefully this software is going to create a loop on the external segments (0)
// A -> B -> C -> D -> E -> F -> E -> D -> C -> B -> A  

// 7-segment map:

//    ____A____
//   |         |
//   F         B
//   |____G____|  
//   |         | 
//   E         C
//   |____D____| .dp
   
//Copyright (C) 2008 Brundo Salvatore
//salvo85[at]gmail[dot]com
//http://www.brundo.org

/*
blinky.c : basic 7-segment blinking with atmega168

Copyright (C) 2008 Brundo Salvatore
salvo85[at]gmail[dot]com
http://www.brundo.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


 /*
 7-segment map:

    ____A____
   |         |
   F         B
   |____G____|  
   |         | 
   E         C
   |____D____| .dp
   
   
   pins for HD1133R:
   
   G  F  K  A  B
   |__|__|__|__|
   |    __     |
   |   |__|    | 
   |   |__|.   |
   |           |
   |__ __ __ __|
   |  |  |  |  |
   E  D  x  C  dp
   
   x means that pin isn't connected to anything..
   K means Katode
   
   Collegamenti:
   
   G--28 PC5
   F--27 PC4
   A--26 PC3
   B--25 PC2

   E---2 PD0
   D---3 PD1
   C---4 PD2
   dp--5 PD3

   Eventuali 4 npn:
   D0--PB4
   D1--PB3
   D2--PB2
   D3--PB1

*/




#include <avr/io.h>

#define F_CPU 1000000 // 1 Mhz is default frequency clock of atmega168
#include <util/delay.h>


//DDRx Data Direction Register for port x




int main(){
	
	DDRB = 0xFF;
	DDRC = 0xFF;
	DDRD = 0xFF;
	
	int i=5;
  while(i){

		PORTC=0b00111100;
		PORTD=0b00001111;
		_delay_ms(500);
		
		PORTC=0b00000000;
		PORTD=0b00000000;
		_delay_ms(500);
		
		i--;

  }

	i=5;
	int rotation_delay=500;

	while(i){
		//PORTC=0bxxGFABxx
		//PORTD=0bxxxxdCDE
		//
		// rotazione:
		// A
		PORTC=0b00001000;
		_delay_ms(rotation_delay);
	
		// B
		PORTC=0b00000100;
		_delay_ms(rotation_delay);
	
		// C
		PORTC=0b00000000;
		PORTD=0b00000100;
		_delay_ms(rotation_delay);
	
		// D
		PORTD=0b00000010;
		_delay_ms(rotation_delay);
	
		// E
		PORTD=0b00000001;
		_delay_ms(rotation_delay);

		// F
		PORTD=0b00000000;
		PORTC=0b00010000;
		_delay_ms(rotation_delay);

		// E
		PORTC=0b00000000;
		PORTD=0b00000001;
		_delay_ms(rotation_delay);

		// D
		PORTD=0b00000010;
		_delay_ms(rotation_delay);

		// C
		PORTD=0b00000100;
		_delay_ms(rotation_delay);

		// B
		PORTD=0b00000000;
		PORTC=0b00000100;
		_delay_ms(rotation_delay);

		PORTC=0x00;
	  PORTD=0x00;
		
		i--;
	}



	// da qui in gi� vorrei fare i numeri:
	
	// 0
	PORTC=0b00001000;
	PORTC=0b00000100;
	PORTD=0b00000100;
	PORTD=0b00000010;
	PORTC=0b00000000;
	PORTD=0b00000000;
	_delay_ms(rotation_delay);
	PORTC=0x00;
	PORTD=0x00;

	// 1
	PORTC=0b00000100;
	PORTD=0b00000100;
	_delay_ms(rotation_delay);
	PORTC=0x00;
	PORTD=0x00;

	// 2
	PORTC=0b00001000;
	PORTC=0b00000100;
	PORTC=0b00100000;
	PORTD=0b00000001;
	PORTD=0b00000010;
	_delay_ms(rotation_delay);
		
  

  return(0);
}

/*

Explaining the software
The code's pretty simple, as far as AVR code goes, but still displays a few tricks of the trade. If you're still basking in the glory of your blinking light, you can come back later.

The #include lines load up some of the extra functions and definitions in the AVR-libC suite. In particular, the delay function _delay_ms() is in delay.h. Interrupt.h has all the pin definitions which make life easier. You're always going to want to include it.

The program always starts from the function main(). In this simple case, it's the only function we have.

The first command in main(), DDRD = _BV(PD4);, seems pretty cryptic, but here's what it's doing. DDRD is the Data Direction Register for port D. All the input/output pins are broken up into different ports for easier access. The one we're using happens to be in D. We need to enable the pin PD4 for output for the LED.

The DDRs are set up so that they have 8 bits, one for each pin in the port. You set a pin up for input by writing 0 to its bit in the register. You can set it to output by writing a 1. We want the contents of DDRD to be 00001000, or output only on pin 4 (read right to left). So how do we do this?

_BV(i) takes a number, i, and converts it to an 8-bit binary number where the i'th bit is a 1 and the rest are zeros. Just exactly what we need. And PD4 is the number corresponding to pin 4 on this port. So we set DDRD to _BV(PD4), and then pin 4 (and only pin 4) is set up for output -- blinking our LED.

The rest of the program repeats forever in a loop. It alternates between turning pin D4 on and off, with a delay in-between.

You can turn the individual pins on (and off) by writing a 1 (0) to the PORT register. The syntax is just like above with the DDR -- PORTD = _BV(PD4) sets the fourth pin in port D to 1.

The _delay_ms() function then waits for a bit. It may not be quite 1ms, though. Depending on what clock speed your chip is set at, it may be a lot faster. The timing's not critical here, so let's overlook that for now.

Finally, PORTD &= ~_BV(PD4); turns pin PD4 off without affecting the rest of the values in PORTD. Let's look in detail at how it does it.

_BV(PD4) creates a binary number with the 4th bit (from the right) as a 1 -- 00001000. "~" is the logical complement operator. It turns the 00001000 into 11110111. "&" is the bitwise "and" operator. It compares two bits, and returns a 1 if and only if both bits are 1. If either is 0, it returns a 0.

The "&=" in PORTD &= ~_BV(PD4) is a very common shortcut. It stands for PORTD = PORTD & ~_BV(PD4). This last command compares the current value of PORTD (00001000) and the value (11110111) described above. The zero in the 4th place in ~_BV(PD4), when used with the & operator, always makes the 4th bit of the result = 0, effectively turning off bit PD4.

The 1s in the rest of ~_BV(PD4) make it so that the & operator doesn't clobber the rest of the contents of PORTD. Since the & returns 1 only if both inputs are 1, the remaining bits in PORTD are re-assigned whatever value they already have -- leaving them unchanged.

Could we have set PORTD = 0? Sure. Since we're only using the one pin in D, it would turn it off just fine. But it would have the side-effect of turning off _all_ the pins on port D, and it wouldn't have provided such a nice example. The bit-masking techniques ( &= ~_BV() and its opposite, |= _BV() ) are pretty useful to learn for chip-level programming.

The last part of the code, return(0), never gets reached. The while(1) command ensures that the chip is always going to be stuck in the while loop. I just included the return() command because the compiler complained when I didn't include it -- the main C function in a program is always supposed to have a return value, even if the chip will never get there.

That's a lot of programming for one day. Take some time to admire the simple beauties of the flashing LED.
*/


/*
the following lines are only for simple ascii design...
 __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __
|__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|
|__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__| 
 
*/
