//Copyright (C) 2012 Brundo Salvatore
//salvo85[at]gmail[dot]com
//http://www.brundo.org

/*
hello_world.c : basic hello world atmega8

Copyright (C) 2012 Brundo Salvatore
salvo85[at]gmail[dot]com
http://www.brundo.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <avr/io.h>
#define F_CPU 1000000 // 1 Mhz is default frequency clock of atmega168
#include <util/delay.h>

//DDRx Data Direction Register for port x
int main(){
	
	DDRD = 0xFF;
	
  while(1){
		PORTD=0b11111111;
		_delay_ms(1000);
		
		PORTD=0b00000000;
		_delay_ms(1000);
  }
}
