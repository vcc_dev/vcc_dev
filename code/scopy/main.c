/*
 * main.c
 *
 *  Created on: Nov 22, 2013
 *      Author: salvix
 */


#include <avr/io.h>
#define F_CPU 1000000 // 1 Mhz is default frequency clock of atmega168
#include <util/delay.h>

//DDRx Data Direction Register for port x
int main(){

	DDRD = 0xFF;

  while(1){
		PORTD=0b11111111;
		_delay_ms(1000);

		PORTD=0b00000000;
		_delay_ms(1000);
  }
}
