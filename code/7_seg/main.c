#include <avr/io.h>
#define F_CPU 1000000



#define bit_get(p,m) ((p) & (m))
#define bit_set(p,m) ((p) |= (m))
#define bit_clear(p,m) ((p) &= ~(m))
#define bit_flip(p,m) ((p) ^= (m))
#define bit_write(c,p,m) (c ? bit_set(p,m) : bit_clear(p,m))
#define BIT(x) (0x01 << (x))
#define LONGBIT(x) ((unsigned long)0x00000001 << (x)) 



#define SEG7_0_A_PORT PORTC
#define SEG7_0_A_PIN 5

#define SEG7_0_B_PORT PORTC
#define SEG7_0_B_PIN 4

#define SEG7_0_C_PORT PORTD
#define SEG7_0_C_PIN 6

#define SEG7_0_D_PORT PORTD
#define SEG7_0_D_PIN 5

#define SEG7_0_E_PORT PORTC
#define SEG7_0_E_PIN 1

#define SEG7_0_F_PORT PORTC
#define SEG7_0_F_PIN 3

#define SEG7_0_G_PORT PORTC
#define SEG7_0_G_PIN 2

#define SEG7_0_DOT_PORT PORTD
#define SEG7_0_DOT_PIN 7

// 7-segment map:
//    ____A____
//   |         |
//   F         B
//   |____G____|  
//   |         | 
//   E         C
//   |____D____| .DOT

/*
decimal symbols:
        __    __          __    __   __    __    __    __   
   |    __|   __|  |__|  |__   |__     |  |__|  |__|  |  |  
   |   |__    __|     |   __|  |__|    |  |__|   __|  |__|  
  1      2     3     4     5     6    7     8     9     0


hex symbols:
 __          __          __    __    
|__|  |__   |      __|  |__   |__ 
|  |  |__|  |__   |__|  |__   |    
  A     B     C     D     E     F


alphabetical symbols:
 __          __          __    __    __                    __          
|__|  |__   |      __|  |__   |__   |     |__|         |  |__   |      
|  |  |__|  |__   |__|  |__   |     |__|  |  |   |  |__|  |  |  |__  
  A    B     C     D     E     F     G     H     I    J     K    L  
  
 __                __    __          __                            __   
|  |   __    __   |__|  |__|   __   |     |__   |  |  |  |  |__|   __   
|  |  |  |  |__|  |        |  |      __|  |__   |__|   __    __    __   
  M     N     O     P     Q     R     S     T     U     V     W     X   
  
       __   
|__|     |  
 __|  |__   
  Y     Z   
  
*/

// 48 - 57 numbers, 65 - 90 letters
// BYTE is G F E D C B A DOT
unsigned char seven_lookup_table[40] = {
  0b01111110, // 0
  0b00001100, // 1
  0b10110110, // 2
  0b10011110, // 3
  0b11001100, // 4
  0b11011010, // 5
  0b11111010, // 6
  0b00001110, // 7
  0b11111110, // 8
  0b11011110, // 9

  0b11101110, // a
  0b11111000, // b
  0b01110010, // c
  0b10111100, // d
  0b11110010, // e
  0b11100010, // f
  // END OF HEX!
  
  // these are odd...
  0b01111010, // g
  0b11101100, // h
  0b00001000, // i
  0b00111100, // j

  0b11101010, // k
  0b01110000, // l
  0b01101110, // m
  0b10101000, // n
  0b10111000, // o
  0b11100110, // p
  0b11001110, // q
  0b10100000, // r
  0b01011010, // s
  0b11110000, // t

  0b01111100, // u
  0b01010100, // v
  0b11010100, // w
  0b10010010, // x
  0b11011100, // y
  0b00110110, // z
  
  0b00000001, // .
  0b00000001, // .
  0b00000001, // .
  0b00000001 // .

  
};

#define seg7_0_set_a(){ \
  bit_set(SEG7_0_A_PORT, BIT(SEG7_0_A_PIN)); \
};

#define seg7_0_set_b(){ \
  bit_set(SEG7_0_B_PORT, BIT(SEG7_0_B_PIN)); \
};

#define seg7_0_set_c(){ \
  bit_set(SEG7_0_C_PORT, BIT(SEG7_0_C_PIN)); \
};

#define seg7_0_set_d(){ \
  bit_set(SEG7_0_D_PORT, BIT(SEG7_0_D_PIN)); \
};

#define seg7_0_set_e(){ \
  bit_set(SEG7_0_E_PORT, BIT(SEG7_0_E_PIN)); \
};

#define seg7_0_set_f(){ \
  bit_set(SEG7_0_F_PORT, BIT(SEG7_0_F_PIN)); \
};

#define seg7_0_set_g(){ \
  bit_set(SEG7_0_G_PORT, BIT(SEG7_0_G_PIN)); \
};

#define seg7_0_set_dot(){ \
  bit_set(SEG7_0_DOT_PORT, BIT(SEG7_0_DOT_PIN)); \
};


#define led_init(){ \
  DDRD = 0xFF; \
  DDRC = 0xFF; \
};

#define led_on(){ \
  PORTC = 0xFF; \
  PORTD = 0xFF; \
};

#define led_off(){ \
  PORTD = 0x00; \
  PORTC = 0x00; \
};

#define USART_BAUDRATE 1200 // VERY SLOW WITHOUT CRYSTAL!
#define BAUD_PRESCALE (((F_CPU / (USART_BAUDRATE * 16UL))) - 1)
#define cr() serial_tx('\n');

#define wait() delay_ms(50);
#define q_wait() delay_ms(5);


void serial_init();
unsigned char serial_loopback(); // simple serial loopback to test connections
unsigned char serial_rx();
void serial_tx(unsigned char tx_byte);

void delay_ms(uint16_t x); // general purpose delay

#define SEG7_PERIOD_MS 30
#define seven_wait() delay_ms(SEG7_PERIOD_MS);
unsigned char seven_lookup(unsigned char seg_byte);
void seven_print_char(unsigned char seg_byte);
void seven_print(unsigned char * string);
void seven_segment_on(unsigned char segment);
void seven_alphabet_test(unsigned char start, unsigned char end);

int main (void)
{
  unsigned char rx_byte;
  signed char print_char;

  led_init();
  serial_init();
   
  for (;;) // Loop forever
  {
    
    //seven_alphabet_test(48, 58);
    char *static_str = "     My pookie love you are the best thing that happened in my life I LOVE U GIGANTE AND I WILL LOVE YOU FOREVER";
    seven_print(static_str);
    //~ // SEND GREETINGS
    //~ serial_tx('H');
    //~ serial_tx('i');
    //~ serial_tx('!');
    //~ cr();
    //~ 
    //~ // RECEIVE A CHAR!    
    //~ rx_byte = serial_rx();
    //~ 
    //~ // PRINT RECEIVED CHAR!    
    //~ serial_tx('T');
    //~ serial_tx(':');
    //~ serial_tx(rx_byte);
    //~ cr();    
    //~ cr();
    //~ q_wait(); 
  } 
}

//General short delays
void delay_ms(uint16_t x)
{
  uint8_t y, z;
  for ( ; x > 0 ; x--){
    for ( y = 0 ; y < 80 ; y++){
      for ( z = 0 ; z < 40 ; z++){
        asm volatile ("nop");
      }
    }
  }
}

void serial_init()
{
  UCSRB |= (1 << RXEN) | (1 << TXEN);   // Turn on the transmission and reception circuitry
  UCSRC |= (1 << URSEL) | (1 << UCSZ0) | (1 << UCSZ1); // Use 8-bit character sizes

  UBRRH = (BAUD_PRESCALE >> 8); // Load upper 8-bits of the baud rate value into the high byte of the UBRR register
  UBRRL = BAUD_PRESCALE; // Load lower 8-bits of the baud rate value into the low byte of the UBRR register

}




unsigned char serial_loopback()
{
  char rx_byte;
  while ((UCSRA & (1 << RXC)) == 0) {}; // Do nothing until data have been received and is ready to be read from UDR
  rx_byte = UDR; // Fetch the received byte value into the variable "ByteReceived"
      
  while ((UCSRA & (1 << UDRE)) == 0) {}; // Do nothing until UDR is ready for more data to be written to it
  UDR = rx_byte;
  return rx_byte;
}


void seven_print(unsigned char * string){
  while(*string != '\0'){
    seven_print_char(*string);
    seven_wait();
    led_off();
    seven_wait();
    *string++;
  }

}

void seven_print_char(unsigned char seg_byte){
  unsigned char seg_c, seg_lookup, seg_bit;
  seg_lookup = seven_lookup(seg_byte);
  for(seg_c=0; seg_c<8; seg_c++){
    if(bit_get(seg_lookup, BIT(seg_c))){
      seven_segment_on(seg_c);
    } 
  }
  
}


void seven_alphabet_test(unsigned char start, unsigned char end){
  unsigned char print_char;
  for(print_char = start; print_char < end; print_char++)
  {
    seven_print_char(print_char);
    seven_wait();
    led_off();
    seven_wait();
  }  
}

void seven_segment_on(unsigned char segment){
  switch(segment){
    case 0:
      seg7_0_set_dot();     
    break;
    case 1:
      seg7_0_set_a();      
    break;
    case 2:
      seg7_0_set_b();      
    break;
    case 3:
      seg7_0_set_c();      
    break;
    case 4:
      seg7_0_set_d();      
    break;
    case 5:
      seg7_0_set_e();      
    break;
    case 6:
      seg7_0_set_f();      
    break;
    case 7:
      seg7_0_set_g();      
    break;
  }
}

unsigned char seven_lookup(unsigned char seg_byte){ 
  // BYTE is G F E D C B A DOT
  // 7-segment map:
  //    ____A____
  //   |         |
  //   F         B
  //   |____G____|  
  //   |         | 
  //   E         C
  //   |____D____| .DOT
  unsigned char map_value;
  map_value = 0;
  if(seg_byte>=48 && seg_byte<=57){
    map_value = seg_byte - 48;
  }
  else if(seg_byte>=65 && seg_byte<=90){
    map_value = seg_byte - (65 - 10);
  }
  else if(seg_byte>=97 && seg_byte<=122){
    map_value = seg_byte - (97 - 10);
  }
  else{
    map_value = 39; // . is not defined/available char!
  }
  return seven_lookup_table[map_value];
  // return 0b11110011;
  // return 0b11111111;
  // return ; // 2.
  //~ return 0b00001101; // 1.
}

void serial_tx(unsigned char tx_byte)
{
  while ((UCSRA & (1 << UDRE)) == 0) {}; // Do nothing until UDR is ready for more data to be written to it
  UDR = tx_byte;
}

unsigned char serial_rx()
{
  unsigned char rx_byte;
  while ((UCSRA & (1 << RXC)) == 0) {}; // Do nothing until data have been received and is ready to be read from UDR
  rx_byte = UDR;
  return rx_byte;
}
