#include "sw_rgb_pwm.h"
void init_rgb_led()
{
  bit_set(RGB_DDR, BIT(BLUE_PIN));
  bit_set(RGB_DDR, BIT(GREEN_PIN));
  bit_set(RGB_DDR, BIT(RED_PIN));
}

void blink_port_pin_busy_waiting(volatile uint8_t *port, uint8_t bit, uint16_t duration)
{
  bit_set(*port,bit);
  delay_ms(duration);
  bit_clear(*port,bit);
  delay_ms(duration);
}

void square_wave_port_pin_busy_waiting(volatile uint8_t *port, uint8_t bit, uint8_t duty_cycle){
  bit_set(*port, bit);
  delay_us(duty_cycle);
  bit_clear(*port, bit);
  delay_us(100-duty_cycle);
}

void rgb_square_wave_port_busy_waiting(volatile uint8_t *port, uint8_t bit_r, uint8_t d_cycle_r, uint8_t bit_g, uint8_t d_cycle_g, uint8_t bit_b, uint8_t d_cycle_b){
   uint8_t t_min, t_med, t_max;
  uint8_t min, med, max;
  uint16_t period_size = 255;
  if(d_cycle_r >= d_cycle_g){ // RG -> BRG//RBG//RGB
    if(d_cycle_b >= d_cycle_r){
      // BRG
      max = bit_b; med = bit_r; min = bit_g;
      t_max = d_cycle_b; t_med = d_cycle_r; t_min = d_cycle_g;
    }
    else{
      if(d_cycle_g >= d_cycle_b){
        // RGB
        max = bit_r; med = bit_g; min = bit_b;
        t_max = d_cycle_r; t_med = d_cycle_g; t_min = d_cycle_b;
      }
      else {
        // RBG
        max = bit_r; med = bit_b; min = bit_g;
        t_max = d_cycle_r; t_med = d_cycle_b; t_min = d_cycle_g;
      }
    }
  }
  else{//GR -> BGR//GBR//GRB
    if(d_cycle_b >= d_cycle_g){
      //BGR
      max = bit_b; med = bit_g; min = bit_r;
      t_max = d_cycle_b; t_med = d_cycle_g; t_min = d_cycle_r;

    }
    else {
      if(d_cycle_r >= d_cycle_b){
        // GRB
        max = bit_g; med = bit_r; min = bit_b;
        t_max = d_cycle_g; t_med = d_cycle_r; t_min = d_cycle_b;
      }
      else {
        // GBR
        max = bit_g; med = bit_b; min = bit_r;
        t_max = d_cycle_g; t_med = d_cycle_b; t_min = d_cycle_r;
      }
    }
  }
  if(d_cycle_r > 0)
    bit_set(*port, bit_r);
  if(d_cycle_g > 0)
    bit_set(*port, bit_g);
  if(d_cycle_b > 0)
    bit_set(*port, bit_b);
  delay_us(t_min);
  bit_clear(*port, min);
  delay_us(t_med - t_min);
  bit_clear(*port, med);
  delay_us(t_max - t_med);
  bit_clear(*port, max);
  delay_us(period_size - t_max);



}
void repeat_square_wave_port_pin_busy_waiting(volatile uint8_t *port, uint8_t bit, uint8_t duty_cycle, uint8_t times){
  uint16_t actual_duration = times * 10;
  while(actual_duration--){
    square_wave_port_pin_busy_waiting(port, bit, duty_cycle);
  }
}

void blink()
{
  bit_blink(RGB_PORT, BIT(RED_PIN), BLINK_PERIOD);
  bit_blink(RGB_PORT, BIT(GREEN_PIN), BLINK_PERIOD);
  bit_blink(RGB_PORT, BIT(BLUE_PIN), BLINK_PERIOD);
}


void glow()
{
  uint8_t i;
  for(i = 0; i < 100; i++){
    bit_pwm(RGB_PORT, BIT(BLUE_PIN), i, GLOW_PERIOD/100);
  }
  for(i = 100; i > 0; i--){
    bit_pwm(RGB_PORT, BIT(BLUE_PIN), i, GLOW_PERIOD/100);
  }
  for(i = 0; i < 100; i++){
    bit_pwm(RGB_PORT, BIT(GREEN_PIN), i, GLOW_PERIOD/100);
  }
  for(i = 100; i > 0; i--){
    bit_pwm(RGB_PORT, BIT(GREEN_PIN), i, GLOW_PERIOD/100);
  }
  for(i = 0; i < 100; i++){
    bit_pwm(RGB_PORT, BIT(RED_PIN), i, GLOW_PERIOD/100);
  }
  for(i = 100; i > 0; i--){
    bit_pwm(RGB_PORT, BIT(RED_PIN), i, GLOW_PERIOD/100);
  }
}


void delay_ms(uint16_t count){
  while(count--){
    _delay_ms(1);
  }
}

void delay_us(uint16_t count){
  while(count--){
    _delay_us(1);
  }
}

void rgb_colour(uint8_t red, uint8_t green, uint8_t blue)
{

  uint16_t counter = 500;
  while(counter--){
    rgb_pwm(RGB_PORT, BIT(RED_PIN), red, BIT(GREEN_PIN), green, BIT(BLUE_PIN), blue);
  }
}
void rgb_space_2()
{
  uint8_t max = 100;
  uint8_t step = 10;
  uint8_t r,g,b;
  uint16_t counter;
  for(r = max; r > 0; r = r - step){
    for(g = max; g > 0; g = g - step){
      for(b = max; b > 0; b = b - step ){
        rgb_colour(r, g, b);
      }
    }
  }
}
void rgb_space()
{
  uint8_t max = 50;
  uint8_t step = 5;
  uint8_t r,g,b;
  uint16_t counter;
  for(r = 0; r < max; r = r+step){
    for(g = 0; g < max; g = g+step){
      for(b = 0; b < max; b = b+step){
        rgb_colour(r,g,b);
      }
    }
  }
}
