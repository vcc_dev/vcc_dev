#define F_CPU 16500000
#include "bit101.h"
#include <avr/io.h>
#include <util/delay.h>

#define RGB_PORT PORTB
#define RGB_DDR DDRB
#define BLUE_PIN 0
#define GREEN_PIN 1
#define RED_PIN 2
#define BLINK_PERIOD 300
#define GLOW_PERIOD 300

#define bit_blink(p,b,d) blink_port_pin_busy_waiting(&p,b,d)
#define square_wave(p,b,d) square_wave_port_pin_busy_waiting(&p,b,d)
#define bit_pwm(p,b,d,dd) repeat_square_wave_port_pin_busy_waiting(&p,b,d,dd)
#define rgb_pwm(p,r,dr,g,dg,b,db) rgb_square_wave_port_busy_waiting(&p,r,dr,g,dg,b,db)

void init_rgb_led();

void blink_port_pin_busy_waiting(volatile uint8_t *port, uint8_t bit, uint16_t duration);
void square_wave_port_pin_busy_waiting(volatile uint8_t *port, uint8_t bit, uint8_t duty_cycle);


void rgb_square_wave_port_busy_waiting(volatile uint8_t *port, uint8_t bit_r, uint8_t d_cycle_r, uint8_t bit_g, uint8_t d_cycle_g, uint8_t bit_b, uint8_t d_cycle_b);

void repeat_square_wave_port_pin_busy_waiting(volatile uint8_t *port, uint8_t bit, uint8_t duty_cycle, uint8_t times);

void blink();

void glow();

void delay_ms(uint16_t count);


void delay_us(uint16_t count);
void rgb_colour(uint8_t red, uint8_t green, uint8_t blue);
void rgb_space_2();

void rgb_space();
