//Copyright (C) 2018 Brundo Salvatore
//salvo85[at]gmail[dot]com
//http://www.brundo.org

/*
hello_world.c : basic hello world atmega8
*/

#include <avr/io.h>
#define F_CPU 16500000 // 16.5 Mhz is default frequency clock of a digispark!
#include <util/delay.h>

//DDRx Data Direction Register for port x
int main(){

  DDRB = 0xFF;

  while(1){
    PORTB = 0b11111111;
    _delay_ms(1000);

    PORTB = 0b00000000;
    _delay_ms(1000);
  }
}
