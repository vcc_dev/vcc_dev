#include "colors.inc"
#include "shapes.inc"
#include "textures.inc"

box{
//angolo inferiore sinistro,//anfolo superiore destro
<0,0,5>,<4,4,15>
texture{
pigment{color Red}
}
rotate <0,20,0>
}

plane { y, 0
pigment {
checker color Gray65 color Gray30
}
}

light_source{<2,2,2> color White}
light_source{<2,5,7> color White}
light_source{<2,5,12> color White}
light_source{<-10,2,5> color Blue}


camera{
	location <6,6,-2>
	look_at <2,2,5>
}