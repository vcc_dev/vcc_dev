//POVRay-File created by 3d41.ulp v20110101
///home/salvix/Dev/vcc_dev/5v_power/5v_power.brd
//11/8/12 2:24 AM

#version 3.5;

//Set to on if the file should be used as .inc
#local use_file_as_inc = off;
#if(use_file_as_inc=off)


//changes the apperance of resistors (1 Blob / 0 real)
#declare global_res_shape = 1;
//randomize color of resistors 1=random 0=same color
#declare global_res_colselect = 0;
//Number of the color for the resistors
//0=Green, 1="normal color" 2=Blue 3=Brown
#declare global_res_col = 1;
//Set to on if you want to render the PCB upside-down
#declare pcb_upsidedown = off;
//Set to x or z to rotate around the corresponding axis (referring to pcb_upsidedown)
#declare pcb_rotdir = x;
//Set the length off short pins over the PCB
#declare pin_length = 2.5;
#declare global_diode_bend_radius = 1;
#declare global_res_bend_radius = 1;
#declare global_solder = on;

#declare global_show_screws = on;
#declare global_show_washers = on;
#declare global_show_nuts = on;

#declare global_use_radiosity = on;

#declare global_ambient_mul = 1;
#declare global_ambient_mul_emit = 0;

//Animation
#declare global_anim = off;
#local global_anim_showcampath = no;

#declare global_fast_mode = off;

#declare col_preset = 2;
#declare pin_short = on;

#declare e3d_environment = off;

#local cam_x = 0;
#local cam_y = 162;
#local cam_z = -87;
#local cam_a = 20;
#local cam_look_x = 0;
#local cam_look_y = -3;
#local cam_look_z = 0;

#local pcb_rotate_x = 0;
#local pcb_rotate_y = 0;
#local pcb_rotate_z = 0;

#local pcb_board = on;
#local pcb_parts = on;
#local pcb_wire_bridges = off;
#if(global_fast_mode=off)
	#local pcb_polygons = on;
	#local pcb_silkscreen = on;
	#local pcb_wires = on;
	#local pcb_pads_smds = on;
#else
	#local pcb_polygons = off;
	#local pcb_silkscreen = off;
	#local pcb_wires = off;
	#local pcb_pads_smds = off;
#end

#local lgt1_pos_x = 19;
#local lgt1_pos_y = 28;
#local lgt1_pos_z = 20;
#local lgt1_intense = 0.727650;
#local lgt2_pos_x = -19;
#local lgt2_pos_y = 28;
#local lgt2_pos_z = 20;
#local lgt2_intense = 0.727650;
#local lgt3_pos_x = 19;
#local lgt3_pos_y = 28;
#local lgt3_pos_z = -13;
#local lgt3_intense = 0.727650;
#local lgt4_pos_x = -19;
#local lgt4_pos_y = 28;
#local lgt4_pos_z = -13;
#local lgt4_intense = 0.727650;

//Do not change these values
#declare pcb_height = 1.500000;
#declare pcb_cuheight = 0.035000;
#declare pcb_x_size = 50.800000;
#declare pcb_y_size = 38.100000;
#declare pcb_layer1_used = 0;
#declare pcb_layer16_used = 1;
#declare inc_testmode = off;
#declare global_seed=seed(980);
#declare global_pcb_layer_dis = array[16]
{
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	1.535000,
}
#declare global_pcb_real_hole = 2.000000;

#include "e3d_tools.inc"
#include "e3d_user.inc"

global_settings{charset utf8}

#if(e3d_environment=on)
sky_sphere {pigment {Navy}
pigment {bozo turbulence 0.65 octaves 7 omega 0.7 lambda 2
color_map {
[0.0 0.1 color rgb <0.85, 0.85, 0.85> color rgb <0.75, 0.75, 0.75>]
[0.1 0.5 color rgb <0.75, 0.75, 0.75> color rgbt <1, 1, 1, 1>]
[0.5 1.0 color rgbt <1, 1, 1, 1> color rgbt <1, 1, 1, 1>]}
scale <0.1, 0.5, 0.1>} rotate -90*x}
plane{y, -10.0-max(pcb_x_size,pcb_y_size)*abs(max(sin((pcb_rotate_x/180)*pi),sin((pcb_rotate_z/180)*pi)))
texture{T_Chrome_2D
normal{waves 0.1 frequency 3000.0 scale 3000.0}} translate<0,0,0>}
#end

//Animation data
#if(global_anim=on)
#declare global_anim_showcampath = no;
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#declare global_anim_npoints_cam_flight=0;
#warning "No/not enough Animation Data available (min. 3 points) (Flight path)"
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#declare global_anim_npoints_cam_view=0;
#warning "No/not enough Animation Data available (min. 3 points) (View path)"
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#end

#if((global_anim_showcampath=yes)&(global_anim=off))
#end
#if(global_anim=on)
camera
{
	location global_anim_spline_cam_flight(clock)
	#if(global_anim_npoints_cam_view>2)
		look_at global_anim_spline_cam_view(clock)
	#else
		look_at global_anim_spline_cam_flight(clock+0.01)-<0,-0.01,0>
	#end
	angle 45
}
light_source
{
	global_anim_spline_cam_flight(clock)
	color rgb <1,1,1>
	spotlight point_at 
	#if(global_anim_npoints_cam_view>2)
		global_anim_spline_cam_view(clock)
	#else
		global_anim_spline_cam_flight(clock+0.01)-<0,-0.01,0>
	#end
	radius 35 falloff  40
}
#else
camera
{
	location <cam_x,cam_y,cam_z>
	look_at <cam_look_x,cam_look_y,cam_look_z>
	angle cam_a
	//translates the camera that <0,0,0> is over the Eagle <0,0>
	//translate<-25.400000,0,-19.050000>
}
#end

background{col_bgr}
light_source{<lgt1_pos_x,lgt1_pos_y,lgt1_pos_z> White*lgt1_intense}
light_source{<lgt2_pos_x,lgt2_pos_y,lgt2_pos_z> White*lgt2_intense}
light_source{<lgt3_pos_x,lgt3_pos_y,lgt3_pos_z> White*lgt3_intense}
light_source{<lgt4_pos_x,lgt4_pos_y,lgt4_pos_z> White*lgt4_intense}
#end


#macro ZV_POWER(mac_x_ver,mac_y_ver,mac_z_ver,mac_x_rot,mac_y_rot,mac_z_rot)
union{
#if(pcb_board = on)
difference{
union{
//Board
prism{-1.500000,0.000000,8
<0.000000,0.000000><50.800000,0.000000>
<50.800000,0.000000><50.800000,38.100000>
<50.800000,38.100000><0.000000,38.100000>
<0.000000,38.100000><0.000000,0.000000>
texture{col_brd}}
}//End union(PCB)
//Holes(real)/Parts
cylinder{<35.560000,1,44.196000><35.560000,-5,44.196000>1.651000 texture{col_hls}}
//Holes(real)/Board
cylinder{<48.260000,1,2.540000><48.260000,-5,2.540000>1.651000 texture{col_hls}}
cylinder{<2.540000,1,2.540000><2.540000,-5,2.540000>1.651000 texture{col_hls}}
cylinder{<2.540000,1,35.560000><2.540000,-5,35.560000>1.651000 texture{col_hls}}
cylinder{<48.260000,1,35.560000><48.260000,-5,35.560000>1.651000 texture{col_hls}}
//Holes(real)/Vias
}//End difference(reale Bohrungen/Durchbrüche)
#end
#if(pcb_parts=on)//Parts
union{
#ifndef(pack_C1) #declare global_pack_C1=yes; object {CAP_DIS_ELKO_2MM_5MM("100uF/25V",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,0> translate<25.400000,0.000000,20.320000>}#end		//Elko 2mm Pitch, 5mm  Diameter, 11mm High C1 100uF/25V CPOL-RADIAL-100UF-25V
#ifndef(pack_C2) #declare global_pack_C2=yes; object {CAP_DIS_ELKO_2MM_5MM("10uF/10V",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,0> translate<25.400000,0.000000,10.160000>}#end		//Elko 2mm Pitch, 5mm  Diameter, 11mm High C2 10uF/10V CPOL-RADIAL-100UF-25V
#ifndef(pack_D1) #declare global_pack_D1=yes; object {DIODE_DIS_DO7_102MM_H("1N4001",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,0> translate<12.700000,0.000000,25.400000>}#end		//Diode DO7 10mm hor. D1 1N4001 DIODE-1N4001
#ifndef(pack_IC1) #declare global_pack_IC1=yes; object {TR_TO220_3_H2("7805",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<35.560000,0.000000,33.020000>}#end		//TO220 horizontal straight leads IC1 7805 78XXL
#ifndef(pack_LED1) #declare global_pack_LED1=yes; object {DIODE_DIS_LED_5MM(Red,0.700000,0.000000,)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,0> translate<38.100000,0.000000,7.620000>}#end		//Diskrete 5MM LED LED1 Power LED5MM
#ifndef(pack_PTC) #declare global_pack_PTC=yes; object {RES_DIS_PTC660_5_3("250mA",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,0> translate<17.780000,0.000000,25.400000>}#end		//PTC thermistor diam 5 x 3 PTC 250mA PTC
#ifndef(pack_R1) #declare global_pack_R1=yes; object {RES_DIS_0207_10MM(texture{pigment{Yellow}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{Violet*1.2}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{DarkBrown}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture {T_Gold_5C finish{reflection 0.1}},)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,0> translate<38.100000,0.000000,20.320000>}#end		//Discrete Resistor 0,3W 10MM Grid R1 470 AXIAL-0.4
#ifndef(pack_S1) #declare global_pack_S1=yes; object {SWITCH_SECME_1K2_SH()translate<0,0,0> rotate<0,0.000000,0>rotate<0,0,0> rotate<0,0,0> translate<15.240000,0.000000,35.560000>}#end		//Switch 1K2 Straight High actuator S1 On/Off SWITCH-SPDT_KIT
#ifndef(pack_VIN) #declare global_pack_VIN=yes; object {CON_PH_1X2()translate<3,0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,0> translate<5.080000,0.000000,17.780000>}#end		//Header 2,54mm Grid 2Pin 1Row (jumper.lib) VIN >9V 1X02_LOCK
#ifndef(pack_VOUT) #declare global_pack_VOUT=yes; object {CON_PH_1X2()translate<0,0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,0> translate<45.720000,0.000000,20.320000>}#end		//Header 2,54mm Grid 2Pin 1Row (jumper.lib) VOUT 5V 1X02_LOCK
}//End union
#end
#if(pcb_pads_smds=on)
//Pads&SMD/Parts
#ifndef(global_pack_C1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.651000,0.700000,1,16,0+global_tmp,0) rotate<0,-270.000000,0>translate<25.400000,0,19.050000> texture{col_thl}}
#ifndef(global_pack_C1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.651000,0.700000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<25.400000,0,21.590000> texture{col_thl}}
#ifndef(global_pack_C2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.651000,0.700000,1,16,0+global_tmp,0) rotate<0,-270.000000,0>translate<25.400000,0,8.890000> texture{col_thl}}
#ifndef(global_pack_C2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.651000,0.700000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<25.400000,0,11.430000> texture{col_thl}}
#ifndef(global_pack_D1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.981200,1.000000,1,16,1+global_tmp,0) rotate<0,-90.000000,0>translate<12.700000,0,20.320000> texture{col_thl}}
#ifndef(global_pack_D1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.981200,1.000000,1,16,1+global_tmp,0) rotate<0,-90.000000,0>translate<12.700000,0,30.480000> texture{col_thl}}
#ifndef(global_pack_IC1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<35.560000,0,29.210000> texture{col_thl}}
#ifndef(global_pack_IC1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<33.020000,0,29.210000> texture{col_thl}}
#ifndef(global_pack_IC1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<38.100000,0,29.210000> texture{col_thl}}
#ifndef(global_pack_LED1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,2+global_tmp,0) rotate<0,-270.000000,0>translate<38.100000,0,8.890000> texture{col_thl}}
#ifndef(global_pack_LED1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,2+global_tmp,0) rotate<0,-270.000000,0>translate<38.100000,0,6.350000> texture{col_thl}}
#ifndef(global_pack_PTC) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.800000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<17.780000,0,27.940000> texture{col_thl}}
#ifndef(global_pack_PTC) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.800000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<17.780000,0,22.860000> texture{col_thl}}
#ifndef(global_pack_R1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-90.000000,0>translate<38.100000,0,15.240000> texture{col_thl}}
#ifndef(global_pack_R1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-90.000000,0>translate<38.100000,0,25.400000> texture{col_thl}}
#ifndef(global_pack_S1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-90.000000,0>translate<12.522200,0,35.560000> texture{col_thl}}
#ifndef(global_pack_S1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-90.000000,0>translate<15.240000,0,35.560000> texture{col_thl}}
#ifndef(global_pack_S1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-90.000000,0>translate<17.957800,0,35.560000> texture{col_thl}}
#ifndef(global_pack_VIN) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-180.000000,0>translate<5.080000,0,17.602200> texture{col_thl}}
#ifndef(global_pack_VIN) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-180.000000,0>translate<5.080000,0,20.497800> texture{col_thl}}
#ifndef(global_pack_VOUT) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-0.000000,0>translate<45.720000,0,20.497800> texture{col_thl}}
#ifndef(global_pack_VOUT) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-0.000000,0>translate<45.720000,0,17.602200> texture{col_thl}}
//Pads/Vias
#end
#if(pcb_wires=on)
union{
//Signals
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<5.080000,-1.535000,17.602200>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<5.080000,-1.535000,17.780000>}
box{<0,0,-0.508000><0.177800,0.035000,0.508000> rotate<0,90.000000,0> translate<5.080000,-1.535000,17.780000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<5.080000,-1.535000,20.497800>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<5.080000,-1.535000,20.320000>}
box{<0,0,-0.508000><0.177800,0.035000,0.508000> rotate<0,-90.000000,0> translate<5.080000,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<5.080000,-1.535000,17.780000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<12.700000,-1.535000,17.780000>}
box{<0,0,-0.508000><7.620000,0.035000,0.508000> rotate<0,0.000000,0> translate<5.080000,-1.535000,17.780000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<5.080000,-1.535000,20.320000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<12.700000,-1.535000,20.320000>}
box{<0,0,-0.508000><7.620000,0.035000,0.508000> rotate<0,0.000000,0> translate<5.080000,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<12.700000,-1.535000,30.480000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<15.240000,-1.535000,33.020000>}
box{<0,0,-0.508000><3.592102,0.035000,0.508000> rotate<0,-44.997030,0> translate<12.700000,-1.535000,30.480000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<15.240000,-1.535000,33.020000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<15.240000,-1.535000,35.560000>}
box{<0,0,-0.508000><2.540000,0.035000,0.508000> rotate<0,90.000000,0> translate<15.240000,-1.535000,35.560000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<17.780000,-1.535000,27.940000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<17.780000,-1.535000,35.560000>}
box{<0,0,-0.508000><7.620000,0.035000,0.508000> rotate<0,90.000000,0> translate<17.780000,-1.535000,35.560000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<17.780000,-1.535000,35.560000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<17.957800,-1.535000,35.560000>}
box{<0,0,-0.508000><0.177800,0.035000,0.508000> rotate<0,0.000000,0> translate<17.780000,-1.535000,35.560000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<12.700000,-1.535000,17.780000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<19.050000,-1.535000,17.780000>}
box{<0,0,-0.508000><6.350000,0.035000,0.508000> rotate<0,0.000000,0> translate<12.700000,-1.535000,17.780000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<17.780000,-1.535000,22.860000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<21.590000,-1.535000,22.860000>}
box{<0,0,-0.508000><3.810000,0.035000,0.508000> rotate<0,0.000000,0> translate<17.780000,-1.535000,22.860000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<12.700000,-1.535000,17.780000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<24.130000,-1.535000,6.350000>}
box{<0,0,-0.508000><16.164461,0.035000,0.508000> rotate<0,44.997030,0> translate<12.700000,-1.535000,17.780000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<19.050000,-1.535000,17.780000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<25.400000,-1.535000,11.430000>}
box{<0,0,-0.508000><8.980256,0.035000,0.508000> rotate<0,44.997030,0> translate<19.050000,-1.535000,17.780000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<21.590000,-1.535000,22.860000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<25.400000,-1.535000,19.050000>}
box{<0,0,-0.508000><5.388154,0.035000,0.508000> rotate<0,44.997030,0> translate<21.590000,-1.535000,22.860000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<25.400000,-1.535000,25.400000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<25.400000,-1.535000,21.590000>}
box{<0,0,-0.508000><3.810000,0.035000,0.508000> rotate<0,-90.000000,0> translate<25.400000,-1.535000,21.590000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<25.400000,-1.535000,25.400000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<31.750000,-1.535000,31.750000>}
box{<0,0,-0.508000><8.980256,0.035000,0.508000> rotate<0,-44.997030,0> translate<25.400000,-1.535000,25.400000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<25.400000,-1.535000,19.050000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<33.020000,-1.535000,26.670000>}
box{<0,0,-0.508000><10.776307,0.035000,0.508000> rotate<0,-44.997030,0> translate<25.400000,-1.535000,19.050000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<33.020000,-1.535000,26.670000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<33.020000,-1.535000,29.210000>}
box{<0,0,-0.508000><2.540000,0.035000,0.508000> rotate<0,90.000000,0> translate<33.020000,-1.535000,29.210000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<31.750000,-1.535000,31.750000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<34.290000,-1.535000,31.750000>}
box{<0,0,-0.508000><2.540000,0.035000,0.508000> rotate<0,0.000000,0> translate<31.750000,-1.535000,31.750000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<25.400000,-1.535000,11.430000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<35.560000,-1.535000,21.590000>}
box{<0,0,-0.508000><14.368410,0.035000,0.508000> rotate<0,-44.997030,0> translate<25.400000,-1.535000,11.430000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<35.560000,-1.535000,21.590000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<35.560000,-1.535000,29.210000>}
box{<0,0,-0.508000><7.620000,0.035000,0.508000> rotate<0,90.000000,0> translate<35.560000,-1.535000,29.210000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<34.290000,-1.535000,31.750000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<35.560000,-1.535000,30.480000>}
box{<0,0,-0.508000><1.796051,0.035000,0.508000> rotate<0,44.997030,0> translate<34.290000,-1.535000,31.750000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<35.560000,-1.535000,29.210000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<35.560000,-1.535000,30.480000>}
box{<0,0,-0.508000><1.270000,0.035000,0.508000> rotate<0,90.000000,0> translate<35.560000,-1.535000,30.480000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<25.400000,-1.535000,8.890000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<36.830000,-1.535000,20.320000>}
box{<0,0,-0.508000><16.164461,0.035000,0.508000> rotate<0,-44.997030,0> translate<25.400000,-1.535000,8.890000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<24.130000,-1.535000,6.350000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<38.100000,-1.535000,6.350000>}
box{<0,0,-0.508000><13.970000,0.035000,0.508000> rotate<0,0.000000,0> translate<24.130000,-1.535000,6.350000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<38.100000,-1.535000,8.890000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<38.100000,-1.535000,15.240000>}
box{<0,0,-0.508000><6.350000,0.035000,0.508000> rotate<0,90.000000,0> translate<38.100000,-1.535000,15.240000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<36.830000,-1.535000,20.320000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<38.100000,-1.535000,21.590000>}
box{<0,0,-0.508000><1.796051,0.035000,0.508000> rotate<0,-44.997030,0> translate<36.830000,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<38.100000,-1.535000,21.590000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<38.100000,-1.535000,25.400000>}
box{<0,0,-0.508000><3.810000,0.035000,0.508000> rotate<0,90.000000,0> translate<38.100000,-1.535000,25.400000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<38.100000,-1.535000,25.400000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<38.100000,-1.535000,29.210000>}
box{<0,0,-0.508000><3.810000,0.035000,0.508000> rotate<0,90.000000,0> translate<38.100000,-1.535000,29.210000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<38.100000,-1.535000,6.350000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<45.720000,-1.535000,13.970000>}
box{<0,0,-0.508000><10.776307,0.035000,0.508000> rotate<0,-44.997030,0> translate<38.100000,-1.535000,6.350000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<45.720000,-1.535000,13.970000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<45.720000,-1.535000,17.602200>}
box{<0,0,-0.508000><3.632200,0.035000,0.508000> rotate<0,90.000000,0> translate<45.720000,-1.535000,17.602200> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<45.720000,-1.535000,17.602200>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<45.720000,-1.535000,17.780000>}
box{<0,0,-0.508000><0.177800,0.035000,0.508000> rotate<0,90.000000,0> translate<45.720000,-1.535000,17.780000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<45.720000,-1.535000,17.602200>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<45.720000,-1.535000,17.780000>}
box{<0,0,-0.508000><0.177800,0.035000,0.508000> rotate<0,90.000000,0> translate<45.720000,-1.535000,17.780000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<36.830000,-1.535000,20.320000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<45.720000,-1.535000,20.320000>}
box{<0,0,-0.508000><8.890000,0.035000,0.508000> rotate<0,0.000000,0> translate<36.830000,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<45.720000,-1.535000,20.320000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<45.720000,-1.535000,20.497800>}
box{<0,0,-0.508000><0.177800,0.035000,0.508000> rotate<0,90.000000,0> translate<45.720000,-1.535000,20.497800> }
//Text
//Rect
union{
texture{col_pds}
}
texture{col_wrs}
}
#end
#if(pcb_polygons=on)
union{
//Polygons
texture{col_pol}
}
#end
union{
cylinder{<25.400000,0.038000,19.050000><25.400000,-1.538000,19.050000>0.350000}
cylinder{<25.400000,0.038000,21.590000><25.400000,-1.538000,21.590000>0.350000}
cylinder{<25.400000,0.038000,8.890000><25.400000,-1.538000,8.890000>0.350000}
cylinder{<25.400000,0.038000,11.430000><25.400000,-1.538000,11.430000>0.350000}
cylinder{<12.700000,0.038000,20.320000><12.700000,-1.538000,20.320000>0.500000}
cylinder{<12.700000,0.038000,30.480000><12.700000,-1.538000,30.480000>0.500000}
cylinder{<35.560000,0.038000,29.210000><35.560000,-1.538000,29.210000>0.508000}
cylinder{<33.020000,0.038000,29.210000><33.020000,-1.538000,29.210000>0.508000}
cylinder{<38.100000,0.038000,29.210000><38.100000,-1.538000,29.210000>0.508000}
cylinder{<38.100000,0.038000,8.890000><38.100000,-1.538000,8.890000>0.406400}
cylinder{<38.100000,0.038000,6.350000><38.100000,-1.538000,6.350000>0.406400}
cylinder{<17.780000,0.038000,27.940000><17.780000,-1.538000,27.940000>0.400000}
cylinder{<17.780000,0.038000,22.860000><17.780000,-1.538000,22.860000>0.400000}
cylinder{<38.100000,0.038000,15.240000><38.100000,-1.538000,15.240000>0.450000}
cylinder{<38.100000,0.038000,25.400000><38.100000,-1.538000,25.400000>0.450000}
cylinder{<12.522200,0.038000,35.560000><12.522200,-1.538000,35.560000>0.508000}
cylinder{<15.240000,0.038000,35.560000><15.240000,-1.538000,35.560000>0.508000}
cylinder{<17.957800,0.038000,35.560000><17.957800,-1.538000,35.560000>0.508000}
cylinder{<5.080000,0.038000,17.602200><5.080000,-1.538000,17.602200>0.508000}
cylinder{<5.080000,0.038000,20.497800><5.080000,-1.538000,20.497800>0.508000}
cylinder{<45.720000,0.038000,20.497800><45.720000,-1.538000,20.497800>0.508000}
cylinder{<45.720000,0.038000,17.602200><45.720000,-1.538000,17.602200>0.508000}
//Holes(fast)/Vias
//Holes(fast)/Board
texture{col_hls}
}
#if(pcb_silkscreen=on)
//Silk Screen
union{
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.780800,0.000000,2.616200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.696200,0.000000,2.616200>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<7.696200,0.000000,2.616200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.696200,0.000000,2.616200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.780800,0.000000,3.700800>}
box{<0,0,-0.076200><1.533856,0.036000,0.076200> rotate<0,-44.997030,0> translate<7.696200,0.000000,2.616200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.780800,0.000000,3.700800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.780800,0.000000,3.972000>}
box{<0,0,-0.076200><0.271200,0.036000,0.076200> rotate<0,90.000000,0> translate<8.780800,0.000000,3.972000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.780800,0.000000,3.972000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.509600,0.000000,4.243100>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,44.986466,0> translate<8.509600,0.000000,4.243100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.509600,0.000000,4.243100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.967300,0.000000,4.243100>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<7.967300,0.000000,4.243100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.967300,0.000000,4.243100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.696200,0.000000,3.972000>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,-44.997030,0> translate<7.696200,0.000000,3.972000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.333300,0.000000,2.887300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.333300,0.000000,3.972000>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,90.000000,0> translate<9.333300,0.000000,3.972000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.333300,0.000000,3.972000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.604400,0.000000,4.243100>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,-44.997030,0> translate<9.333300,0.000000,3.972000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.604400,0.000000,4.243100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.146700,0.000000,4.243100>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<9.604400,0.000000,4.243100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.146700,0.000000,4.243100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.417900,0.000000,3.972000>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,44.986466,0> translate<10.146700,0.000000,4.243100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.417900,0.000000,3.972000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.417900,0.000000,2.887300>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,-90.000000,0> translate<10.417900,0.000000,2.887300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.417900,0.000000,2.887300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.146700,0.000000,2.616200>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-44.986466,0> translate<10.146700,0.000000,2.616200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.146700,0.000000,2.616200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.604400,0.000000,2.616200>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<9.604400,0.000000,2.616200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.604400,0.000000,2.616200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.333300,0.000000,2.887300>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<9.333300,0.000000,2.887300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.333300,0.000000,2.887300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.417900,0.000000,3.972000>}
box{<0,0,-0.076200><1.533927,0.036000,0.076200> rotate<0,-44.999671,0> translate<9.333300,0.000000,2.887300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.970400,0.000000,3.700800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.512700,0.000000,4.243100>}
box{<0,0,-0.076200><0.766928,0.036000,0.076200> rotate<0,-44.997030,0> translate<10.970400,0.000000,3.700800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.512700,0.000000,4.243100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.512700,0.000000,2.616200>}
box{<0,0,-0.076200><1.626900,0.036000,0.076200> rotate<0,-90.000000,0> translate<11.512700,0.000000,2.616200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.970400,0.000000,2.616200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.055000,0.000000,2.616200>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<10.970400,0.000000,2.616200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.692100,0.000000,2.616200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.607500,0.000000,2.616200>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<12.607500,0.000000,2.616200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.607500,0.000000,2.616200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.692100,0.000000,3.700800>}
box{<0,0,-0.076200><1.533856,0.036000,0.076200> rotate<0,-44.997030,0> translate<12.607500,0.000000,2.616200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.692100,0.000000,3.700800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.692100,0.000000,3.972000>}
box{<0,0,-0.076200><0.271200,0.036000,0.076200> rotate<0,90.000000,0> translate<13.692100,0.000000,3.972000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.692100,0.000000,3.972000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.420900,0.000000,4.243100>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,44.986466,0> translate<13.420900,0.000000,4.243100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.420900,0.000000,4.243100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.878600,0.000000,4.243100>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<12.878600,0.000000,4.243100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.878600,0.000000,4.243100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.607500,0.000000,3.972000>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,-44.997030,0> translate<12.607500,0.000000,3.972000> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<2.692400,0.000000,7.772400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<2.692400,0.000000,11.280500>}
box{<0,0,-0.152400><3.508100,0.036000,0.152400> rotate<0,90.000000,0> translate<2.692400,0.000000,11.280500> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<2.692400,0.000000,11.280500>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<5.031100,0.000000,7.772400>}
box{<0,0,-0.152400><4.216193,0.036000,0.152400> rotate<0,56.306593,0> translate<2.692400,0.000000,11.280500> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<5.031100,0.000000,7.772400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<5.031100,0.000000,11.280500>}
box{<0,0,-0.152400><3.508100,0.036000,0.152400> rotate<0,90.000000,0> translate<5.031100,0.000000,11.280500> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<7.954500,0.000000,7.772400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<6.785100,0.000000,7.772400>}
box{<0,0,-0.152400><1.169400,0.036000,0.152400> rotate<0,0.000000,0> translate<6.785100,0.000000,7.772400> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<6.785100,0.000000,7.772400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<6.200500,0.000000,8.357000>}
box{<0,0,-0.152400><0.826749,0.036000,0.152400> rotate<0,44.997030,0> translate<6.200500,0.000000,8.357000> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<6.200500,0.000000,8.357000>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<6.200500,0.000000,9.526400>}
box{<0,0,-0.152400><1.169400,0.036000,0.152400> rotate<0,90.000000,0> translate<6.200500,0.000000,9.526400> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<6.200500,0.000000,9.526400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<6.785100,0.000000,10.111100>}
box{<0,0,-0.152400><0.826820,0.036000,0.152400> rotate<0,-45.001930,0> translate<6.200500,0.000000,9.526400> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<6.785100,0.000000,10.111100>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<7.954500,0.000000,10.111100>}
box{<0,0,-0.152400><1.169400,0.036000,0.152400> rotate<0,0.000000,0> translate<6.785100,0.000000,10.111100> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<7.954500,0.000000,10.111100>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<8.539200,0.000000,9.526400>}
box{<0,0,-0.152400><0.826891,0.036000,0.152400> rotate<0,44.997030,0> translate<7.954500,0.000000,10.111100> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<8.539200,0.000000,9.526400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<8.539200,0.000000,8.941700>}
box{<0,0,-0.152400><0.584700,0.036000,0.152400> rotate<0,-90.000000,0> translate<8.539200,0.000000,8.941700> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<8.539200,0.000000,8.941700>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<6.200500,0.000000,8.941700>}
box{<0,0,-0.152400><2.338700,0.036000,0.152400> rotate<0,0.000000,0> translate<6.200500,0.000000,8.941700> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<10.293200,0.000000,7.772400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<11.462600,0.000000,7.772400>}
box{<0,0,-0.152400><1.169400,0.036000,0.152400> rotate<0,0.000000,0> translate<10.293200,0.000000,7.772400> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<11.462600,0.000000,7.772400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<12.047300,0.000000,8.357000>}
box{<0,0,-0.152400><0.826820,0.036000,0.152400> rotate<0,-44.992130,0> translate<11.462600,0.000000,7.772400> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<12.047300,0.000000,8.357000>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<12.047300,0.000000,9.526400>}
box{<0,0,-0.152400><1.169400,0.036000,0.152400> rotate<0,90.000000,0> translate<12.047300,0.000000,9.526400> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<12.047300,0.000000,9.526400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<11.462600,0.000000,10.111100>}
box{<0,0,-0.152400><0.826891,0.036000,0.152400> rotate<0,44.997030,0> translate<11.462600,0.000000,10.111100> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<11.462600,0.000000,10.111100>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<10.293200,0.000000,10.111100>}
box{<0,0,-0.152400><1.169400,0.036000,0.152400> rotate<0,0.000000,0> translate<10.293200,0.000000,10.111100> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<10.293200,0.000000,10.111100>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<9.708600,0.000000,9.526400>}
box{<0,0,-0.152400><0.826820,0.036000,0.152400> rotate<0,-45.001930,0> translate<9.708600,0.000000,9.526400> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<9.708600,0.000000,9.526400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<9.708600,0.000000,8.357000>}
box{<0,0,-0.152400><1.169400,0.036000,0.152400> rotate<0,-90.000000,0> translate<9.708600,0.000000,8.357000> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<9.708600,0.000000,8.357000>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<10.293200,0.000000,7.772400>}
box{<0,0,-0.152400><0.826749,0.036000,0.152400> rotate<0,44.997030,0> translate<9.708600,0.000000,8.357000> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<13.216700,0.000000,7.772400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<13.216700,0.000000,10.111100>}
box{<0,0,-0.152400><2.338700,0.036000,0.152400> rotate<0,90.000000,0> translate<13.216700,0.000000,10.111100> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<13.216700,0.000000,10.111100>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<14.970700,0.000000,10.111100>}
box{<0,0,-0.152400><1.754000,0.036000,0.152400> rotate<0,0.000000,0> translate<13.216700,0.000000,10.111100> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<14.970700,0.000000,10.111100>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<15.555400,0.000000,9.526400>}
box{<0,0,-0.152400><0.826891,0.036000,0.152400> rotate<0,44.997030,0> translate<14.970700,0.000000,10.111100> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<15.555400,0.000000,9.526400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<15.555400,0.000000,7.772400>}
box{<0,0,-0.152400><1.754000,0.036000,0.152400> rotate<0,-90.000000,0> translate<15.555400,0.000000,7.772400> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.854200,0.000000,23.749600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.938800,0.000000,23.749600>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<1.854200,0.000000,23.749600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.396500,0.000000,24.292000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.396500,0.000000,23.207300>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,-90.000000,0> translate<2.396500,0.000000,23.207300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.491300,0.000000,24.020800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<4.033600,0.000000,24.563100>}
box{<0,0,-0.076200><0.766928,0.036000,0.076200> rotate<0,-44.997030,0> translate<3.491300,0.000000,24.020800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<4.033600,0.000000,24.563100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<4.033600,0.000000,22.936200>}
box{<0,0,-0.076200><1.626900,0.036000,0.076200> rotate<0,-90.000000,0> translate<4.033600,0.000000,22.936200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.491300,0.000000,22.936200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<4.575900,0.000000,22.936200>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<3.491300,0.000000,22.936200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.213000,0.000000,22.936200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.128400,0.000000,22.936200>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<5.128400,0.000000,22.936200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.128400,0.000000,22.936200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.213000,0.000000,24.020800>}
box{<0,0,-0.076200><1.533856,0.036000,0.076200> rotate<0,-44.997030,0> translate<5.128400,0.000000,22.936200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.213000,0.000000,24.020800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.213000,0.000000,24.292000>}
box{<0,0,-0.076200><0.271200,0.036000,0.076200> rotate<0,90.000000,0> translate<6.213000,0.000000,24.292000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.213000,0.000000,24.292000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.941800,0.000000,24.563100>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,44.986466,0> translate<5.941800,0.000000,24.563100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.941800,0.000000,24.563100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.399500,0.000000,24.563100>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<5.399500,0.000000,24.563100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.399500,0.000000,24.563100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.128400,0.000000,24.292000>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,-44.997030,0> translate<5.128400,0.000000,24.292000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<44.018200,0.000000,24.003600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.102800,0.000000,24.003600>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<44.018200,0.000000,24.003600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<44.560500,0.000000,24.546000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<44.560500,0.000000,23.461300>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,-90.000000,0> translate<44.560500,0.000000,23.461300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.739900,0.000000,24.817100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.655300,0.000000,24.817100>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<45.655300,0.000000,24.817100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.655300,0.000000,24.817100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.655300,0.000000,24.003600>}
box{<0,0,-0.076200><0.813500,0.036000,0.076200> rotate<0,-90.000000,0> translate<45.655300,0.000000,24.003600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.655300,0.000000,24.003600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.197600,0.000000,24.274800>}
box{<0,0,-0.076200><0.606332,0.036000,0.076200> rotate<0,-26.567524,0> translate<45.655300,0.000000,24.003600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.197600,0.000000,24.274800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.468700,0.000000,24.274800>}
box{<0,0,-0.076200><0.271100,0.036000,0.076200> rotate<0,0.000000,0> translate<46.197600,0.000000,24.274800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.468700,0.000000,24.274800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.739900,0.000000,24.003600>}
box{<0,0,-0.076200><0.383535,0.036000,0.076200> rotate<0,44.997030,0> translate<46.468700,0.000000,24.274800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.739900,0.000000,24.003600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.739900,0.000000,23.461300>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,-90.000000,0> translate<46.739900,0.000000,23.461300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.739900,0.000000,23.461300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.468700,0.000000,23.190200>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-44.986466,0> translate<46.468700,0.000000,23.190200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.468700,0.000000,23.190200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.926400,0.000000,23.190200>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<45.926400,0.000000,23.190200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.926400,0.000000,23.190200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.655300,0.000000,23.461300>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<45.655300,0.000000,23.461300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<44.778900,0.000000,12.573600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<44.778900,0.000000,12.031300>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,-90.000000,0> translate<44.778900,0.000000,12.031300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<44.778900,0.000000,12.031300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.050000,0.000000,11.760200>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<44.778900,0.000000,12.031300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.050000,0.000000,11.760200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.134700,0.000000,11.760200>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,0.000000,0> translate<45.050000,0.000000,11.760200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.134700,0.000000,11.760200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.405800,0.000000,12.031300>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,-44.997030,0> translate<46.134700,0.000000,11.760200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.405800,0.000000,12.031300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.405800,0.000000,12.573600>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,90.000000,0> translate<46.405800,0.000000,12.573600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.405800,0.000000,12.573600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.134700,0.000000,12.844800>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,45.007595,0> translate<46.134700,0.000000,12.844800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.134700,0.000000,12.844800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.050000,0.000000,12.844800>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,0.000000,0> translate<45.050000,0.000000,12.844800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.050000,0.000000,12.844800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<44.778900,0.000000,12.573600>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-45.007595,0> translate<44.778900,0.000000,12.573600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.321200,0.000000,13.397300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.134700,0.000000,13.397300>}
box{<0,0,-0.076200><0.813500,0.036000,0.076200> rotate<0,0.000000,0> translate<45.321200,0.000000,13.397300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.134700,0.000000,13.397300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.405800,0.000000,13.668400>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,-44.997030,0> translate<46.134700,0.000000,13.397300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.405800,0.000000,13.668400>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.405800,0.000000,14.481900>}
box{<0,0,-0.076200><0.813500,0.036000,0.076200> rotate<0,90.000000,0> translate<46.405800,0.000000,14.481900> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.405800,0.000000,14.481900>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.321200,0.000000,14.481900>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<45.321200,0.000000,14.481900> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.050000,0.000000,15.305500>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.134700,0.000000,15.305500>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,0.000000,0> translate<45.050000,0.000000,15.305500> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.134700,0.000000,15.305500>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.405800,0.000000,15.576700>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-45.007595,0> translate<46.134700,0.000000,15.305500> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.321200,0.000000,15.034400>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.321200,0.000000,15.576700>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,90.000000,0> translate<45.321200,0.000000,15.576700> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.511800,0.000000,13.284200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.511800,0.000000,13.826500>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,90.000000,0> translate<5.511800,0.000000,13.826500> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.511800,0.000000,13.555300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.884900,0.000000,13.555300>}
box{<0,0,-0.076200><1.626900,0.036000,0.076200> rotate<0,0.000000,0> translate<3.884900,0.000000,13.555300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.884900,0.000000,13.284200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.884900,0.000000,13.826500>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,90.000000,0> translate<3.884900,0.000000,13.826500> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.511800,0.000000,14.375600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<4.427200,0.000000,14.375600>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<4.427200,0.000000,14.375600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<4.427200,0.000000,14.375600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<4.427200,0.000000,15.189000>}
box{<0,0,-0.076200><0.813400,0.036000,0.076200> rotate<0,90.000000,0> translate<4.427200,0.000000,15.189000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<4.427200,0.000000,15.189000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<4.698400,0.000000,15.460200>}
box{<0,0,-0.076200><0.383535,0.036000,0.076200> rotate<0,-44.997030,0> translate<4.427200,0.000000,15.189000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<4.698400,0.000000,15.460200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.511800,0.000000,15.460200>}
box{<0,0,-0.076200><0.813400,0.036000,0.076200> rotate<0,0.000000,0> translate<4.698400,0.000000,15.460200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.932900,0.000000,35.179600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.932900,0.000000,34.637300>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,-90.000000,0> translate<6.932900,0.000000,34.637300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.932900,0.000000,34.637300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.204000,0.000000,34.366200>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<6.932900,0.000000,34.637300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.204000,0.000000,34.366200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.288700,0.000000,34.366200>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,0.000000,0> translate<7.204000,0.000000,34.366200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.288700,0.000000,34.366200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.559800,0.000000,34.637300>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,-44.997030,0> translate<8.288700,0.000000,34.366200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.559800,0.000000,34.637300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.559800,0.000000,35.179600>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,90.000000,0> translate<8.559800,0.000000,35.179600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.559800,0.000000,35.179600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.288700,0.000000,35.450800>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,45.007595,0> translate<8.288700,0.000000,35.450800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.288700,0.000000,35.450800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.204000,0.000000,35.450800>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,0.000000,0> translate<7.204000,0.000000,35.450800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.204000,0.000000,35.450800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.932900,0.000000,35.179600>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-45.007595,0> translate<6.932900,0.000000,35.179600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.559800,0.000000,36.003300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.475200,0.000000,36.003300>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<7.475200,0.000000,36.003300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.475200,0.000000,36.003300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.475200,0.000000,36.816700>}
box{<0,0,-0.076200><0.813400,0.036000,0.076200> rotate<0,90.000000,0> translate<7.475200,0.000000,36.816700> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.475200,0.000000,36.816700>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.746400,0.000000,37.087900>}
box{<0,0,-0.076200><0.383535,0.036000,0.076200> rotate<0,-44.997030,0> translate<7.475200,0.000000,36.816700> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.746400,0.000000,37.087900>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.559800,0.000000,37.087900>}
box{<0,0,-0.076200><0.813400,0.036000,0.076200> rotate<0,0.000000,0> translate<7.746400,0.000000,37.087900> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<21.918900,0.000000,34.925600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<21.918900,0.000000,34.383300>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,-90.000000,0> translate<21.918900,0.000000,34.383300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<21.918900,0.000000,34.383300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.190000,0.000000,34.112200>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<21.918900,0.000000,34.383300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.190000,0.000000,34.112200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.274700,0.000000,34.112200>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,0.000000,0> translate<22.190000,0.000000,34.112200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.274700,0.000000,34.112200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.545800,0.000000,34.383300>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,-44.997030,0> translate<23.274700,0.000000,34.112200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.545800,0.000000,34.383300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.545800,0.000000,34.925600>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,90.000000,0> translate<23.545800,0.000000,34.925600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.545800,0.000000,34.925600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.274700,0.000000,35.196800>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,45.007595,0> translate<23.274700,0.000000,35.196800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.274700,0.000000,35.196800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.190000,0.000000,35.196800>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,0.000000,0> translate<22.190000,0.000000,35.196800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.190000,0.000000,35.196800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<21.918900,0.000000,34.925600>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-45.007595,0> translate<21.918900,0.000000,34.925600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.545800,0.000000,36.020400>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.190000,0.000000,36.020400>}
box{<0,0,-0.076200><1.355800,0.036000,0.076200> rotate<0,0.000000,0> translate<22.190000,0.000000,36.020400> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.190000,0.000000,36.020400>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<21.918900,0.000000,36.291600>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,45.007595,0> translate<21.918900,0.000000,36.291600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.732400,0.000000,35.749300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.732400,0.000000,36.291600>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,90.000000,0> translate<22.732400,0.000000,36.291600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.545800,0.000000,37.111800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.190000,0.000000,37.111800>}
box{<0,0,-0.076200><1.355800,0.036000,0.076200> rotate<0,0.000000,0> translate<22.190000,0.000000,37.111800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.190000,0.000000,37.111800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<21.918900,0.000000,37.383000>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,45.007595,0> translate<21.918900,0.000000,37.383000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.732400,0.000000,36.840700>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.732400,0.000000,37.383000>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,90.000000,0> translate<22.732400,0.000000,37.383000> }
//C1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.670000,0.000000,20.955000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.670000,0.000000,22.225000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,90.000000,0> translate<26.670000,0.000000,22.225000> }
difference{
cylinder{<25.400000,0,20.320000><25.400000,0.036000,20.320000>3.351600 translate<0,0.000000,0>}
cylinder{<25.400000,-0.1,20.320000><25.400000,0.135000,20.320000>3.148400 translate<0,0.000000,0>}}
//C2 silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.670000,0.000000,10.795000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.670000,0.000000,12.065000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,90.000000,0> translate<26.670000,0.000000,12.065000> }
difference{
cylinder{<25.400000,0,10.160000><25.400000,0.036000,10.160000>3.351600 translate<0,0.000000,0>}
cylinder{<25.400000,-0.1,10.160000><25.400000,0.135000,10.160000>3.148400 translate<0,0.000000,0>}}
//D1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<11.430000,0.000000,28.575000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<11.430000,0.000000,27.305000>}
box{<0,0,-0.127000><1.270000,0.036000,0.127000> rotate<0,-90.000000,0> translate<11.430000,0.000000,27.305000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<11.430000,0.000000,27.305000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<11.430000,0.000000,22.225000>}
box{<0,0,-0.127000><5.080000,0.036000,0.127000> rotate<0,-90.000000,0> translate<11.430000,0.000000,22.225000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<11.430000,0.000000,22.225000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<12.700000,0.000000,22.225000>}
box{<0,0,-0.127000><1.270000,0.036000,0.127000> rotate<0,0.000000,0> translate<11.430000,0.000000,22.225000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<12.700000,0.000000,22.225000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<13.970000,0.000000,22.225000>}
box{<0,0,-0.127000><1.270000,0.036000,0.127000> rotate<0,0.000000,0> translate<12.700000,0.000000,22.225000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<13.970000,0.000000,22.225000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<13.970000,0.000000,27.305000>}
box{<0,0,-0.127000><5.080000,0.036000,0.127000> rotate<0,90.000000,0> translate<13.970000,0.000000,27.305000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<13.970000,0.000000,27.305000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<13.970000,0.000000,28.575000>}
box{<0,0,-0.127000><1.270000,0.036000,0.127000> rotate<0,90.000000,0> translate<13.970000,0.000000,28.575000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<13.970000,0.000000,28.575000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<12.700000,0.000000,28.575000>}
box{<0,0,-0.127000><1.270000,0.036000,0.127000> rotate<0,0.000000,0> translate<12.700000,0.000000,28.575000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<12.700000,0.000000,28.575000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<11.430000,0.000000,28.575000>}
box{<0,0,-0.127000><1.270000,0.036000,0.127000> rotate<0,0.000000,0> translate<11.430000,0.000000,28.575000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<11.430000,0.000000,27.305000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<13.970000,0.000000,27.305000>}
box{<0,0,-0.127000><2.540000,0.036000,0.127000> rotate<0,0.000000,0> translate<11.430000,0.000000,27.305000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<12.700000,0.000000,22.225000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<12.700000,0.000000,21.590000>}
box{<0,0,-0.127000><0.635000,0.036000,0.127000> rotate<0,-90.000000,0> translate<12.700000,0.000000,21.590000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<12.700000,0.000000,28.575000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<12.700000,0.000000,29.210000>}
box{<0,0,-0.127000><0.635000,0.036000,0.127000> rotate<0,90.000000,0> translate<12.700000,0.000000,29.210000> }
//IC1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<30.353000,0.000000,31.750000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<40.767000,0.000000,31.750000>}
box{<0,0,-0.076200><10.414000,0.036000,0.076200> rotate<0,0.000000,0> translate<30.353000,0.000000,31.750000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<30.988000,0.000000,32.385000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<40.132000,0.000000,32.385000>}
box{<0,0,-0.025400><9.144000,0.036000,0.025400> rotate<0,0.000000,0> translate<30.988000,0.000000,32.385000> }
box{<-0.635000,0,-0.444500><0.635000,0.036000,0.444500> rotate<0,-0.000000,0> translate<38.100000,0.000000,31.305500>}
box{<-0.635000,0,-0.825500><0.635000,0.036000,0.825500> rotate<0,-0.000000,0> translate<38.100000,0.000000,30.035500>}
box{<-0.635000,0,-0.444500><0.635000,0.036000,0.444500> rotate<0,-0.000000,0> translate<35.560000,0.000000,31.305500>}
box{<-0.635000,0,-0.444500><0.635000,0.036000,0.444500> rotate<0,-0.000000,0> translate<33.020000,0.000000,31.305500>}
box{<-0.635000,0,-0.825500><0.635000,0.036000,0.825500> rotate<0,-0.000000,0> translate<35.560000,0.000000,30.035500>}
box{<-0.635000,0,-0.825500><0.635000,0.036000,0.825500> rotate<0,-0.000000,0> translate<33.020000,0.000000,30.035500>}
//LED1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<36.195000,0.000000,5.080000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<40.005000,0.000000,5.080000>}
box{<0,0,-0.101600><3.810000,0.036000,0.101600> rotate<0,0.000000,0> translate<36.195000,0.000000,5.080000> }
object{ARC(3.175000,0.254000,306.869898,593.130102,0.036000) translate<38.100000,0.000000,7.620000>}
object{ARC(1.143000,0.152400,0.000000,90.000000,0.036000) translate<38.100000,0.000000,7.620000>}
object{ARC(1.143000,0.152400,180.000000,270.000000,0.036000) translate<38.100000,0.000000,7.620000>}
object{ARC(1.651000,0.152400,0.000000,90.000000,0.036000) translate<38.100000,0.000000,7.620000>}
object{ARC(1.651000,0.152400,180.000000,270.000000,0.036000) translate<38.100000,0.000000,7.620000>}
object{ARC(2.159000,0.152400,0.000000,90.000000,0.036000) translate<38.100000,0.000000,7.620000>}
object{ARC(2.159000,0.152400,180.000000,270.000000,0.036000) translate<38.100000,0.000000,7.620000>}
difference{
cylinder{<38.100000,0,7.620000><38.100000,0.036000,7.620000>2.616200 translate<0,0.000000,0>}
cylinder{<38.100000,-0.1,7.620000><38.100000,0.135000,7.620000>2.463800 translate<0,0.000000,0>}}
//PTC silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.304000,0.000000,29.210000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.304000,0.000000,21.590000>}
box{<0,0,-0.101600><7.620000,0.036000,0.101600> rotate<0,-90.000000,0> translate<19.304000,0.000000,21.590000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.304000,0.000000,21.590000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<16.256000,0.000000,21.590000>}
box{<0,0,-0.101600><3.048000,0.036000,0.101600> rotate<0,0.000000,0> translate<16.256000,0.000000,21.590000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<16.256000,0.000000,21.590000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<16.256000,0.000000,29.210000>}
box{<0,0,-0.101600><7.620000,0.036000,0.101600> rotate<0,90.000000,0> translate<16.256000,0.000000,29.210000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<16.256000,0.000000,29.210000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.304000,0.000000,29.210000>}
box{<0,0,-0.101600><3.048000,0.036000,0.101600> rotate<0,0.000000,0> translate<16.256000,0.000000,29.210000> }
//R1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<39.300000,0.000000,17.170000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<36.900000,0.000000,17.170000>}
box{<0,0,-0.101600><2.400000,0.036000,0.101600> rotate<0,0.000000,0> translate<36.900000,0.000000,17.170000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<36.900000,0.000000,17.170000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<36.900000,0.000000,23.470000>}
box{<0,0,-0.101600><6.300000,0.036000,0.101600> rotate<0,90.000000,0> translate<36.900000,0.000000,23.470000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<36.900000,0.000000,23.470000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<39.300000,0.000000,23.470000>}
box{<0,0,-0.101600><2.400000,0.036000,0.101600> rotate<0,0.000000,0> translate<36.900000,0.000000,23.470000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<39.300000,0.000000,23.470000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<39.300000,0.000000,17.170000>}
box{<0,0,-0.101600><6.300000,0.036000,0.101600> rotate<0,-90.000000,0> translate<39.300000,0.000000,17.170000> }
//S1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.425000,0.000000,37.735000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.425000,0.000000,33.385000>}
box{<0,0,-0.101600><4.350000,0.036000,0.101600> rotate<0,-90.000000,0> translate<9.425000,0.000000,33.385000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.425000,0.000000,33.385000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.055000,0.000000,33.385000>}
box{<0,0,-0.101600><11.630000,0.036000,0.101600> rotate<0,0.000000,0> translate<9.425000,0.000000,33.385000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.055000,0.000000,33.385000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.055000,0.000000,37.735000>}
box{<0,0,-0.101600><4.350000,0.036000,0.101600> rotate<0,90.000000,0> translate<21.055000,0.000000,37.735000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.055000,0.000000,37.735000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.425000,0.000000,37.735000>}
box{<0,0,-0.101600><11.630000,0.036000,0.101600> rotate<0,0.000000,0> translate<9.425000,0.000000,37.735000> }
box{<-0.228600,0,-0.304800><0.228600,0.036000,0.304800> rotate<0,-90.000000,0> translate<15.240000,0.000000,35.560000>}
box{<-0.228600,0,-0.304800><0.228600,0.036000,0.304800> rotate<0,-90.000000,0> translate<12.700000,0.000000,35.560000>}
box{<-0.228600,0,-0.304800><0.228600,0.036000,0.304800> rotate<0,-90.000000,0> translate<17.780000,0.000000,35.560000>}
//VIN silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.810000,0.000000,17.145000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.810000,0.000000,18.415000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,90.000000,0> translate<3.810000,0.000000,18.415000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.810000,0.000000,18.415000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<4.445000,0.000000,19.050000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<3.810000,0.000000,18.415000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<5.715000,0.000000,19.050000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<6.350000,0.000000,18.415000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<5.715000,0.000000,19.050000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<4.445000,0.000000,19.050000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.810000,0.000000,19.685000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<3.810000,0.000000,19.685000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.810000,0.000000,19.685000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.810000,0.000000,20.955000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,90.000000,0> translate<3.810000,0.000000,20.955000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.810000,0.000000,20.955000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<4.445000,0.000000,21.590000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<3.810000,0.000000,20.955000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<5.715000,0.000000,21.590000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<6.350000,0.000000,20.955000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<5.715000,0.000000,21.590000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<6.350000,0.000000,20.955000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<6.350000,0.000000,19.685000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,-90.000000,0> translate<6.350000,0.000000,19.685000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<6.350000,0.000000,19.685000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<5.715000,0.000000,19.050000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<5.715000,0.000000,19.050000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<4.445000,0.000000,16.510000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<5.715000,0.000000,16.510000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<4.445000,0.000000,16.510000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.810000,0.000000,17.145000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<4.445000,0.000000,16.510000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<3.810000,0.000000,17.145000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<5.715000,0.000000,16.510000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<6.350000,0.000000,17.145000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<5.715000,0.000000,16.510000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<6.350000,0.000000,18.415000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<6.350000,0.000000,17.145000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,-90.000000,0> translate<6.350000,0.000000,17.145000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<4.445000,0.000000,21.590000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<5.715000,0.000000,21.590000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<4.445000,0.000000,21.590000> }
box{<-0.292100,0,-0.292100><0.292100,0.036000,0.292100> rotate<0,-90.000000,0> translate<5.080000,0.000000,17.780000>}
box{<-0.292100,0,-0.292100><0.292100,0.036000,0.292100> rotate<0,-90.000000,0> translate<5.080000,0.000000,20.320000>}
//VOUT silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<46.990000,0.000000,20.955000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<46.990000,0.000000,19.685000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,-90.000000,0> translate<46.990000,0.000000,19.685000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<46.990000,0.000000,19.685000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<46.355000,0.000000,19.050000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<46.355000,0.000000,19.050000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<45.085000,0.000000,19.050000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<44.450000,0.000000,19.685000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<44.450000,0.000000,19.685000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<46.355000,0.000000,19.050000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<46.990000,0.000000,18.415000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<46.355000,0.000000,19.050000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<46.990000,0.000000,18.415000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<46.990000,0.000000,17.145000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,-90.000000,0> translate<46.990000,0.000000,17.145000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<46.990000,0.000000,17.145000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<46.355000,0.000000,16.510000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<46.355000,0.000000,16.510000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<45.085000,0.000000,16.510000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<44.450000,0.000000,17.145000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<44.450000,0.000000,17.145000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<44.450000,0.000000,17.145000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<44.450000,0.000000,18.415000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,90.000000,0> translate<44.450000,0.000000,18.415000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<44.450000,0.000000,18.415000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<45.085000,0.000000,19.050000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<44.450000,0.000000,18.415000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<46.355000,0.000000,21.590000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<45.085000,0.000000,21.590000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<45.085000,0.000000,21.590000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<46.990000,0.000000,20.955000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<46.355000,0.000000,21.590000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<46.355000,0.000000,21.590000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<45.085000,0.000000,21.590000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<44.450000,0.000000,20.955000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<44.450000,0.000000,20.955000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<44.450000,0.000000,19.685000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<44.450000,0.000000,20.955000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,90.000000,0> translate<44.450000,0.000000,20.955000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<46.355000,0.000000,16.510000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<45.085000,0.000000,16.510000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<45.085000,0.000000,16.510000> }
box{<-0.292100,0,-0.292100><0.292100,0.036000,0.292100> rotate<0,-270.000000,0> translate<45.720000,0.000000,20.320000>}
box{<-0.292100,0,-0.292100><0.292100,0.036000,0.292100> rotate<0,-270.000000,0> translate<45.720000,0.000000,17.780000>}
texture{col_slk}
}
#end
translate<mac_x_ver,mac_y_ver,mac_z_ver>
rotate<mac_x_rot,mac_y_rot,mac_z_rot>
}//End union
#end

#if(use_file_as_inc=off)
object{  ZV_POWER(-25.400000,0,-19.050000,pcb_rotate_x,pcb_rotate_y,pcb_rotate_z)
#if(pcb_upsidedown=on)
rotate pcb_rotdir*180
#end
}
#end


//Parts not found in 3dpack.dat or 3dusrpac.dat are:
