//POVRay-File created by 3d41.ulp v20110101
///home/salvix/Dev/vcc_dev/5v_power_cheap/5_power_cheap.brd
//1/9/13 10:50 PM

#version 3.5;

//Set to on if the file should be used as .inc
#local use_file_as_inc = off;
#if(use_file_as_inc=off)


//changes the apperance of resistors (1 Blob / 0 real)
#declare global_res_shape = 1;
//randomize color of resistors 1=random 0=same color
#declare global_res_colselect = 0;
//Number of the color for the resistors
//0=Green, 1="normal color" 2=Blue 3=Brown
#declare global_res_col = 1;
//Set to on if you want to render the PCB upside-down
#declare pcb_upsidedown = off;
//Set to x or z to rotate around the corresponding axis (referring to pcb_upsidedown)
#declare pcb_rotdir = x;
//Set the length off short pins over the PCB
#declare pin_length = 2.5;
#declare global_diode_bend_radius = 1;
#declare global_res_bend_radius = 1;
#declare global_solder = on;

#declare global_show_screws = on;
#declare global_show_washers = on;
#declare global_show_nuts = on;

#declare global_use_radiosity = on;

#declare global_ambient_mul = 1;
#declare global_ambient_mul_emit = 0;

//Animation
#declare global_anim = off;
#local global_anim_showcampath = no;

#declare global_fast_mode = off;

#declare col_preset = 2;
#declare pin_short = on;

#declare e3d_environment = off;

#local cam_x = 0;
#local cam_y = 109;
#local cam_z = -58;
#local cam_a = 20;
#local cam_look_x = 0;
#local cam_look_y = -2;
#local cam_look_z = 0;

#local pcb_rotate_x = 0;
#local pcb_rotate_y = 0;
#local pcb_rotate_z = 0;

#local pcb_board = on;
#local pcb_parts = on;
#local pcb_wire_bridges = off;
#if(global_fast_mode=off)
	#local pcb_polygons = on;
	#local pcb_silkscreen = on;
	#local pcb_wires = on;
	#local pcb_pads_smds = on;
#else
	#local pcb_polygons = off;
	#local pcb_silkscreen = off;
	#local pcb_wires = off;
	#local pcb_pads_smds = off;
#end

#local lgt1_pos_x = 11;
#local lgt1_pos_y = 17;
#local lgt1_pos_z = 13;
#local lgt1_intense = 0.711060;
#local lgt2_pos_x = -11;
#local lgt2_pos_y = 17;
#local lgt2_pos_z = 13;
#local lgt2_intense = 0.711060;
#local lgt3_pos_x = 11;
#local lgt3_pos_y = 17;
#local lgt3_pos_z = -9;
#local lgt3_intense = 0.711060;
#local lgt4_pos_x = -11;
#local lgt4_pos_y = 17;
#local lgt4_pos_z = -9;
#local lgt4_intense = 0.711060;

//Do not change these values
#declare pcb_height = 1.500000;
#declare pcb_cuheight = 0.035000;
#declare pcb_x_size = 30.480000;
#declare pcb_y_size = 25.400000;
#declare pcb_layer1_used = 0;
#declare pcb_layer16_used = 1;
#declare inc_testmode = off;
#declare global_seed=seed(87);
#declare global_pcb_layer_dis = array[16]
{
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	1.535000,
}
#declare global_pcb_real_hole = 2.000000;

#include "e3d_tools.inc"
#include "e3d_user.inc"

global_settings{charset utf8}

#if(e3d_environment=on)
sky_sphere {pigment {Navy}
pigment {bozo turbulence 0.65 octaves 7 omega 0.7 lambda 2
color_map {
[0.0 0.1 color rgb <0.85, 0.85, 0.85> color rgb <0.75, 0.75, 0.75>]
[0.1 0.5 color rgb <0.75, 0.75, 0.75> color rgbt <1, 1, 1, 1>]
[0.5 1.0 color rgbt <1, 1, 1, 1> color rgbt <1, 1, 1, 1>]}
scale <0.1, 0.5, 0.1>} rotate -90*x}
plane{y, -10.0-max(pcb_x_size,pcb_y_size)*abs(max(sin((pcb_rotate_x/180)*pi),sin((pcb_rotate_z/180)*pi)))
texture{T_Chrome_2D
normal{waves 0.1 frequency 3000.0 scale 3000.0}} translate<0,0,0>}
#end

//Animation data
#if(global_anim=on)
#declare global_anim_showcampath = no;
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#declare global_anim_npoints_cam_flight=0;
#warning "No/not enough Animation Data available (min. 3 points) (Flight path)"
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#declare global_anim_npoints_cam_view=0;
#warning "No/not enough Animation Data available (min. 3 points) (View path)"
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#end

#if((global_anim_showcampath=yes)&(global_anim=off))
#end
#if(global_anim=on)
camera
{
	location global_anim_spline_cam_flight(clock)
	#if(global_anim_npoints_cam_view>2)
		look_at global_anim_spline_cam_view(clock)
	#else
		look_at global_anim_spline_cam_flight(clock+0.01)-<0,-0.01,0>
	#end
	angle 45
}
light_source
{
	global_anim_spline_cam_flight(clock)
	color rgb <1,1,1>
	spotlight point_at 
	#if(global_anim_npoints_cam_view>2)
		global_anim_spline_cam_view(clock)
	#else
		global_anim_spline_cam_flight(clock+0.01)-<0,-0.01,0>
	#end
	radius 35 falloff  40
}
#else
camera
{
	location <cam_x,cam_y,cam_z>
	look_at <cam_look_x,cam_look_y,cam_look_z>
	angle cam_a
	//translates the camera that <0,0,0> is over the Eagle <0,0>
	//translate<-15.240000,0,-12.700000>
}
#end

background{col_bgr}
light_source{<lgt1_pos_x,lgt1_pos_y,lgt1_pos_z> White*lgt1_intense}
light_source{<lgt2_pos_x,lgt2_pos_y,lgt2_pos_z> White*lgt2_intense}
light_source{<lgt3_pos_x,lgt3_pos_y,lgt3_pos_z> White*lgt3_intense}
light_source{<lgt4_pos_x,lgt4_pos_y,lgt4_pos_z> White*lgt4_intense}
#end


#macro Z_POWER_CHEAP(mac_x_ver,mac_y_ver,mac_z_ver,mac_x_rot,mac_y_rot,mac_z_rot)
union{
#if(pcb_board = on)
difference{
union{
//Board
prism{-1.500000,0.000000,8
<0.000000,0.000000><30.480000,0.000000>
<30.480000,0.000000><30.480000,25.400000>
<30.480000,25.400000><0.000000,25.400000>
<0.000000,25.400000><0.000000,0.000000>
texture{col_brd}}
}//End union(PCB)
//Holes(real)/Parts
//Holes(real)/Board
//Holes(real)/Vias
}//End difference(reale Bohrungen/Durchbrüche)
#end
#if(pcb_parts=on)//Parts
union{
#ifndef(pack_INPUT) #declare global_pack_INPUT=yes; object {CON_PH_1X2()translate<0,0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,0> translate<2.540000,0.000000,7.620000>}#end		//Header 2,54mm Grid 2Pin 1Row (jumper.lib) INPUT +7V 1X02_LOCK
#ifndef(pack_L) #declare global_pack_L=yes; object {DIODE_DIS_LED_5MM(Green,0.300000,0.000000,)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,0> translate<22.860000,0.000000,22.860000>}#end		//Diskrete 5MM LED L GREEN LED5MM
#ifndef(pack_OUTPUT) #declare global_pack_OUTPUT=yes; object {CON_PH_1X2()translate<0,0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,0> translate<27.940000,0.000000,10.160000>}#end		//Header 2,54mm Grid 2Pin 1Row (jumper.lib) OUTPUT +5V 1X02_LOCK
#ifndef(pack_R1) #declare global_pack_R1=yes; object {RES_DIS_0204_075MM(texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture {T_Gold_5C finish{reflection 0.1}},)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,0> translate<10.160000,0.000000,10.160000>}#end		//Discrete Resistor 0,15W 7,5MM Grid R1  AXIAL-0.3
#ifndef(pack_R2) #declare global_pack_R2=yes; object {RES_DIS_0204_075MM(texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture {T_Gold_5C finish{reflection 0.1}},)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,0> translate<15.240000,0.000000,10.160000>}#end		//Discrete Resistor 0,15W 7,5MM Grid R2  AXIAL-0.3
#ifndef(pack_RL) #declare global_pack_RL=yes; object {RES_DIS_0204_075MM(texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture {T_Gold_5C finish{reflection 0.1}},)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,0> translate<20.320000,0.000000,10.160000>}#end		//Discrete Resistor 0,15W 7,5MM Grid RL  AXIAL-0.3
#ifndef(pack_S1) #declare global_pack_S1=yes; object {SWITCH_SECME_1K2_SH()translate<0,0,0> rotate<0,0.000000,0>rotate<0,0,0> rotate<0,0,0> translate<10.160000,0.000000,22.860000>}#end		//Switch 1K2 Straight High actuator S1  SWITCH-SPDT_KIT
}//End union
#end
#if(pcb_pads_smds=on)
//Pads&SMD/Parts
#ifndef(global_pack_INPUT) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-180.000000,0>translate<2.540000,0,7.442200> texture{col_thl}}
#ifndef(global_pack_INPUT) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-180.000000,0>translate<2.540000,0,10.337800> texture{col_thl}}
#ifndef(global_pack_L) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,2+global_tmp,0) rotate<0,-180.000000,0>translate<24.130000,0,22.860000> texture{col_thl}}
#ifndef(global_pack_L) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,2+global_tmp,0) rotate<0,-180.000000,0>translate<21.590000,0,22.860000> texture{col_thl}}
#ifndef(global_pack_OUTPUT) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-0.000000,0>translate<27.940000,0,10.337800> texture{col_thl}}
#ifndef(global_pack_OUTPUT) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-0.000000,0>translate<27.940000,0,7.442200> texture{col_thl}}
#ifndef(global_pack_R1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<10.160000,0,13.970000> texture{col_thl}}
#ifndef(global_pack_R1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<10.160000,0,6.350000> texture{col_thl}}
#ifndef(global_pack_R2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<15.240000,0,13.970000> texture{col_thl}}
#ifndef(global_pack_R2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<15.240000,0,6.350000> texture{col_thl}}
#ifndef(global_pack_RL) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-90.000000,0>translate<20.320000,0,6.350000> texture{col_thl}}
#ifndef(global_pack_RL) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-90.000000,0>translate<20.320000,0,13.970000> texture{col_thl}}
#ifndef(global_pack_S1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-90.000000,0>translate<7.442200,0,22.860000> texture{col_thl}}
#ifndef(global_pack_S1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-90.000000,0>translate<10.160000,0,22.860000> texture{col_thl}}
#ifndef(global_pack_S1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-90.000000,0>translate<12.877800,0,22.860000> texture{col_thl}}
//Pads/Vias
#end
#if(pcb_wires=on)
union{
//Signals
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.540000,-1.535000,7.442200>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.540000,-1.535000,7.620000>}
box{<0,0,-0.254000><0.177800,0.035000,0.254000> rotate<0,90.000000,0> translate<2.540000,-1.535000,7.620000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.540000,-1.535000,10.337800>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.540000,-1.535000,10.160000>}
box{<0,0,-0.254000><0.177800,0.035000,0.254000> rotate<0,-90.000000,0> translate<2.540000,-1.535000,10.160000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.540000,-1.535000,10.337800>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.540000,-1.535000,10.160000>}
box{<0,0,-0.254000><0.177800,0.035000,0.254000> rotate<0,-90.000000,0> translate<2.540000,-1.535000,10.160000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.540000,-1.535000,10.337800>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.540000,-1.535000,15.240000>}
box{<0,0,-0.254000><4.902200,0.035000,0.254000> rotate<0,90.000000,0> translate<2.540000,-1.535000,15.240000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.540000,-1.535000,7.620000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.350000,-1.535000,7.620000>}
box{<0,0,-0.254000><3.810000,0.035000,0.254000> rotate<0,0.000000,0> translate<2.540000,-1.535000,7.620000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.350000,-1.535000,7.620000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<8.890000,-1.535000,7.620000>}
box{<0,0,-0.254000><2.540000,0.035000,0.254000> rotate<0,0.000000,0> translate<6.350000,-1.535000,7.620000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.350000,-1.535000,7.620000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<10.160000,-1.535000,3.810000>}
box{<0,0,-0.254000><5.388154,0.035000,0.254000> rotate<0,44.997030,0> translate<6.350000,-1.535000,7.620000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<10.160000,-1.535000,13.970000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<10.160000,-1.535000,20.320000>}
box{<0,0,-0.254000><6.350000,0.035000,0.254000> rotate<0,90.000000,0> translate<10.160000,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.540000,-1.535000,15.240000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<10.160000,-1.535000,22.860000>}
box{<0,0,-0.254000><10.776307,0.035000,0.254000> rotate<0,-44.997030,0> translate<2.540000,-1.535000,15.240000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<10.160000,-1.535000,20.320000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<12.700000,-1.535000,22.860000>}
box{<0,0,-0.254000><3.592102,0.035000,0.254000> rotate<0,-44.997030,0> translate<10.160000,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<12.700000,-1.535000,22.860000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<12.877800,-1.535000,22.860000>}
box{<0,0,-0.254000><0.177800,0.035000,0.254000> rotate<0,0.000000,0> translate<12.700000,-1.535000,22.860000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<10.160000,-1.535000,3.810000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<15.240000,-1.535000,3.810000>}
box{<0,0,-0.254000><5.080000,0.035000,0.254000> rotate<0,0.000000,0> translate<10.160000,-1.535000,3.810000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<10.160000,-1.535000,6.350000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<15.240000,-1.535000,6.350000>}
box{<0,0,-0.254000><5.080000,0.035000,0.254000> rotate<0,0.000000,0> translate<10.160000,-1.535000,6.350000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<8.890000,-1.535000,7.620000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<15.240000,-1.535000,13.970000>}
box{<0,0,-0.254000><8.980256,0.035000,0.254000> rotate<0,-44.997030,0> translate<8.890000,-1.535000,7.620000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<15.240000,-1.535000,3.810000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<17.780000,-1.535000,6.350000>}
box{<0,0,-0.254000><3.592102,0.035000,0.254000> rotate<0,-44.997030,0> translate<15.240000,-1.535000,3.810000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<17.780000,-1.535000,6.350000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<20.320000,-1.535000,6.350000>}
box{<0,0,-0.254000><2.540000,0.035000,0.254000> rotate<0,0.000000,0> translate<17.780000,-1.535000,6.350000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<20.320000,-1.535000,13.970000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<21.590000,-1.535000,15.240000>}
box{<0,0,-0.254000><1.796051,0.035000,0.254000> rotate<0,-44.997030,0> translate<20.320000,-1.535000,13.970000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<21.590000,-1.535000,15.240000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<21.590000,-1.535000,22.860000>}
box{<0,0,-0.254000><7.620000,0.035000,0.254000> rotate<0,90.000000,0> translate<21.590000,-1.535000,22.860000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<24.130000,-1.535000,15.240000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<24.130000,-1.535000,13.970000>}
box{<0,0,-0.254000><1.270000,0.035000,0.254000> rotate<0,-90.000000,0> translate<24.130000,-1.535000,13.970000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<15.240000,-1.535000,6.350000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<24.130000,-1.535000,15.240000>}
box{<0,0,-0.254000><12.572359,0.035000,0.254000> rotate<0,-44.997030,0> translate<15.240000,-1.535000,6.350000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<24.130000,-1.535000,15.240000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<24.130000,-1.535000,22.860000>}
box{<0,0,-0.254000><7.620000,0.035000,0.254000> rotate<0,90.000000,0> translate<24.130000,-1.535000,22.860000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<20.320000,-1.535000,6.350000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<26.670000,-1.535000,6.350000>}
box{<0,0,-0.254000><6.350000,0.035000,0.254000> rotate<0,0.000000,0> translate<20.320000,-1.535000,6.350000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<27.940000,-1.535000,7.620000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<27.940000,-1.535000,7.442200>}
box{<0,0,-0.254000><0.177800,0.035000,0.254000> rotate<0,-90.000000,0> translate<27.940000,-1.535000,7.442200> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<26.670000,-1.535000,6.350000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<27.940000,-1.535000,7.620000>}
box{<0,0,-0.254000><1.796051,0.035000,0.254000> rotate<0,-44.997030,0> translate<26.670000,-1.535000,6.350000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<24.130000,-1.535000,13.970000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<27.940000,-1.535000,10.160000>}
box{<0,0,-0.254000><5.388154,0.035000,0.254000> rotate<0,44.997030,0> translate<24.130000,-1.535000,13.970000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<27.940000,-1.535000,10.160000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<27.940000,-1.535000,10.337800>}
box{<0,0,-0.254000><0.177800,0.035000,0.254000> rotate<0,90.000000,0> translate<27.940000,-1.535000,10.337800> }
//Text
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<7.035800,-1.535000,2.684800>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<6.493500,-1.535000,3.227100>}
box{<0,0,-0.076200><0.766928,0.035000,0.076200> rotate<0,44.997030,0> translate<6.493500,-1.535000,3.227100> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<6.493500,-1.535000,3.227100>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<6.493500,-1.535000,1.600200>}
box{<0,0,-0.076200><1.626900,0.035000,0.076200> rotate<0,-90.000000,0> translate<6.493500,-1.535000,1.600200> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<7.035800,-1.535000,1.600200>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<5.951200,-1.535000,1.600200>}
box{<0,0,-0.076200><1.084600,0.035000,0.076200> rotate<0,0.000000,0> translate<5.951200,-1.535000,1.600200> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<5.398700,-1.535000,2.956000>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<5.127600,-1.535000,3.227100>}
box{<0,0,-0.076200><0.383393,0.035000,0.076200> rotate<0,44.997030,0> translate<5.127600,-1.535000,3.227100> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<5.127600,-1.535000,3.227100>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<4.585300,-1.535000,3.227100>}
box{<0,0,-0.076200><0.542300,0.035000,0.076200> rotate<0,0.000000,0> translate<4.585300,-1.535000,3.227100> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<4.585300,-1.535000,3.227100>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<4.314100,-1.535000,2.956000>}
box{<0,0,-0.076200><0.383464,0.035000,0.076200> rotate<0,-44.986466,0> translate<4.314100,-1.535000,2.956000> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<4.314100,-1.535000,2.956000>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<4.314100,-1.535000,2.684800>}
box{<0,0,-0.076200><0.271200,0.035000,0.076200> rotate<0,-90.000000,0> translate<4.314100,-1.535000,2.684800> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<4.314100,-1.535000,2.684800>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<4.585300,-1.535000,2.413600>}
box{<0,0,-0.076200><0.383535,0.035000,0.076200> rotate<0,44.997030,0> translate<4.314100,-1.535000,2.684800> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<4.585300,-1.535000,2.413600>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<4.856400,-1.535000,2.413600>}
box{<0,0,-0.076200><0.271100,0.035000,0.076200> rotate<0,0.000000,0> translate<4.585300,-1.535000,2.413600> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<4.585300,-1.535000,2.413600>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<4.314100,-1.535000,2.142500>}
box{<0,0,-0.076200><0.383464,0.035000,0.076200> rotate<0,-44.986466,0> translate<4.314100,-1.535000,2.142500> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<4.314100,-1.535000,2.142500>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<4.314100,-1.535000,1.871300>}
box{<0,0,-0.076200><0.271200,0.035000,0.076200> rotate<0,-90.000000,0> translate<4.314100,-1.535000,1.871300> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<4.314100,-1.535000,1.871300>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<4.585300,-1.535000,1.600200>}
box{<0,0,-0.076200><0.383464,0.035000,0.076200> rotate<0,44.986466,0> translate<4.314100,-1.535000,1.871300> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<4.585300,-1.535000,1.600200>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<5.127600,-1.535000,1.600200>}
box{<0,0,-0.076200><0.542300,0.035000,0.076200> rotate<0,0.000000,0> translate<4.585300,-1.535000,1.600200> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<5.127600,-1.535000,1.600200>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<5.398700,-1.535000,1.871300>}
box{<0,0,-0.076200><0.383393,0.035000,0.076200> rotate<0,-44.997030,0> translate<5.127600,-1.535000,1.600200> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<3.761600,-1.535000,1.871300>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<3.761600,-1.535000,2.956000>}
box{<0,0,-0.076200><1.084700,0.035000,0.076200> rotate<0,90.000000,0> translate<3.761600,-1.535000,2.956000> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<3.761600,-1.535000,2.956000>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<3.490500,-1.535000,3.227100>}
box{<0,0,-0.076200><0.383393,0.035000,0.076200> rotate<0,44.997030,0> translate<3.490500,-1.535000,3.227100> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<3.490500,-1.535000,3.227100>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<2.948200,-1.535000,3.227100>}
box{<0,0,-0.076200><0.542300,0.035000,0.076200> rotate<0,0.000000,0> translate<2.948200,-1.535000,3.227100> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<2.948200,-1.535000,3.227100>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<2.677000,-1.535000,2.956000>}
box{<0,0,-0.076200><0.383464,0.035000,0.076200> rotate<0,-44.986466,0> translate<2.677000,-1.535000,2.956000> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<2.677000,-1.535000,2.956000>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<2.677000,-1.535000,1.871300>}
box{<0,0,-0.076200><1.084700,0.035000,0.076200> rotate<0,-90.000000,0> translate<2.677000,-1.535000,1.871300> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<2.677000,-1.535000,1.871300>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<2.948200,-1.535000,1.600200>}
box{<0,0,-0.076200><0.383464,0.035000,0.076200> rotate<0,44.986466,0> translate<2.677000,-1.535000,1.871300> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<2.948200,-1.535000,1.600200>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<3.490500,-1.535000,1.600200>}
box{<0,0,-0.076200><0.542300,0.035000,0.076200> rotate<0,0.000000,0> translate<2.948200,-1.535000,1.600200> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<3.490500,-1.535000,1.600200>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<3.761600,-1.535000,1.871300>}
box{<0,0,-0.076200><0.383393,0.035000,0.076200> rotate<0,-44.997030,0> translate<3.490500,-1.535000,1.600200> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<3.761600,-1.535000,1.871300>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<2.677000,-1.535000,2.956000>}
box{<0,0,-0.076200><1.533927,0.035000,0.076200> rotate<0,44.999671,0> translate<2.677000,-1.535000,2.956000> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<2.124500,-1.535000,2.684800>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<1.582200,-1.535000,3.227100>}
box{<0,0,-0.076200><0.766928,0.035000,0.076200> rotate<0,44.997030,0> translate<1.582200,-1.535000,3.227100> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<1.582200,-1.535000,3.227100>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<1.582200,-1.535000,1.600200>}
box{<0,0,-0.076200><1.626900,0.035000,0.076200> rotate<0,-90.000000,0> translate<1.582200,-1.535000,1.600200> }
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<2.124500,-1.535000,1.600200>}
cylinder{<0,0,0><0,0.035000,0>0.076200 translate<1.039900,-1.535000,1.600200>}
box{<0,0,-0.076200><1.084600,0.035000,0.076200> rotate<0,0.000000,0> translate<1.039900,-1.535000,1.600200> }
//Rect
union{
texture{col_pds}
}
texture{col_wrs}
}
#end
#if(pcb_polygons=on)
union{
//Polygons
texture{col_pol}
}
#end
union{
cylinder{<2.540000,0.038000,7.442200><2.540000,-1.538000,7.442200>0.508000}
cylinder{<2.540000,0.038000,10.337800><2.540000,-1.538000,10.337800>0.508000}
cylinder{<24.130000,0.038000,22.860000><24.130000,-1.538000,22.860000>0.406400}
cylinder{<21.590000,0.038000,22.860000><21.590000,-1.538000,22.860000>0.406400}
cylinder{<27.940000,0.038000,10.337800><27.940000,-1.538000,10.337800>0.508000}
cylinder{<27.940000,0.038000,7.442200><27.940000,-1.538000,7.442200>0.508000}
cylinder{<10.160000,0.038000,13.970000><10.160000,-1.538000,13.970000>0.450000}
cylinder{<10.160000,0.038000,6.350000><10.160000,-1.538000,6.350000>0.450000}
cylinder{<15.240000,0.038000,13.970000><15.240000,-1.538000,13.970000>0.450000}
cylinder{<15.240000,0.038000,6.350000><15.240000,-1.538000,6.350000>0.450000}
cylinder{<20.320000,0.038000,6.350000><20.320000,-1.538000,6.350000>0.450000}
cylinder{<20.320000,0.038000,13.970000><20.320000,-1.538000,13.970000>0.450000}
cylinder{<7.442200,0.038000,22.860000><7.442200,-1.538000,22.860000>0.508000}
cylinder{<10.160000,0.038000,22.860000><10.160000,-1.538000,22.860000>0.508000}
cylinder{<12.877800,0.038000,22.860000><12.877800,-1.538000,22.860000>0.508000}
//Holes(fast)/Vias
//Holes(fast)/Board
texture{col_hls}
}
#if(pcb_silkscreen=on)
//Silk Screen
union{
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<1.827900,0.000000,2.012500>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.607500,0.000000,2.012500>}
box{<0,0,-0.050800><0.779600,0.036000,0.050800> rotate<0,0.000000,0> translate<1.827900,0.000000,2.012500> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.607500,0.000000,2.012500>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.997200,0.000000,2.402200>}
box{<0,0,-0.050800><0.551119,0.036000,0.050800> rotate<0,-44.997030,0> translate<2.607500,0.000000,2.012500> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.997200,0.000000,2.402200>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.607500,0.000000,2.792000>}
box{<0,0,-0.050800><0.551190,0.036000,0.050800> rotate<0,45.004380,0> translate<2.607500,0.000000,2.792000> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.607500,0.000000,2.792000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<1.827900,0.000000,2.792000>}
box{<0,0,-0.050800><0.779600,0.036000,0.050800> rotate<0,0.000000,0> translate<1.827900,0.000000,2.792000> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.217700,0.000000,3.181800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.217700,0.000000,3.376600>}
box{<0,0,-0.050800><0.194800,0.036000,0.050800> rotate<0,90.000000,0> translate<2.217700,0.000000,3.376600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.217700,0.000000,3.376600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.997200,0.000000,3.376600>}
box{<0,0,-0.050800><0.779500,0.036000,0.050800> rotate<0,0.000000,0> translate<2.217700,0.000000,3.376600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.997200,0.000000,3.181800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.997200,0.000000,3.571500>}
box{<0,0,-0.050800><0.389700,0.036000,0.050800> rotate<0,90.000000,0> translate<2.997200,0.000000,3.571500> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<1.633000,0.000000,3.376600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<1.827900,0.000000,3.376600>}
box{<0,0,-0.050800><0.194900,0.036000,0.050800> rotate<0,0.000000,0> translate<1.633000,0.000000,3.376600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.997200,0.000000,3.961300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.217700,0.000000,3.961300>}
box{<0,0,-0.050800><0.779500,0.036000,0.050800> rotate<0,0.000000,0> translate<2.217700,0.000000,3.961300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.217700,0.000000,3.961300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.217700,0.000000,4.545900>}
box{<0,0,-0.050800><0.584600,0.036000,0.050800> rotate<0,90.000000,0> translate<2.217700,0.000000,4.545900> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.217700,0.000000,4.545900>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.412600,0.000000,4.740800>}
box{<0,0,-0.050800><0.275630,0.036000,0.050800> rotate<0,-44.997030,0> translate<2.217700,0.000000,4.545900> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.412600,0.000000,4.740800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.997200,0.000000,4.740800>}
box{<0,0,-0.050800><0.584600,0.036000,0.050800> rotate<0,0.000000,0> translate<2.412600,0.000000,4.740800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<27.227900,0.000000,1.574800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.007500,0.000000,1.574800>}
box{<0,0,-0.050800><0.779600,0.036000,0.050800> rotate<0,0.000000,0> translate<27.227900,0.000000,1.574800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.007500,0.000000,1.574800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.397200,0.000000,1.964500>}
box{<0,0,-0.050800><0.551119,0.036000,0.050800> rotate<0,-44.997030,0> translate<28.007500,0.000000,1.574800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.397200,0.000000,1.964500>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.007500,0.000000,2.354300>}
box{<0,0,-0.050800><0.551190,0.036000,0.050800> rotate<0,45.004380,0> translate<28.007500,0.000000,2.354300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.007500,0.000000,2.354300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<27.227900,0.000000,2.354300>}
box{<0,0,-0.050800><0.779600,0.036000,0.050800> rotate<0,0.000000,0> translate<27.227900,0.000000,2.354300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.397200,0.000000,2.938900>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.397200,0.000000,3.328700>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,90.000000,0> translate<28.397200,0.000000,3.328700> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.397200,0.000000,3.328700>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.202400,0.000000,3.523600>}
box{<0,0,-0.050800><0.275560,0.036000,0.050800> rotate<0,45.011732,0> translate<28.202400,0.000000,3.523600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.202400,0.000000,3.523600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<27.812600,0.000000,3.523600>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,0.000000,0> translate<27.812600,0.000000,3.523600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<27.812600,0.000000,3.523600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<27.617700,0.000000,3.328700>}
box{<0,0,-0.050800><0.275630,0.036000,0.050800> rotate<0,-44.997030,0> translate<27.617700,0.000000,3.328700> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<27.617700,0.000000,3.328700>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<27.617700,0.000000,2.938900>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,-90.000000,0> translate<27.617700,0.000000,2.938900> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<27.617700,0.000000,2.938900>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<27.812600,0.000000,2.744100>}
box{<0,0,-0.050800><0.275560,0.036000,0.050800> rotate<0,44.982329,0> translate<27.617700,0.000000,2.938900> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<27.812600,0.000000,2.744100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.202400,0.000000,2.744100>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,0.000000,0> translate<27.812600,0.000000,2.744100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.202400,0.000000,2.744100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.397200,0.000000,2.938900>}
box{<0,0,-0.050800><0.275489,0.036000,0.050800> rotate<0,-44.997030,0> translate<28.202400,0.000000,2.744100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<27.617700,0.000000,3.913400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.202400,0.000000,3.913400>}
box{<0,0,-0.050800><0.584700,0.036000,0.050800> rotate<0,0.000000,0> translate<27.617700,0.000000,3.913400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.202400,0.000000,3.913400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.397200,0.000000,4.108200>}
box{<0,0,-0.050800><0.275489,0.036000,0.050800> rotate<0,-44.997030,0> translate<28.202400,0.000000,3.913400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.397200,0.000000,4.108200>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.397200,0.000000,4.692900>}
box{<0,0,-0.050800><0.584700,0.036000,0.050800> rotate<0,90.000000,0> translate<28.397200,0.000000,4.692900> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.397200,0.000000,4.692900>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<27.617700,0.000000,4.692900>}
box{<0,0,-0.050800><0.779500,0.036000,0.050800> rotate<0,0.000000,0> translate<27.617700,0.000000,4.692900> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<27.422800,0.000000,5.277500>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.202400,0.000000,5.277500>}
box{<0,0,-0.050800><0.779600,0.036000,0.050800> rotate<0,0.000000,0> translate<27.422800,0.000000,5.277500> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.202400,0.000000,5.277500>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<28.397200,0.000000,5.472400>}
box{<0,0,-0.050800><0.275560,0.036000,0.050800> rotate<0,-45.011732,0> translate<28.202400,0.000000,5.277500> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<27.617700,0.000000,5.082700>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<27.617700,0.000000,5.472400>}
box{<0,0,-0.050800><0.389700,0.036000,0.050800> rotate<0,90.000000,0> translate<27.617700,0.000000,5.472400> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.902700,0.000000,2.108200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.716100,0.000000,2.108200>}
box{<0,0,-0.076200><0.813400,0.036000,0.076200> rotate<0,0.000000,0> translate<10.902700,0.000000,2.108200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.716100,0.000000,2.108200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.987300,0.000000,2.379300>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-44.986466,0> translate<11.716100,0.000000,2.108200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.987300,0.000000,2.379300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.716100,0.000000,2.650500>}
box{<0,0,-0.076200><0.383535,0.036000,0.076200> rotate<0,44.997030,0> translate<11.716100,0.000000,2.650500> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.716100,0.000000,2.650500>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.173800,0.000000,2.650500>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<11.173800,0.000000,2.650500> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.173800,0.000000,2.650500>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.902700,0.000000,2.921600>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<10.902700,0.000000,2.921600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.902700,0.000000,2.921600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.173800,0.000000,3.192800>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-45.007595,0> translate<10.902700,0.000000,2.921600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.173800,0.000000,3.192800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.987300,0.000000,3.192800>}
box{<0,0,-0.076200><0.813500,0.036000,0.076200> rotate<0,0.000000,0> translate<11.173800,0.000000,3.192800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.810900,0.000000,3.192800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.353200,0.000000,3.192800>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<12.810900,0.000000,3.192800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.353200,0.000000,3.192800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.624400,0.000000,2.921600>}
box{<0,0,-0.076200><0.383535,0.036000,0.076200> rotate<0,44.997030,0> translate<13.353200,0.000000,3.192800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.624400,0.000000,2.921600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.624400,0.000000,2.108200>}
box{<0,0,-0.076200><0.813400,0.036000,0.076200> rotate<0,-90.000000,0> translate<13.624400,0.000000,2.108200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.624400,0.000000,2.108200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.810900,0.000000,2.108200>}
box{<0,0,-0.076200><0.813500,0.036000,0.076200> rotate<0,0.000000,0> translate<12.810900,0.000000,2.108200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.810900,0.000000,2.108200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.539800,0.000000,2.379300>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<12.539800,0.000000,2.379300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.539800,0.000000,2.379300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.810900,0.000000,2.650500>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-45.007595,0> translate<12.539800,0.000000,2.379300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.810900,0.000000,2.650500>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.624400,0.000000,2.650500>}
box{<0,0,-0.076200><0.813500,0.036000,0.076200> rotate<0,0.000000,0> translate<12.810900,0.000000,2.650500> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.176900,0.000000,3.735100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.448000,0.000000,3.735100>}
box{<0,0,-0.076200><0.271100,0.036000,0.076200> rotate<0,0.000000,0> translate<14.176900,0.000000,3.735100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.448000,0.000000,3.735100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.448000,0.000000,2.108200>}
box{<0,0,-0.076200><1.626900,0.036000,0.076200> rotate<0,-90.000000,0> translate<14.448000,0.000000,2.108200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.176900,0.000000,2.108200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.719200,0.000000,2.108200>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<14.176900,0.000000,2.108200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.268300,0.000000,3.192800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.810600,0.000000,2.108200>}
box{<0,0,-0.076200><1.212620,0.036000,0.076200> rotate<0,63.430762,0> translate<15.268300,0.000000,3.192800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.810600,0.000000,2.108200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.352900,0.000000,3.192800>}
box{<0,0,-0.076200><1.212620,0.036000,0.076200> rotate<0,-63.430762,0> translate<15.810600,0.000000,2.108200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.905400,0.000000,3.192800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.176500,0.000000,3.192800>}
box{<0,0,-0.076200><0.271100,0.036000,0.076200> rotate<0,0.000000,0> translate<16.905400,0.000000,3.192800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.176500,0.000000,3.192800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.176500,0.000000,2.108200>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,-90.000000,0> translate<17.176500,0.000000,2.108200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.905400,0.000000,2.108200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.447700,0.000000,2.108200>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<16.905400,0.000000,2.108200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.176500,0.000000,4.006300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.176500,0.000000,3.735100>}
box{<0,0,-0.076200><0.271200,0.036000,0.076200> rotate<0,-90.000000,0> translate<17.176500,0.000000,3.735100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.996800,0.000000,3.192800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<19.081400,0.000000,2.108200>}
box{<0,0,-0.076200><1.533856,0.036000,0.076200> rotate<0,44.997030,0> translate<17.996800,0.000000,3.192800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.996800,0.000000,2.108200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<19.081400,0.000000,3.192800>}
box{<0,0,-0.076200><1.533856,0.036000,0.076200> rotate<0,-44.997030,0> translate<17.996800,0.000000,2.108200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.155100,0.000000,18.940800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.697400,0.000000,19.483100>}
box{<0,0,-0.076200><0.766928,0.036000,0.076200> rotate<0,-44.997030,0> translate<12.155100,0.000000,18.940800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.697400,0.000000,19.483100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.697400,0.000000,17.856200>}
box{<0,0,-0.076200><1.626900,0.036000,0.076200> rotate<0,-90.000000,0> translate<12.697400,0.000000,17.856200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.155100,0.000000,17.856200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.239700,0.000000,17.856200>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<12.155100,0.000000,17.856200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.075100,0.000000,18.127300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.075100,0.000000,19.212000>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,90.000000,0> translate<7.075100,0.000000,19.212000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.075100,0.000000,19.212000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.346200,0.000000,19.483100>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,-44.997030,0> translate<7.075100,0.000000,19.212000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.346200,0.000000,19.483100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.888500,0.000000,19.483100>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<7.346200,0.000000,19.483100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.888500,0.000000,19.483100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.159700,0.000000,19.212000>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,44.986466,0> translate<7.888500,0.000000,19.483100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.159700,0.000000,19.212000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.159700,0.000000,18.127300>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,-90.000000,0> translate<8.159700,0.000000,18.127300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.159700,0.000000,18.127300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.888500,0.000000,17.856200>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-44.986466,0> translate<7.888500,0.000000,17.856200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.888500,0.000000,17.856200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.346200,0.000000,17.856200>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<7.346200,0.000000,17.856200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.346200,0.000000,17.856200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.075100,0.000000,18.127300>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<7.075100,0.000000,18.127300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.075100,0.000000,18.127300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.159700,0.000000,19.212000>}
box{<0,0,-0.076200><1.533927,0.036000,0.076200> rotate<0,-44.999671,0> translate<7.075100,0.000000,18.127300> }
//INPUT silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<1.270000,0.000000,6.985000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<1.270000,0.000000,8.255000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,90.000000,0> translate<1.270000,0.000000,8.255000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<1.270000,0.000000,8.255000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<1.905000,0.000000,8.890000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<1.270000,0.000000,8.255000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.175000,0.000000,8.890000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.810000,0.000000,8.255000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<3.175000,0.000000,8.890000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<1.905000,0.000000,8.890000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<1.270000,0.000000,9.525000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<1.270000,0.000000,9.525000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<1.270000,0.000000,9.525000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<1.270000,0.000000,10.795000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,90.000000,0> translate<1.270000,0.000000,10.795000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<1.270000,0.000000,10.795000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<1.905000,0.000000,11.430000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<1.270000,0.000000,10.795000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.175000,0.000000,11.430000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.810000,0.000000,10.795000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<3.175000,0.000000,11.430000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.810000,0.000000,10.795000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.810000,0.000000,9.525000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,-90.000000,0> translate<3.810000,0.000000,9.525000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.810000,0.000000,9.525000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.175000,0.000000,8.890000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<3.175000,0.000000,8.890000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<1.905000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.175000,0.000000,6.350000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<1.905000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<1.270000,0.000000,6.985000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<1.905000,0.000000,6.350000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<1.270000,0.000000,6.985000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.175000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.810000,0.000000,6.985000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<3.175000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.810000,0.000000,8.255000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.810000,0.000000,6.985000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,-90.000000,0> translate<3.810000,0.000000,6.985000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<1.905000,0.000000,11.430000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<3.175000,0.000000,11.430000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<1.905000,0.000000,11.430000> }
box{<-0.292100,0,-0.292100><0.292100,0.036000,0.292100> rotate<0,-90.000000,0> translate<2.540000,0.000000,7.620000>}
box{<-0.292100,0,-0.292100><0.292100,0.036000,0.292100> rotate<0,-90.000000,0> translate<2.540000,0.000000,10.160000>}
//L silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,24.765000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,20.955000>}
box{<0,0,-0.101600><3.810000,0.036000,0.101600> rotate<0,-90.000000,0> translate<20.320000,0.000000,20.955000> }
object{ARC(3.175000,0.254000,216.869898,503.130102,0.036000) translate<22.860000,0.000000,22.860000>}
object{ARC(1.143000,0.152400,270.000000,360.000000,0.036000) translate<22.860000,0.000000,22.860000>}
object{ARC(1.143000,0.152400,90.000000,180.000000,0.036000) translate<22.860000,0.000000,22.860000>}
object{ARC(1.651000,0.152400,270.000000,360.000000,0.036000) translate<22.860000,0.000000,22.860000>}
object{ARC(1.651000,0.152400,90.000000,180.000000,0.036000) translate<22.860000,0.000000,22.860000>}
object{ARC(2.159000,0.152400,270.000000,360.000000,0.036000) translate<22.860000,0.000000,22.860000>}
object{ARC(2.159000,0.152400,90.000000,180.000000,0.036000) translate<22.860000,0.000000,22.860000>}
difference{
cylinder{<22.860000,0,22.860000><22.860000,0.036000,22.860000>2.616200 translate<0,0.000000,0>}
cylinder{<22.860000,-0.1,22.860000><22.860000,0.135000,22.860000>2.463800 translate<0,0.000000,0>}}
//OUTPUT silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.210000,0.000000,10.795000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.210000,0.000000,9.525000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,-90.000000,0> translate<29.210000,0.000000,9.525000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.210000,0.000000,9.525000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<28.575000,0.000000,8.890000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<28.575000,0.000000,8.890000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<27.305000,0.000000,8.890000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.670000,0.000000,9.525000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<26.670000,0.000000,9.525000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<28.575000,0.000000,8.890000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.210000,0.000000,8.255000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<28.575000,0.000000,8.890000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.210000,0.000000,8.255000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.210000,0.000000,6.985000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,-90.000000,0> translate<29.210000,0.000000,6.985000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.210000,0.000000,6.985000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<28.575000,0.000000,6.350000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<28.575000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<27.305000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.670000,0.000000,6.985000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<26.670000,0.000000,6.985000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.670000,0.000000,6.985000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.670000,0.000000,8.255000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,90.000000,0> translate<26.670000,0.000000,8.255000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.670000,0.000000,8.255000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<27.305000,0.000000,8.890000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<26.670000,0.000000,8.255000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<28.575000,0.000000,11.430000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<27.305000,0.000000,11.430000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<27.305000,0.000000,11.430000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.210000,0.000000,10.795000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<28.575000,0.000000,11.430000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<28.575000,0.000000,11.430000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<27.305000,0.000000,11.430000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.670000,0.000000,10.795000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<26.670000,0.000000,10.795000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.670000,0.000000,9.525000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.670000,0.000000,10.795000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,90.000000,0> translate<26.670000,0.000000,10.795000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<28.575000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<27.305000,0.000000,6.350000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<27.305000,0.000000,6.350000> }
box{<-0.292100,0,-0.292100><0.292100,0.036000,0.292100> rotate<0,-270.000000,0> translate<27.940000,0.000000,10.160000>}
box{<-0.292100,0,-0.292100><0.292100,0.036000,0.292100> rotate<0,-270.000000,0> translate<27.940000,0.000000,7.620000>}
//R1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.922000,0.000000,12.700000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.922000,0.000000,7.620000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,-90.000000,0> translate<10.922000,0.000000,7.620000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.922000,0.000000,7.620000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,7.620000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<10.160000,0.000000,7.620000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,7.620000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.398000,0.000000,7.620000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<9.398000,0.000000,7.620000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.398000,0.000000,7.620000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.398000,0.000000,12.700000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,90.000000,0> translate<9.398000,0.000000,12.700000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.398000,0.000000,12.700000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,12.700000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<9.398000,0.000000,12.700000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,12.700000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.922000,0.000000,12.700000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<10.160000,0.000000,12.700000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,7.620000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,7.366000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,-90.000000,0> translate<10.160000,0.000000,7.366000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,12.700000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,12.954000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,90.000000,0> translate<10.160000,0.000000,12.954000> }
//R2 silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<16.002000,0.000000,12.700000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<16.002000,0.000000,7.620000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,-90.000000,0> translate<16.002000,0.000000,7.620000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<16.002000,0.000000,7.620000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<15.240000,0.000000,7.620000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<15.240000,0.000000,7.620000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<15.240000,0.000000,7.620000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<14.478000,0.000000,7.620000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<14.478000,0.000000,7.620000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<14.478000,0.000000,7.620000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<14.478000,0.000000,12.700000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,90.000000,0> translate<14.478000,0.000000,12.700000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<14.478000,0.000000,12.700000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<15.240000,0.000000,12.700000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<14.478000,0.000000,12.700000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<15.240000,0.000000,12.700000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<16.002000,0.000000,12.700000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<15.240000,0.000000,12.700000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<15.240000,0.000000,7.620000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<15.240000,0.000000,7.366000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,-90.000000,0> translate<15.240000,0.000000,7.366000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<15.240000,0.000000,12.700000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<15.240000,0.000000,12.954000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,90.000000,0> translate<15.240000,0.000000,12.954000> }
//RL silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.558000,0.000000,7.620000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.558000,0.000000,12.700000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,90.000000,0> translate<19.558000,0.000000,12.700000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.558000,0.000000,12.700000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,12.700000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<19.558000,0.000000,12.700000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,12.700000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.082000,0.000000,12.700000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<20.320000,0.000000,12.700000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.082000,0.000000,12.700000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.082000,0.000000,7.620000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,-90.000000,0> translate<21.082000,0.000000,7.620000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.082000,0.000000,7.620000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,7.620000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<20.320000,0.000000,7.620000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,7.620000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.558000,0.000000,7.620000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<19.558000,0.000000,7.620000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,12.700000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,12.954000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,90.000000,0> translate<20.320000,0.000000,12.954000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,7.620000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,7.366000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,-90.000000,0> translate<20.320000,0.000000,7.366000> }
//S1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<4.345000,0.000000,25.035000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<4.345000,0.000000,20.685000>}
box{<0,0,-0.101600><4.350000,0.036000,0.101600> rotate<0,-90.000000,0> translate<4.345000,0.000000,20.685000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<4.345000,0.000000,20.685000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<15.975000,0.000000,20.685000>}
box{<0,0,-0.101600><11.630000,0.036000,0.101600> rotate<0,0.000000,0> translate<4.345000,0.000000,20.685000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<15.975000,0.000000,20.685000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<15.975000,0.000000,25.035000>}
box{<0,0,-0.101600><4.350000,0.036000,0.101600> rotate<0,90.000000,0> translate<15.975000,0.000000,25.035000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<15.975000,0.000000,25.035000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<4.345000,0.000000,25.035000>}
box{<0,0,-0.101600><11.630000,0.036000,0.101600> rotate<0,0.000000,0> translate<4.345000,0.000000,25.035000> }
box{<-0.228600,0,-0.304800><0.228600,0.036000,0.304800> rotate<0,-90.000000,0> translate<10.160000,0.000000,22.860000>}
box{<-0.228600,0,-0.304800><0.228600,0.036000,0.304800> rotate<0,-90.000000,0> translate<7.620000,0.000000,22.860000>}
box{<-0.228600,0,-0.304800><0.228600,0.036000,0.304800> rotate<0,-90.000000,0> translate<12.700000,0.000000,22.860000>}
texture{col_slk}
}
#end
translate<mac_x_ver,mac_y_ver,mac_z_ver>
rotate<mac_x_rot,mac_y_rot,mac_z_rot>
}//End union
#end

#if(use_file_as_inc=off)
object{  Z_POWER_CHEAP(-15.240000,0,-12.700000,pcb_rotate_x,pcb_rotate_y,pcb_rotate_z)
#if(pcb_upsidedown=on)
rotate pcb_rotdir*180
#end
}
#end


//Parts not found in 3dpack.dat or 3dusrpac.dat are:
