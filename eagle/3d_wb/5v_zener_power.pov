//POVRay-File created by 3d41.ulp v20110101
///home/salvix/Dev/vcc_dev/5v_zener/5v_zener_power.brd
//1/31/13 2:06 AM

#version 3.5;

//Set to on if the file should be used as .inc
#local use_file_as_inc = off;
#if(use_file_as_inc=off)


//changes the apperance of resistors (1 Blob / 0 real)
#declare global_res_shape = 1;
//randomize color of resistors 1=random 0=same color
#declare global_res_colselect = 0;
//Number of the color for the resistors
//0=Green, 1="normal color" 2=Blue 3=Brown
#declare global_res_col = 1;
//Set to on if you want to render the PCB upside-down
#declare pcb_upsidedown = off;
//Set to x or z to rotate around the corresponding axis (referring to pcb_upsidedown)
#declare pcb_rotdir = x;
//Set the length off short pins over the PCB
#declare pin_length = 2.5;
#declare global_diode_bend_radius = 1;
#declare global_res_bend_radius = 1;
#declare global_solder = on;

#declare global_show_screws = on;
#declare global_show_washers = on;
#declare global_show_nuts = on;

#declare global_use_radiosity = on;

#declare global_ambient_mul = 1;
#declare global_ambient_mul_emit = 0;

//Animation
#declare global_anim = off;
#local global_anim_showcampath = no;

#declare global_fast_mode = off;

#declare col_preset = 2;
#declare pin_short = on;

#declare e3d_environment = off;

#local cam_x = 0;
#local cam_y = 120;
#local cam_z = -64;
#local cam_a = 20;
#local cam_look_x = 0;
#local cam_look_y = -2;
#local cam_look_z = 0;

#local pcb_rotate_x = 0;
#local pcb_rotate_y = 0;
#local pcb_rotate_z = 0;

#local pcb_board = on;
#local pcb_parts = on;
#local pcb_wire_bridges = off;
#if(global_fast_mode=off)
	#local pcb_polygons = on;
	#local pcb_silkscreen = on;
	#local pcb_wires = on;
	#local pcb_pads_smds = on;
#else
	#local pcb_polygons = off;
	#local pcb_silkscreen = off;
	#local pcb_wires = off;
	#local pcb_pads_smds = off;
#end

#local lgt1_pos_x = 6;
#local lgt1_pos_y = 15;
#local lgt1_pos_z = 14;
#local lgt1_intense = 0.707097;
#local lgt2_pos_x = -6;
#local lgt2_pos_y = 15;
#local lgt2_pos_z = 14;
#local lgt2_intense = 0.707097;
#local lgt3_pos_x = 6;
#local lgt3_pos_y = 15;
#local lgt3_pos_z = -10;
#local lgt3_intense = 0.707097;
#local lgt4_pos_x = -6;
#local lgt4_pos_y = 15;
#local lgt4_pos_z = -10;
#local lgt4_intense = 0.707097;

//Do not change these values
#declare pcb_height = 1.500000;
#declare pcb_cuheight = 0.035000;
#declare pcb_x_size = 17.780000;
#declare pcb_y_size = 27.940000;
#declare pcb_layer1_used = 0;
#declare pcb_layer16_used = 1;
#declare inc_testmode = off;
#declare global_seed=seed(433);
#declare global_pcb_layer_dis = array[16]
{
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	1.535000,
}
#declare global_pcb_real_hole = 2.000000;

#include "e3d_tools.inc"
#include "e3d_user.inc"

global_settings{charset utf8}

#if(e3d_environment=on)
sky_sphere {pigment {Navy}
pigment {bozo turbulence 0.65 octaves 7 omega 0.7 lambda 2
color_map {
[0.0 0.1 color rgb <0.85, 0.85, 0.85> color rgb <0.75, 0.75, 0.75>]
[0.1 0.5 color rgb <0.75, 0.75, 0.75> color rgbt <1, 1, 1, 1>]
[0.5 1.0 color rgbt <1, 1, 1, 1> color rgbt <1, 1, 1, 1>]}
scale <0.1, 0.5, 0.1>} rotate -90*x}
plane{y, -10.0-max(pcb_x_size,pcb_y_size)*abs(max(sin((pcb_rotate_x/180)*pi),sin((pcb_rotate_z/180)*pi)))
texture{T_Chrome_2D
normal{waves 0.1 frequency 3000.0 scale 3000.0}} translate<0,0,0>}
#end

//Animation data
#if(global_anim=on)
#declare global_anim_showcampath = no;
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#declare global_anim_npoints_cam_flight=0;
#warning "No/not enough Animation Data available (min. 3 points) (Flight path)"
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#declare global_anim_npoints_cam_view=0;
#warning "No/not enough Animation Data available (min. 3 points) (View path)"
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#end

#if((global_anim_showcampath=yes)&(global_anim=off))
#end
#if(global_anim=on)
camera
{
	location global_anim_spline_cam_flight(clock)
	#if(global_anim_npoints_cam_view>2)
		look_at global_anim_spline_cam_view(clock)
	#else
		look_at global_anim_spline_cam_flight(clock+0.01)-<0,-0.01,0>
	#end
	angle 45
}
light_source
{
	global_anim_spline_cam_flight(clock)
	color rgb <1,1,1>
	spotlight point_at 
	#if(global_anim_npoints_cam_view>2)
		global_anim_spline_cam_view(clock)
	#else
		global_anim_spline_cam_flight(clock+0.01)-<0,-0.01,0>
	#end
	radius 35 falloff  40
}
#else
camera
{
	location <cam_x,cam_y,cam_z>
	look_at <cam_look_x,cam_look_y,cam_look_z>
	angle cam_a
	//translates the camera that <0,0,0> is over the Eagle <0,0>
	//translate<-8.890000,0,-13.970000>
}
#end

background{col_bgr}
light_source{<lgt1_pos_x,lgt1_pos_y,lgt1_pos_z> White*lgt1_intense}
light_source{<lgt2_pos_x,lgt2_pos_y,lgt2_pos_z> White*lgt2_intense}
light_source{<lgt3_pos_x,lgt3_pos_y,lgt3_pos_z> White*lgt3_intense}
light_source{<lgt4_pos_x,lgt4_pos_y,lgt4_pos_z> White*lgt4_intense}
#end


#macro ZV_ZENER_POWER(mac_x_ver,mac_y_ver,mac_z_ver,mac_x_rot,mac_y_rot,mac_z_rot)
union{
#if(pcb_board = on)
difference{
union{
//Board
prism{-1.500000,0.000000,8
<0.000000,0.000000><17.780000,0.000000>
<17.780000,0.000000><17.780000,27.940000>
<17.780000,27.940000><0.000000,27.940000>
<0.000000,27.940000><0.000000,0.000000>
texture{col_brd}}
}//End union(PCB)
//Holes(real)/Parts
//Holes(real)/Board
//Holes(real)/Vias
}//End difference(reale Bohrungen/Durchbrüche)
#end
#if(pcb_parts=on)//Parts
union{
#ifndef(pack_D1) #declare global_pack_D1=yes; object {DIODE_DIS_DO35_102MM_H("ZTE",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,0> translate<7.620000,0.000000,7.620000>}#end		//Diode DO35 10mm hor. D1 ZTE DO35Z10
#ifndef(pack_INPUT) #declare global_pack_INPUT=yes; object {CON_PH_1X2()translate<0,0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,0> translate<2.540000,0.000000,3.810000>}#end		//Header 2,54mm Grid 2Pin 1Row (jumper.lib) INPUT Nokia 1X02
#ifndef(pack_LED) #declare global_pack_LED=yes; object {DIODE_DIS_LED_5MM(Red,0.500000,0.000000,)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,0> translate<12.700000,0.000000,13.970000>}#end		//Diskrete 5MM LED LED D2 LED5MM
#ifndef(pack_OUTPUT) #declare global_pack_OUTPUT=yes; object {CON_PH_1X2()translate<0,0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,0> translate<15.240000,0.000000,3.810000>}#end		//Header 2,54mm Grid 2Pin 1Row (jumper.lib) OUTPUT uC 1X02
#ifndef(pack_R) #declare global_pack_R=yes; object {RES_DIS_0204_075MM(texture{pigment{Orange}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{Orange}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{DarkBrown}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture {T_Gold_5C finish{reflection 0.1}},)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,0> translate<11.430000,0.000000,6.350000>}#end		//Discrete Resistor 0,15W 7,5MM Grid R 330 0204/7
#ifndef(pack_R1) #declare global_pack_R1=yes; object {RES_DIS_0204_075MM(texture{pigment{Orange}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{Orange}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{DarkBrown}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture {T_Gold_5C finish{reflection 0.1}},)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,0> translate<2.540000,0.000000,11.430000>}#end		//Discrete Resistor 0,15W 7,5MM Grid R1 330 0204/7
#ifndef(pack_U_2) #declare global_pack_U_2=yes; object {SWITCH_SECME_1K2_SH()translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<8.890000,0.000000,22.860000>}#end		//Switch 1K2 Straight High actuator U$2 USWITCH-6P 6PDIP
}//End union
#end
#if(pcb_pads_smds=on)
//Pads&SMD/Parts
#ifndef(global_pack_D1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,3+global_tmp,100) rotate<0,-270.000000,0>translate<7.620000,0,2.540000> texture{col_thl}}
#ifndef(global_pack_D1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,3+global_tmp,100) rotate<0,-270.000000,0>translate<7.620000,0,12.700000> texture{col_thl}}
#ifndef(global_pack_INPUT) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-180.000000,0>translate<2.540000,0,2.540000> texture{col_thl}}
#ifndef(global_pack_INPUT) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-180.000000,0>translate<2.540000,0,5.080000> texture{col_thl}}
#ifndef(global_pack_LED) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,2+global_tmp,0) rotate<0,-180.000000,0>translate<13.970000,0,13.970000> texture{col_thl}}
#ifndef(global_pack_LED) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,2+global_tmp,0) rotate<0,-180.000000,0>translate<11.430000,0,13.970000> texture{col_thl}}
#ifndef(global_pack_OUTPUT) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-0.000000,0>translate<15.240000,0,5.080000> texture{col_thl}}
#ifndef(global_pack_OUTPUT) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-0.000000,0>translate<15.240000,0,2.540000> texture{col_thl}}
#ifndef(global_pack_R) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,2+global_tmp,0) rotate<0,-270.000000,0>translate<11.430000,0,10.160000> texture{col_thl}}
#ifndef(global_pack_R) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,2+global_tmp,0) rotate<0,-270.000000,0>translate<11.430000,0,2.540000> texture{col_thl}}
#ifndef(global_pack_R1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,2+global_tmp,0) rotate<0,-90.000000,0>translate<2.540000,0,7.620000> texture{col_thl}}
#ifndef(global_pack_R1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,2+global_tmp,0) rotate<0,-90.000000,0>translate<2.540000,0,15.240000> texture{col_thl}}
#ifndef(global_pack_U_2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.308000,0.800000,1,16,0+global_tmp,0) rotate<0,-0.000000,0>translate<5.715000,0,24.447500> texture{col_thl}}
#ifndef(global_pack_U_2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.308000,0.800000,1,16,0+global_tmp,0) rotate<0,-0.000000,0>translate<5.715000,0,21.272500> texture{col_thl}}
#ifndef(global_pack_U_2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.308000,0.800000,1,16,0+global_tmp,0) rotate<0,-0.000000,0>translate<8.890000,0,24.447500> texture{col_thl}}
#ifndef(global_pack_U_2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.308000,0.800000,1,16,0+global_tmp,0) rotate<0,-0.000000,0>translate<8.890000,0,21.272500> texture{col_thl}}
#ifndef(global_pack_U_2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.308000,0.800000,1,16,0+global_tmp,0) rotate<0,-0.000000,0>translate<12.065000,0,24.447500> texture{col_thl}}
#ifndef(global_pack_U_2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.308000,0.800000,1,16,0+global_tmp,0) rotate<0,-0.000000,0>translate<12.065000,0,21.272500> texture{col_thl}}
//Pads/Vias
#end
#if(pcb_wires=on)
union{
//Signals
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<2.540000,-1.535000,5.080000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<2.540000,-1.535000,7.620000>}
box{<0,0,-0.508000><2.540000,0.035000,0.508000> rotate<0,90.000000,0> translate<2.540000,-1.535000,7.620000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<2.540000,-1.535000,15.240000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<3.810000,-1.535000,16.510000>}
box{<0,0,-0.508000><1.796051,0.035000,0.508000> rotate<0,-44.997030,0> translate<2.540000,-1.535000,15.240000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<2.540000,-1.535000,2.540000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<7.620000,-1.535000,2.540000>}
box{<0,0,-0.508000><5.080000,0.035000,0.508000> rotate<0,0.000000,0> translate<2.540000,-1.535000,2.540000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<3.810000,-1.535000,16.510000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<7.620000,-1.535000,12.700000>}
box{<0,0,-0.508000><5.388154,0.035000,0.508000> rotate<0,44.997030,0> translate<3.810000,-1.535000,16.510000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<8.890000,-1.535000,21.590000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<8.890000,-1.535000,21.272500>}
box{<0,0,-0.508000><0.317500,0.035000,0.508000> rotate<0,-90.000000,0> translate<8.890000,-1.535000,21.272500> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<3.810000,-1.535000,16.510000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<8.890000,-1.535000,21.590000>}
box{<0,0,-0.508000><7.184205,0.035000,0.508000> rotate<0,-44.997030,0> translate<3.810000,-1.535000,16.510000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<7.620000,-1.535000,2.540000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<11.430000,-1.535000,2.540000>}
box{<0,0,-0.508000><3.810000,0.035000,0.508000> rotate<0,0.000000,0> translate<7.620000,-1.535000,2.540000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<11.430000,-1.535000,10.160000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<11.430000,-1.535000,13.970000>}
box{<0,0,-0.508000><3.810000,0.035000,0.508000> rotate<0,90.000000,0> translate<11.430000,-1.535000,13.970000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<12.065000,-1.535000,21.272500>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<12.700000,-1.535000,21.590000>}
box{<0,0,-0.508000><0.709952,0.035000,0.508000> rotate<0,-26.563298,0> translate<12.065000,-1.535000,21.272500> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<12.700000,-1.535000,16.510000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<12.700000,-1.535000,21.590000>}
box{<0,0,-0.508000><5.080000,0.035000,0.508000> rotate<0,90.000000,0> translate<12.700000,-1.535000,21.590000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<12.700000,-1.535000,16.510000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<13.970000,-1.535000,15.240000>}
box{<0,0,-0.508000><1.796051,0.035000,0.508000> rotate<0,44.997030,0> translate<12.700000,-1.535000,16.510000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<13.970000,-1.535000,13.970000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<13.970000,-1.535000,15.240000>}
box{<0,0,-0.508000><1.270000,0.035000,0.508000> rotate<0,90.000000,0> translate<13.970000,-1.535000,15.240000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<11.430000,-1.535000,2.540000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<15.240000,-1.535000,2.540000>}
box{<0,0,-0.508000><3.810000,0.035000,0.508000> rotate<0,0.000000,0> translate<11.430000,-1.535000,2.540000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<13.970000,-1.535000,13.970000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<15.240000,-1.535000,12.700000>}
box{<0,0,-0.508000><1.796051,0.035000,0.508000> rotate<0,44.997030,0> translate<13.970000,-1.535000,13.970000> }
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<15.240000,-1.535000,5.080000>}
cylinder{<0,0,0><0,0.035000,0>0.508000 translate<15.240000,-1.535000,12.700000>}
box{<0,0,-0.508000><7.620000,0.035000,0.508000> rotate<0,90.000000,0> translate<15.240000,-1.535000,12.700000> }
//Text
//Rect
union{
texture{col_pds}
}
texture{col_wrs}
}
#end
#if(pcb_polygons=on)
union{
//Polygons
texture{col_pol}
}
#end
union{
cylinder{<7.620000,0.038000,2.540000><7.620000,-1.538000,2.540000>0.406400}
cylinder{<7.620000,0.038000,12.700000><7.620000,-1.538000,12.700000>0.406400}
cylinder{<2.540000,0.038000,2.540000><2.540000,-1.538000,2.540000>0.508000}
cylinder{<2.540000,0.038000,5.080000><2.540000,-1.538000,5.080000>0.508000}
cylinder{<13.970000,0.038000,13.970000><13.970000,-1.538000,13.970000>0.406400}
cylinder{<11.430000,0.038000,13.970000><11.430000,-1.538000,13.970000>0.406400}
cylinder{<15.240000,0.038000,5.080000><15.240000,-1.538000,5.080000>0.508000}
cylinder{<15.240000,0.038000,2.540000><15.240000,-1.538000,2.540000>0.508000}
cylinder{<11.430000,0.038000,10.160000><11.430000,-1.538000,10.160000>0.406400}
cylinder{<11.430000,0.038000,2.540000><11.430000,-1.538000,2.540000>0.406400}
cylinder{<2.540000,0.038000,7.620000><2.540000,-1.538000,7.620000>0.406400}
cylinder{<2.540000,0.038000,15.240000><2.540000,-1.538000,15.240000>0.406400}
cylinder{<5.715000,0.038000,24.447500><5.715000,-1.538000,24.447500>0.400000}
cylinder{<5.715000,0.038000,21.272500><5.715000,-1.538000,21.272500>0.400000}
cylinder{<8.890000,0.038000,24.447500><8.890000,-1.538000,24.447500>0.400000}
cylinder{<8.890000,0.038000,21.272500><8.890000,-1.538000,21.272500>0.400000}
cylinder{<12.065000,0.038000,24.447500><12.065000,-1.538000,24.447500>0.400000}
cylinder{<12.065000,0.038000,21.272500><12.065000,-1.538000,21.272500>0.400000}
//Holes(fast)/Vias
//Holes(fast)/Board
cylinder{<1.270000,0.038000,26.670000><1.270000,-1.538000,26.670000>0.300000 }
texture{col_hls}
}
#if(pcb_silkscreen=on)
//Silk Screen
union{
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.951200,0.000000,17.670800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.493500,0.000000,18.213100>}
box{<0,0,-0.076200><0.766928,0.036000,0.076200> rotate<0,-44.997030,0> translate<15.951200,0.000000,17.670800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.493500,0.000000,18.213100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.493500,0.000000,16.586200>}
box{<0,0,-0.076200><1.626900,0.036000,0.076200> rotate<0,-90.000000,0> translate<16.493500,0.000000,16.586200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.951200,0.000000,16.586200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.035800,0.000000,16.586200>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<15.951200,0.000000,16.586200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<0.711200,0.000000,16.857300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<0.711200,0.000000,17.942000>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,90.000000,0> translate<0.711200,0.000000,17.942000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<0.711200,0.000000,17.942000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<0.982300,0.000000,18.213100>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,-44.997030,0> translate<0.711200,0.000000,17.942000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<0.982300,0.000000,18.213100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.524600,0.000000,18.213100>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<0.982300,0.000000,18.213100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.524600,0.000000,18.213100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.795800,0.000000,17.942000>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,44.986466,0> translate<1.524600,0.000000,18.213100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.795800,0.000000,17.942000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.795800,0.000000,16.857300>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,-90.000000,0> translate<1.795800,0.000000,16.857300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.795800,0.000000,16.857300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.524600,0.000000,16.586200>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-44.986466,0> translate<1.524600,0.000000,16.586200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.524600,0.000000,16.586200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<0.982300,0.000000,16.586200>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<0.982300,0.000000,16.586200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<0.982300,0.000000,16.586200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<0.711200,0.000000,16.857300>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<0.711200,0.000000,16.857300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<0.711200,0.000000,16.857300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.795800,0.000000,17.942000>}
box{<0,0,-0.076200><1.533927,0.036000,0.076200> rotate<0,-44.999671,0> translate<0.711200,0.000000,16.857300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.046200,0.000000,8.509600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.130800,0.000000,8.509600>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<14.046200,0.000000,8.509600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.588500,0.000000,9.052000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.588500,0.000000,7.967300>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,-90.000000,0> translate<14.588500,0.000000,7.967300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.767900,0.000000,9.323100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.683300,0.000000,9.323100>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<15.683300,0.000000,9.323100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.683300,0.000000,9.323100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.683300,0.000000,8.509600>}
box{<0,0,-0.076200><0.813500,0.036000,0.076200> rotate<0,-90.000000,0> translate<15.683300,0.000000,8.509600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.683300,0.000000,8.509600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.225600,0.000000,8.780800>}
box{<0,0,-0.076200><0.606332,0.036000,0.076200> rotate<0,-26.567524,0> translate<15.683300,0.000000,8.509600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.225600,0.000000,8.780800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.496700,0.000000,8.780800>}
box{<0,0,-0.076200><0.271100,0.036000,0.076200> rotate<0,0.000000,0> translate<16.225600,0.000000,8.780800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.496700,0.000000,8.780800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.767900,0.000000,8.509600>}
box{<0,0,-0.076200><0.383535,0.036000,0.076200> rotate<0,44.997030,0> translate<16.496700,0.000000,8.780800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.767900,0.000000,8.509600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.767900,0.000000,7.967300>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,-90.000000,0> translate<16.767900,0.000000,7.967300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.767900,0.000000,7.967300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.496700,0.000000,7.696200>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-44.986466,0> translate<16.496700,0.000000,7.696200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.496700,0.000000,7.696200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.954400,0.000000,7.696200>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<15.954400,0.000000,7.696200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.954400,0.000000,7.696200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.683300,0.000000,7.967300>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<15.683300,0.000000,7.967300> }
//D1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.254000 translate<7.620000,0.000000,2.540000>}
cylinder{<0,0,0><0,0.036000,0>0.254000 translate<7.620000,0.000000,3.429000>}
box{<0,0,-0.254000><0.889000,0.036000,0.254000> rotate<0,90.000000,0> translate<7.620000,0.000000,3.429000> }
cylinder{<0,0,0><0,0.036000,0>0.254000 translate<7.620000,0.000000,12.700000>}
cylinder{<0,0,0><0,0.036000,0>0.254000 translate<7.620000,0.000000,11.811000>}
box{<0,0,-0.254000><0.889000,0.036000,0.254000> rotate<0,-90.000000,0> translate<7.620000,0.000000,11.811000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.620000,0.000000,8.255000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.620000,0.000000,7.620000>}
box{<0,0,-0.076200><0.635000,0.036000,0.076200> rotate<0,-90.000000,0> translate<7.620000,0.000000,7.620000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.255000,0.000000,6.604000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.985000,0.000000,6.604000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<6.985000,0.000000,6.604000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.985000,0.000000,6.604000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.620000,0.000000,7.620000>}
box{<0,0,-0.076200><1.198116,0.036000,0.076200> rotate<0,-57.990789,0> translate<6.985000,0.000000,6.604000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.620000,0.000000,7.620000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.620000,0.000000,6.096000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,-90.000000,0> translate<7.620000,0.000000,6.096000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.620000,0.000000,7.620000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.255000,0.000000,6.604000>}
box{<0,0,-0.076200><1.198116,0.036000,0.076200> rotate<0,57.990789,0> translate<7.620000,0.000000,7.620000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.620000,0.000000,7.620000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.985000,0.000000,7.620000>}
box{<0,0,-0.076200><0.635000,0.036000,0.076200> rotate<0,0.000000,0> translate<6.985000,0.000000,7.620000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.255000,0.000000,7.366000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.255000,0.000000,7.620000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,90.000000,0> translate<8.255000,0.000000,7.620000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.255000,0.000000,7.620000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.620000,0.000000,7.620000>}
box{<0,0,-0.076200><0.635000,0.036000,0.076200> rotate<0,0.000000,0> translate<7.620000,0.000000,7.620000> }
object{ARC(0.254000,0.152400,270.000000,360.000000,0.036000) translate<8.382000,0.000000,5.588000>}
object{ARC(0.254000,0.152400,0.000000,90.000000,0.036000) translate<8.382000,0.000000,9.652000>}
object{ARC(0.254000,0.152400,90.000000,180.000000,0.036000) translate<6.858000,0.000000,9.652000>}
object{ARC(0.254000,0.152400,180.000000,270.000000,0.036000) translate<6.858000,0.000000,5.588000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.382000,0.000000,5.334000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.858000,0.000000,5.334000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,0.000000,0> translate<6.858000,0.000000,5.334000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.382000,0.000000,9.906000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.858000,0.000000,9.906000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,0.000000,0> translate<6.858000,0.000000,9.906000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.636000,0.000000,9.652000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.636000,0.000000,5.588000>}
box{<0,0,-0.076200><4.064000,0.036000,0.076200> rotate<0,-90.000000,0> translate<8.636000,0.000000,5.588000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.604000,0.000000,9.652000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.604000,0.000000,5.588000>}
box{<0,0,-0.076200><4.064000,0.036000,0.076200> rotate<0,-90.000000,0> translate<6.604000,0.000000,5.588000> }
box{<-0.254000,0,-1.016000><0.254000,0.036000,1.016000> rotate<0,-270.000000,0> translate<7.620000,0.000000,9.271000>}
box{<-0.952500,0,-0.254000><0.952500,0.036000,0.254000> rotate<0,-270.000000,0> translate<7.620000,0.000000,4.381500>}
box{<-0.952500,0,-0.254000><0.952500,0.036000,0.254000> rotate<0,-270.000000,0> translate<7.620000,0.000000,10.858500>}
//INPUT silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.270000,0.000000,1.905000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.270000,0.000000,3.175000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,90.000000,0> translate<1.270000,0.000000,3.175000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.270000,0.000000,3.175000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.905000,0.000000,3.810000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<1.270000,0.000000,3.175000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.905000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.175000,0.000000,3.810000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<1.905000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.175000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.810000,0.000000,3.175000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<3.175000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.905000,0.000000,1.270000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.175000,0.000000,1.270000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<1.905000,0.000000,1.270000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.270000,0.000000,1.905000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.905000,0.000000,1.270000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<1.270000,0.000000,1.905000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.175000,0.000000,1.270000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.810000,0.000000,1.905000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<3.175000,0.000000,1.270000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.810000,0.000000,3.175000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.810000,0.000000,1.905000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<3.810000,0.000000,1.905000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.905000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.270000,0.000000,4.445000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<1.270000,0.000000,4.445000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.270000,0.000000,4.445000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.270000,0.000000,5.715000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,90.000000,0> translate<1.270000,0.000000,5.715000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.270000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.905000,0.000000,6.350000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<1.270000,0.000000,5.715000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.905000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.175000,0.000000,6.350000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<1.905000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.175000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.810000,0.000000,5.715000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<3.175000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.810000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.810000,0.000000,4.445000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<3.810000,0.000000,4.445000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.810000,0.000000,4.445000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.175000,0.000000,3.810000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<3.175000,0.000000,3.810000> }
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<2.540000,0.000000,2.540000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<2.540000,0.000000,5.080000>}
//LED silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,15.875000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,12.065000>}
box{<0,0,-0.101600><3.810000,0.036000,0.101600> rotate<0,-90.000000,0> translate<10.160000,0.000000,12.065000> }
object{ARC(3.175000,0.254000,216.869898,503.130102,0.036000) translate<12.700000,0.000000,13.970000>}
object{ARC(1.143000,0.152400,270.000000,360.000000,0.036000) translate<12.700000,0.000000,13.970000>}
object{ARC(1.143000,0.152400,90.000000,180.000000,0.036000) translate<12.700000,0.000000,13.970000>}
object{ARC(1.651000,0.152400,270.000000,360.000000,0.036000) translate<12.700000,0.000000,13.970000>}
object{ARC(1.651000,0.152400,90.000000,180.000000,0.036000) translate<12.700000,0.000000,13.970000>}
object{ARC(2.159000,0.152400,270.000000,360.000000,0.036000) translate<12.700000,0.000000,13.970000>}
object{ARC(2.159000,0.152400,90.000000,180.000000,0.036000) translate<12.700000,0.000000,13.970000>}
difference{
cylinder{<12.700000,0,13.970000><12.700000,0.036000,13.970000>2.616200 translate<0,0.000000,0>}
cylinder{<12.700000,-0.1,13.970000><12.700000,0.135000,13.970000>2.463800 translate<0,0.000000,0>}}
//OUTPUT silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.510000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.510000,0.000000,4.445000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<16.510000,0.000000,4.445000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.510000,0.000000,4.445000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.875000,0.000000,3.810000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<15.875000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.875000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.605000,0.000000,3.810000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<14.605000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.605000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.970000,0.000000,4.445000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<13.970000,0.000000,4.445000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.875000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.605000,0.000000,6.350000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<14.605000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.510000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.875000,0.000000,6.350000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<15.875000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.605000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.970000,0.000000,5.715000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<13.970000,0.000000,5.715000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.970000,0.000000,4.445000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.970000,0.000000,5.715000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,90.000000,0> translate<13.970000,0.000000,5.715000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.875000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.510000,0.000000,3.175000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<15.875000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.510000,0.000000,3.175000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.510000,0.000000,1.905000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<16.510000,0.000000,1.905000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.510000,0.000000,1.905000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.875000,0.000000,1.270000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<15.875000,0.000000,1.270000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.875000,0.000000,1.270000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.605000,0.000000,1.270000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<14.605000,0.000000,1.270000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.605000,0.000000,1.270000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.970000,0.000000,1.905000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<13.970000,0.000000,1.905000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.970000,0.000000,1.905000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.970000,0.000000,3.175000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,90.000000,0> translate<13.970000,0.000000,3.175000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.970000,0.000000,3.175000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.605000,0.000000,3.810000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<13.970000,0.000000,3.175000> }
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-270.000000,0> translate<15.240000,0.000000,5.080000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-270.000000,0> translate<15.240000,0.000000,2.540000>}
//R silk screen
cylinder{<0,0,0><0,0.036000,0>0.254000 translate<11.430000,0.000000,2.540000>}
cylinder{<0,0,0><0,0.036000,0>0.254000 translate<11.430000,0.000000,3.429000>}
box{<0,0,-0.254000><0.889000,0.036000,0.254000> rotate<0,90.000000,0> translate<11.430000,0.000000,3.429000> }
cylinder{<0,0,0><0,0.036000,0>0.254000 translate<11.430000,0.000000,10.160000>}
cylinder{<0,0,0><0,0.036000,0>0.254000 translate<11.430000,0.000000,9.271000>}
box{<0,0,-0.254000><0.889000,0.036000,0.254000> rotate<0,-90.000000,0> translate<11.430000,0.000000,9.271000> }
object{ARC(0.254000,0.152400,0.000000,90.000000,0.036000) translate<12.192000,0.000000,8.636000>}
object{ARC(0.254000,0.152400,90.000000,180.000000,0.036000) translate<10.668000,0.000000,8.636000>}
object{ARC(0.254000,0.152400,180.000000,270.000000,0.036000) translate<10.668000,0.000000,4.064000>}
object{ARC(0.254000,0.152400,270.000000,360.000000,0.036000) translate<12.192000,0.000000,4.064000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.668000,0.000000,8.890000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.192000,0.000000,8.890000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,0.000000,0> translate<10.668000,0.000000,8.890000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.446000,0.000000,8.636000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.446000,0.000000,8.255000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,-90.000000,0> translate<12.446000,0.000000,8.255000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.319000,0.000000,8.128000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.446000,0.000000,8.255000>}
box{<0,0,-0.076200><0.179605,0.036000,0.076200> rotate<0,-44.997030,0> translate<12.319000,0.000000,8.128000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.414000,0.000000,8.636000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.414000,0.000000,8.255000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,-90.000000,0> translate<10.414000,0.000000,8.255000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.541000,0.000000,8.128000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.414000,0.000000,8.255000>}
box{<0,0,-0.076200><0.179605,0.036000,0.076200> rotate<0,44.997030,0> translate<10.414000,0.000000,8.255000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.319000,0.000000,4.572000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.446000,0.000000,4.445000>}
box{<0,0,-0.076200><0.179605,0.036000,0.076200> rotate<0,44.997030,0> translate<12.319000,0.000000,4.572000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.319000,0.000000,4.572000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.319000,0.000000,8.128000>}
box{<0,0,-0.076200><3.556000,0.036000,0.076200> rotate<0,90.000000,0> translate<12.319000,0.000000,8.128000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.541000,0.000000,4.572000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.414000,0.000000,4.445000>}
box{<0,0,-0.076200><0.179605,0.036000,0.076200> rotate<0,-44.997030,0> translate<10.414000,0.000000,4.445000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.541000,0.000000,4.572000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.541000,0.000000,8.128000>}
box{<0,0,-0.076200><3.556000,0.036000,0.076200> rotate<0,90.000000,0> translate<10.541000,0.000000,8.128000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.446000,0.000000,4.064000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.446000,0.000000,4.445000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,90.000000,0> translate<12.446000,0.000000,4.445000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.414000,0.000000,4.064000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.414000,0.000000,4.445000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,90.000000,0> translate<10.414000,0.000000,4.445000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.668000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.192000,0.000000,3.810000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,0.000000,0> translate<10.668000,0.000000,3.810000> }
box{<-0.190500,0,-0.254000><0.190500,0.036000,0.254000> rotate<0,-270.000000,0> translate<11.430000,0.000000,3.619500>}
box{<-0.190500,0,-0.254000><0.190500,0.036000,0.254000> rotate<0,-270.000000,0> translate<11.430000,0.000000,9.080500>}
//R1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.254000 translate<2.540000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.254000 translate<2.540000,0.000000,14.351000>}
box{<0,0,-0.254000><0.889000,0.036000,0.254000> rotate<0,-90.000000,0> translate<2.540000,0.000000,14.351000> }
cylinder{<0,0,0><0,0.036000,0>0.254000 translate<2.540000,0.000000,7.620000>}
cylinder{<0,0,0><0,0.036000,0>0.254000 translate<2.540000,0.000000,8.509000>}
box{<0,0,-0.254000><0.889000,0.036000,0.254000> rotate<0,90.000000,0> translate<2.540000,0.000000,8.509000> }
object{ARC(0.254000,0.152400,180.000000,270.000000,0.036000) translate<1.778000,0.000000,9.144000>}
object{ARC(0.254000,0.152400,270.000000,360.000000,0.036000) translate<3.302000,0.000000,9.144000>}
object{ARC(0.254000,0.152400,0.000000,90.000000,0.036000) translate<3.302000,0.000000,13.716000>}
object{ARC(0.254000,0.152400,90.000000,180.000000,0.036000) translate<1.778000,0.000000,13.716000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.302000,0.000000,8.890000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.778000,0.000000,8.890000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,0.000000,0> translate<1.778000,0.000000,8.890000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.524000,0.000000,9.144000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.524000,0.000000,9.525000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,90.000000,0> translate<1.524000,0.000000,9.525000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.651000,0.000000,9.652000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.524000,0.000000,9.525000>}
box{<0,0,-0.076200><0.179605,0.036000,0.076200> rotate<0,-44.997030,0> translate<1.524000,0.000000,9.525000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.556000,0.000000,9.144000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.556000,0.000000,9.525000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,90.000000,0> translate<3.556000,0.000000,9.525000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.429000,0.000000,9.652000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.556000,0.000000,9.525000>}
box{<0,0,-0.076200><0.179605,0.036000,0.076200> rotate<0,44.997030,0> translate<3.429000,0.000000,9.652000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.651000,0.000000,13.208000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.524000,0.000000,13.335000>}
box{<0,0,-0.076200><0.179605,0.036000,0.076200> rotate<0,44.997030,0> translate<1.524000,0.000000,13.335000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.651000,0.000000,13.208000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.651000,0.000000,9.652000>}
box{<0,0,-0.076200><3.556000,0.036000,0.076200> rotate<0,-90.000000,0> translate<1.651000,0.000000,9.652000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.429000,0.000000,13.208000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.556000,0.000000,13.335000>}
box{<0,0,-0.076200><0.179605,0.036000,0.076200> rotate<0,-44.997030,0> translate<3.429000,0.000000,13.208000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.429000,0.000000,13.208000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.429000,0.000000,9.652000>}
box{<0,0,-0.076200><3.556000,0.036000,0.076200> rotate<0,-90.000000,0> translate<3.429000,0.000000,9.652000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.524000,0.000000,13.716000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.524000,0.000000,13.335000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,-90.000000,0> translate<1.524000,0.000000,13.335000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.556000,0.000000,13.716000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.556000,0.000000,13.335000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,-90.000000,0> translate<3.556000,0.000000,13.335000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.302000,0.000000,13.970000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.778000,0.000000,13.970000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,0.000000,0> translate<1.778000,0.000000,13.970000> }
box{<-0.190500,0,-0.254000><0.190500,0.036000,0.254000> rotate<0,-90.000000,0> translate<2.540000,0.000000,14.160500>}
box{<-0.190500,0,-0.254000><0.190500,0.036000,0.254000> rotate<0,-90.000000,0> translate<2.540000,0.000000,8.699500>}
//U$2 silk screen
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<15.240000,0.000000,18.415000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<15.240000,0.000000,27.305000>}
box{<0,0,-0.063500><8.890000,0.036000,0.063500> rotate<0,90.000000,0> translate<15.240000,0.000000,27.305000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<15.240000,0.000000,27.305000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<2.540000,0.000000,27.305000>}
box{<0,0,-0.063500><12.700000,0.036000,0.063500> rotate<0,0.000000,0> translate<2.540000,0.000000,27.305000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<2.540000,0.000000,27.305000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<2.540000,0.000000,18.415000>}
box{<0,0,-0.063500><8.890000,0.036000,0.063500> rotate<0,-90.000000,0> translate<2.540000,0.000000,18.415000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<2.540000,0.000000,18.415000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<15.240000,0.000000,18.415000>}
box{<0,0,-0.063500><12.700000,0.036000,0.063500> rotate<0,0.000000,0> translate<2.540000,0.000000,18.415000> }
cylinder{<0,0,0><0,0.036000,0>0.508000 translate<15.240000,-1.536000,24.447500>}
cylinder{<0,0,0><0,0.036000,0>0.508000 translate<15.240000,-1.536000,21.272500>}
box{<0,0,-0.508000><3.175000,0.036000,0.508000> rotate<0,-90.000000,0> translate<15.240000,-1.536000,21.272500> }
cylinder{<0,0,0><0,0.036000,0>0.508000 translate<2.540000,-1.536000,24.447500>}
cylinder{<0,0,0><0,0.036000,0>0.508000 translate<2.540000,-1.536000,21.272500>}
box{<0,0,-0.508000><3.175000,0.036000,0.508000> rotate<0,-90.000000,0> translate<2.540000,-1.536000,21.272500> }
texture{col_slk}
}
#end
translate<mac_x_ver,mac_y_ver,mac_z_ver>
rotate<mac_x_rot,mac_y_rot,mac_z_rot>
}//End union
#end

#if(use_file_as_inc=off)
object{  ZV_ZENER_POWER(-8.890000,0,-13.970000,pcb_rotate_x,pcb_rotate_y,pcb_rotate_z)
#if(pcb_upsidedown=on)
rotate pcb_rotdir*180
#end
}
#end


//Parts not found in 3dpack.dat or 3dusrpac.dat are:
//U$2	USWITCH-6P	6PDIP
