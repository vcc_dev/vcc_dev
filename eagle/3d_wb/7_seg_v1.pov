//POVRay-File created by 3d41.ulp v20110101
///home/salvix/Dev/vcc_dev/7_seg_board/7_seg_v1.brd
//1/13/13 6:22 PM

#version 3.5;

//Set to on if the file should be used as .inc
#local use_file_as_inc = off;
#if(use_file_as_inc=off)


//changes the apperance of resistors (1 Blob / 0 real)
#declare global_res_shape = 1;
//randomize color of resistors 1=random 0=same color
#declare global_res_colselect = 0;
//Number of the color for the resistors
//0=Green, 1="normal color" 2=Blue 3=Brown
#declare global_res_col = 1;
//Set to on if you want to render the PCB upside-down
#declare pcb_upsidedown = off;
//Set to x or z to rotate around the corresponding axis (referring to pcb_upsidedown)
#declare pcb_rotdir = x;
//Set the length off short pins over the PCB
#declare pin_length = 2.5;
#declare global_diode_bend_radius = 1;
#declare global_res_bend_radius = 1;
#declare global_solder = on;

#declare global_show_screws = on;
#declare global_show_washers = on;
#declare global_show_nuts = on;

#declare global_use_radiosity = on;

#declare global_ambient_mul = 1;
#declare global_ambient_mul_emit = 0;

//Animation
#declare global_anim = off;
#local global_anim_showcampath = no;

#declare global_fast_mode = off;

#declare col_preset = 2;
#declare pin_short = on;

#declare e3d_environment = off;

#local cam_x = 0;
#local cam_y = 316;
#local cam_z = -169;
#local cam_a = 20;
#local cam_look_x = 0;
#local cam_look_y = -7;
#local cam_look_z = 0;

#local pcb_rotate_x = 0;
#local pcb_rotate_y = 0;
#local pcb_rotate_z = 0;

#local pcb_board = on;
#local pcb_parts = on;
#local pcb_wire_bridges = off;
#if(global_fast_mode=off)
	#local pcb_polygons = on;
	#local pcb_silkscreen = on;
	#local pcb_wires = on;
	#local pcb_pads_smds = on;
#else
	#local pcb_polygons = off;
	#local pcb_silkscreen = off;
	#local pcb_wires = off;
	#local pcb_pads_smds = off;
#end

#local lgt1_pos_x = 13;
#local lgt1_pos_y = 41;
#local lgt1_pos_z = 39;
#local lgt1_intense = 0.737419;
#local lgt2_pos_x = -13;
#local lgt2_pos_y = 41;
#local lgt2_pos_z = 39;
#local lgt2_intense = 0.737419;
#local lgt3_pos_x = 13;
#local lgt3_pos_y = 41;
#local lgt3_pos_z = -26;
#local lgt3_intense = 0.737419;
#local lgt4_pos_x = -13;
#local lgt4_pos_y = 41;
#local lgt4_pos_z = -26;
#local lgt4_intense = 0.737419;

//Do not change these values
#declare pcb_height = 1.500000;
#declare pcb_cuheight = 0.035000;
#declare pcb_x_size = 35.560000;
#declare pcb_y_size = 73.660000;
#declare pcb_layer1_used = 0;
#declare pcb_layer16_used = 1;
#declare inc_testmode = off;
#declare global_seed=seed(885);
#declare global_pcb_layer_dis = array[16]
{
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	1.535000,
}
#declare global_pcb_real_hole = 2.000000;

#include "e3d_tools.inc"
#include "e3d_user.inc"

global_settings{charset utf8}

#if(e3d_environment=on)
sky_sphere {pigment {Navy}
pigment {bozo turbulence 0.65 octaves 7 omega 0.7 lambda 2
color_map {
[0.0 0.1 color rgb <0.85, 0.85, 0.85> color rgb <0.75, 0.75, 0.75>]
[0.1 0.5 color rgb <0.75, 0.75, 0.75> color rgbt <1, 1, 1, 1>]
[0.5 1.0 color rgbt <1, 1, 1, 1> color rgbt <1, 1, 1, 1>]}
scale <0.1, 0.5, 0.1>} rotate -90*x}
plane{y, -10.0-max(pcb_x_size,pcb_y_size)*abs(max(sin((pcb_rotate_x/180)*pi),sin((pcb_rotate_z/180)*pi)))
texture{T_Chrome_2D
normal{waves 0.1 frequency 3000.0 scale 3000.0}} translate<0,0,0>}
#end

//Animation data
#if(global_anim=on)
#declare global_anim_showcampath = no;
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#declare global_anim_npoints_cam_flight=0;
#warning "No/not enough Animation Data available (min. 3 points) (Flight path)"
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#declare global_anim_npoints_cam_view=0;
#warning "No/not enough Animation Data available (min. 3 points) (View path)"
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#end

#if((global_anim_showcampath=yes)&(global_anim=off))
#end
#if(global_anim=on)
camera
{
	location global_anim_spline_cam_flight(clock)
	#if(global_anim_npoints_cam_view>2)
		look_at global_anim_spline_cam_view(clock)
	#else
		look_at global_anim_spline_cam_flight(clock+0.01)-<0,-0.01,0>
	#end
	angle 45
}
light_source
{
	global_anim_spline_cam_flight(clock)
	color rgb <1,1,1>
	spotlight point_at 
	#if(global_anim_npoints_cam_view>2)
		global_anim_spline_cam_view(clock)
	#else
		global_anim_spline_cam_flight(clock+0.01)-<0,-0.01,0>
	#end
	radius 35 falloff  40
}
#else
camera
{
	location <cam_x,cam_y,cam_z>
	look_at <cam_look_x,cam_look_y,cam_look_z>
	angle cam_a
	//translates the camera that <0,0,0> is over the Eagle <0,0>
	//translate<-17.780000,0,-36.830000>
}
#end

background{col_bgr}
light_source{<lgt1_pos_x,lgt1_pos_y,lgt1_pos_z> White*lgt1_intense}
light_source{<lgt2_pos_x,lgt2_pos_y,lgt2_pos_z> White*lgt2_intense}
light_source{<lgt3_pos_x,lgt3_pos_y,lgt3_pos_z> White*lgt3_intense}
light_source{<lgt4_pos_x,lgt4_pos_y,lgt4_pos_z> White*lgt4_intense}
#end


#macro Z_SEG_V1(mac_x_ver,mac_y_ver,mac_z_ver,mac_x_rot,mac_y_rot,mac_z_rot)
union{
#if(pcb_board = on)
difference{
union{
//Board
prism{-1.500000,0.000000,8
<0.000000,0.000000><35.560000,0.000000>
<35.560000,0.000000><35.560000,73.660000>
<35.560000,73.660000><0.000000,73.660000>
<0.000000,73.660000><0.000000,0.000000>
texture{col_brd}}
}//End union(PCB)
//Holes(real)/Parts
//Holes(real)/Board
cylinder{<2.540000,1,71.120000><2.540000,-5,71.120000>1.100000 texture{col_hls}}
//Holes(real)/Vias
}//End difference(reale Bohrungen/Durchbrüche)
#end
#if(pcb_parts=on)//Parts
union{
#ifndef(pack_CONN) #declare global_pack_CONN=yes; object {CON_PH_1X10()translate<11,0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,0> translate<30.480000,0.000000,5.080000>}#end		//Header 2,54mm Grid 10Pin 1Row (jumper.lib) CONN 10 1X10
#ifndef(pack_DIS1) #declare global_pack_DIS1=yes; object {DIODE_DIS_LED_7SEG_20CM_EAGLE("HD-H101",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<15.240000,0.000000,38.100000>}#end		//7 seg display DIS1 HD-H101 HDSP-M
#ifndef(pack_R_A) #declare global_pack_R_A=yes; object {RES_DIS_0204_075MM(texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture {T_Gold_5C finish{reflection 0.1}},)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,0> translate<17.780000,0.000000,60.960000>}#end		//Discrete Resistor 0,15W 7,5MM Grid R_A  AXIAL-0.3
#ifndef(pack_R_B) #declare global_pack_R_B=yes; object {RES_DIS_0204_075MM(texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture {T_Gold_5C finish{reflection 0.1}},)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,0> translate<20.320000,0.000000,60.960000>}#end		//Discrete Resistor 0,15W 7,5MM Grid R_B  AXIAL-0.3
#ifndef(pack_R_C) #declare global_pack_R_C=yes; object {RES_DIS_0204_075MM(texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture {T_Gold_5C finish{reflection 0.1}},)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,0> translate<17.780000,0.000000,17.780000>}#end		//Discrete Resistor 0,15W 7,5MM Grid R_C  AXIAL-0.3
#ifndef(pack_R_D) #declare global_pack_R_D=yes; object {RES_DIS_0204_075MM(texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture {T_Gold_5C finish{reflection 0.1}},)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,0> translate<12.700000,0.000000,17.780000>}#end		//Discrete Resistor 0,15W 7,5MM Grid R_D  AXIAL-0.3
#ifndef(pack_R_DOT) #declare global_pack_R_DOT=yes; object {RES_DIS_0204_075MM(texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture {T_Gold_5C finish{reflection 0.1}},)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,0> translate<20.320000,0.000000,17.780000>}#end		//Discrete Resistor 0,15W 7,5MM Grid R_DOT  AXIAL-0.3
#ifndef(pack_R_E) #declare global_pack_R_E=yes; object {RES_DIS_0204_075MM(texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture {T_Gold_5C finish{reflection 0.1}},)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,0> translate<10.160000,0.000000,17.780000>}#end		//Discrete Resistor 0,15W 7,5MM Grid R_E  AXIAL-0.3
#ifndef(pack_R_F) #declare global_pack_R_F=yes; object {RES_DIS_0204_075MM(texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture {T_Gold_5C finish{reflection 0.1}},)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,0> translate<12.700000,0.000000,60.960000>}#end		//Discrete Resistor 0,15W 7,5MM Grid R_F  AXIAL-0.3
#ifndef(pack_R_G) #declare global_pack_R_G=yes; object {RES_DIS_0204_075MM(texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture{pigment{checker Black White}finish{phong 0.2 ambient (0.1 * global_ambient_mul)}},texture {T_Gold_5C finish{reflection 0.1}},)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,0> translate<10.160000,0.000000,60.960000>}#end		//Discrete Resistor 0,15W 7,5MM Grid R_G  AXIAL-0.3
}//End union
#end
#if(pcb_pads_smds=on)
//Pads&SMD/Parts
#ifndef(global_pack_CONN) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<30.480000,0,5.080000> texture{col_thl}}
#ifndef(global_pack_CONN) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<27.940000,0,5.080000> texture{col_thl}}
#ifndef(global_pack_CONN) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<25.400000,0,5.080000> texture{col_thl}}
#ifndef(global_pack_CONN) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<22.860000,0,5.080000> texture{col_thl}}
#ifndef(global_pack_CONN) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<20.320000,0,5.080000> texture{col_thl}}
#ifndef(global_pack_CONN) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<17.780000,0,5.080000> texture{col_thl}}
#ifndef(global_pack_CONN) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<15.240000,0,5.080000> texture{col_thl}}
#ifndef(global_pack_CONN) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<12.700000,0,5.080000> texture{col_thl}}
#ifndef(global_pack_CONN) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<10.160000,0,5.080000> texture{col_thl}}
#ifndef(global_pack_CONN) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,1.016000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<7.620000,0,5.080000> texture{col_thl}}
#ifndef(global_pack_DIS1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<10.160000,0,30.480000> texture{col_thl}}
#ifndef(global_pack_DIS1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<12.700000,0,30.480000> texture{col_thl}}
#ifndef(global_pack_DIS1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<15.240000,0,30.480000> texture{col_thl}}
#ifndef(global_pack_DIS1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<17.780000,0,30.480000> texture{col_thl}}
#ifndef(global_pack_DIS1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<20.320000,0,30.480000> texture{col_thl}}
#ifndef(global_pack_DIS1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<20.320000,0,45.720000> texture{col_thl}}
#ifndef(global_pack_DIS1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<17.780000,0,45.720000> texture{col_thl}}
#ifndef(global_pack_DIS1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<15.240000,0,45.720000> texture{col_thl}}
#ifndef(global_pack_DIS1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<12.700000,0,45.720000> texture{col_thl}}
#ifndef(global_pack_DIS1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<10.160000,0,45.720000> texture{col_thl}}
#ifndef(global_pack_R_A) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<17.780000,0,64.770000> texture{col_thl}}
#ifndef(global_pack_R_A) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<17.780000,0,57.150000> texture{col_thl}}
#ifndef(global_pack_R_B) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<20.320000,0,64.770000> texture{col_thl}}
#ifndef(global_pack_R_B) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<20.320000,0,57.150000> texture{col_thl}}
#ifndef(global_pack_R_C) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-90.000000,0>translate<17.780000,0,13.970000> texture{col_thl}}
#ifndef(global_pack_R_C) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-90.000000,0>translate<17.780000,0,21.590000> texture{col_thl}}
#ifndef(global_pack_R_D) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-90.000000,0>translate<12.700000,0,13.970000> texture{col_thl}}
#ifndef(global_pack_R_D) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-90.000000,0>translate<12.700000,0,21.590000> texture{col_thl}}
#ifndef(global_pack_R_DOT) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-90.000000,0>translate<20.320000,0,13.970000> texture{col_thl}}
#ifndef(global_pack_R_DOT) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-90.000000,0>translate<20.320000,0,21.590000> texture{col_thl}}
#ifndef(global_pack_R_E) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-90.000000,0>translate<10.160000,0,13.970000> texture{col_thl}}
#ifndef(global_pack_R_E) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-90.000000,0>translate<10.160000,0,21.590000> texture{col_thl}}
#ifndef(global_pack_R_F) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<12.700000,0,64.770000> texture{col_thl}}
#ifndef(global_pack_R_F) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<12.700000,0,57.150000> texture{col_thl}}
#ifndef(global_pack_R_G) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<10.160000,0,64.770000> texture{col_thl}}
#ifndef(global_pack_R_G) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.879600,0.900000,1,16,1+global_tmp,0) rotate<0,-270.000000,0>translate<10.160000,0,57.150000> texture{col_thl}}
//Pads/Vias
#end
#if(pcb_wires=on)
union{
//Signals
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<3.810000,-1.535000,6.350000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<3.810000,-1.535000,55.880000>}
box{<0,0,-0.254000><49.530000,0.035000,0.254000> rotate<0,90.000000,0> translate<3.810000,-1.535000,55.880000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<5.080000,-1.535000,7.620000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<5.080000,-1.535000,55.880000>}
box{<0,0,-0.254000><48.260000,0.035000,0.254000> rotate<0,90.000000,0> translate<5.080000,-1.535000,55.880000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.350000,-1.535000,8.890000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.350000,-1.535000,55.880000>}
box{<0,0,-0.254000><46.990000,0.035000,0.254000> rotate<0,90.000000,0> translate<6.350000,-1.535000,55.880000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<3.810000,-1.535000,6.350000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.620000,-1.535000,2.540000>}
box{<0,0,-0.254000><5.388154,0.035000,0.254000> rotate<0,44.997030,0> translate<3.810000,-1.535000,6.350000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<5.080000,-1.535000,7.620000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.620000,-1.535000,5.080000>}
box{<0,0,-0.254000><3.592102,0.035000,0.254000> rotate<0,44.997030,0> translate<5.080000,-1.535000,7.620000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.620000,-1.535000,10.160000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.620000,-1.535000,13.970000>}
box{<0,0,-0.254000><3.810000,0.035000,0.254000> rotate<0,90.000000,0> translate<7.620000,-1.535000,13.970000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.350000,-1.535000,8.890000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<10.160000,-1.535000,5.080000>}
box{<0,0,-0.254000><5.388154,0.035000,0.254000> rotate<0,44.997030,0> translate<6.350000,-1.535000,8.890000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<10.160000,-1.535000,21.590000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<10.160000,-1.535000,30.480000>}
box{<0,0,-0.254000><8.890000,0.035000,0.254000> rotate<0,90.000000,0> translate<10.160000,-1.535000,30.480000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<10.160000,-1.535000,45.720000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<10.160000,-1.535000,57.150000>}
box{<0,0,-0.254000><11.430000,0.035000,0.254000> rotate<0,90.000000,0> translate<10.160000,-1.535000,57.150000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.620000,-1.535000,13.970000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<11.430000,-1.535000,17.780000>}
box{<0,0,-0.254000><5.388154,0.035000,0.254000> rotate<0,-44.997030,0> translate<7.620000,-1.535000,13.970000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.350000,-1.535000,55.880000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<11.430000,-1.535000,60.960000>}
box{<0,0,-0.254000><7.184205,0.035000,0.254000> rotate<0,-44.997030,0> translate<6.350000,-1.535000,55.880000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<5.080000,-1.535000,55.880000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<11.430000,-1.535000,62.230000>}
box{<0,0,-0.254000><8.980256,0.035000,0.254000> rotate<0,-44.997030,0> translate<5.080000,-1.535000,55.880000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.620000,-1.535000,10.160000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<12.700000,-1.535000,5.080000>}
box{<0,0,-0.254000><7.184205,0.035000,0.254000> rotate<0,44.997030,0> translate<7.620000,-1.535000,10.160000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<10.160000,-1.535000,13.970000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<12.700000,-1.535000,16.510000>}
box{<0,0,-0.254000><3.592102,0.035000,0.254000> rotate<0,-44.997030,0> translate<10.160000,-1.535000,13.970000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<12.700000,-1.535000,21.590000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<12.700000,-1.535000,30.480000>}
box{<0,0,-0.254000><8.890000,0.035000,0.254000> rotate<0,90.000000,0> translate<12.700000,-1.535000,30.480000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<12.700000,-1.535000,45.720000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<12.700000,-1.535000,57.150000>}
box{<0,0,-0.254000><11.430000,0.035000,0.254000> rotate<0,90.000000,0> translate<12.700000,-1.535000,57.150000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<3.810000,-1.535000,55.880000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<12.700000,-1.535000,64.770000>}
box{<0,0,-0.254000><12.572359,0.035000,0.254000> rotate<0,-44.997030,0> translate<3.810000,-1.535000,55.880000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<10.160000,-1.535000,64.770000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<12.700000,-1.535000,67.310000>}
box{<0,0,-0.254000><3.592102,0.035000,0.254000> rotate<0,-44.997030,0> translate<10.160000,-1.535000,64.770000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<11.430000,-1.535000,17.780000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<13.970000,-1.535000,17.780000>}
box{<0,0,-0.254000><2.540000,0.035000,0.254000> rotate<0,0.000000,0> translate<11.430000,-1.535000,17.780000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<12.700000,-1.535000,13.970000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<15.240000,-1.535000,11.430000>}
box{<0,0,-0.254000><3.592102,0.035000,0.254000> rotate<0,44.997030,0> translate<12.700000,-1.535000,13.970000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<15.240000,-1.535000,5.080000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<15.240000,-1.535000,11.430000>}
box{<0,0,-0.254000><6.350000,0.035000,0.254000> rotate<0,90.000000,0> translate<15.240000,-1.535000,11.430000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<15.240000,-1.535000,38.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<15.240000,-1.535000,30.480000>}
box{<0,0,-0.254000><7.620000,0.035000,0.254000> rotate<0,-90.000000,0> translate<15.240000,-1.535000,30.480000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<15.240000,-1.535000,38.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<15.240000,-1.535000,45.720000>}
box{<0,0,-0.254000><7.620000,0.035000,0.254000> rotate<0,90.000000,0> translate<15.240000,-1.535000,45.720000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<11.430000,-1.535000,62.230000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<15.240000,-1.535000,62.230000>}
box{<0,0,-0.254000><3.810000,0.035000,0.254000> rotate<0,0.000000,0> translate<11.430000,-1.535000,62.230000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<11.430000,-1.535000,60.960000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<16.510000,-1.535000,60.960000>}
box{<0,0,-0.254000><5.080000,0.035000,0.254000> rotate<0,0.000000,0> translate<11.430000,-1.535000,60.960000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.620000,-1.535000,2.540000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<17.780000,-1.535000,2.540000>}
box{<0,0,-0.254000><10.160000,0.035000,0.254000> rotate<0,0.000000,0> translate<7.620000,-1.535000,2.540000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<12.700000,-1.535000,16.510000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<17.780000,-1.535000,11.430000>}
box{<0,0,-0.254000><7.184205,0.035000,0.254000> rotate<0,44.997030,0> translate<12.700000,-1.535000,16.510000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<17.780000,-1.535000,5.080000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<17.780000,-1.535000,11.430000>}
box{<0,0,-0.254000><6.350000,0.035000,0.254000> rotate<0,90.000000,0> translate<17.780000,-1.535000,11.430000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<13.970000,-1.535000,17.780000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<17.780000,-1.535000,13.970000>}
box{<0,0,-0.254000><5.388154,0.035000,0.254000> rotate<0,44.997030,0> translate<13.970000,-1.535000,17.780000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<17.780000,-1.535000,21.590000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<17.780000,-1.535000,30.480000>}
box{<0,0,-0.254000><8.890000,0.035000,0.254000> rotate<0,90.000000,0> translate<17.780000,-1.535000,30.480000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<17.780000,-1.535000,45.720000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<17.780000,-1.535000,57.150000>}
box{<0,0,-0.254000><11.430000,0.035000,0.254000> rotate<0,90.000000,0> translate<17.780000,-1.535000,57.150000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<15.240000,-1.535000,62.230000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<17.780000,-1.535000,64.770000>}
box{<0,0,-0.254000><3.592102,0.035000,0.254000> rotate<0,-44.997030,0> translate<15.240000,-1.535000,62.230000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<17.780000,-1.535000,2.540000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<20.320000,-1.535000,5.080000>}
box{<0,0,-0.254000><3.592102,0.035000,0.254000> rotate<0,-44.997030,0> translate<17.780000,-1.535000,2.540000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<20.320000,-1.535000,21.590000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<20.320000,-1.535000,30.480000>}
box{<0,0,-0.254000><8.890000,0.035000,0.254000> rotate<0,90.000000,0> translate<20.320000,-1.535000,30.480000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<20.320000,-1.535000,45.720000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<20.320000,-1.535000,57.150000>}
box{<0,0,-0.254000><11.430000,0.035000,0.254000> rotate<0,90.000000,0> translate<20.320000,-1.535000,57.150000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<16.510000,-1.535000,60.960000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<20.320000,-1.535000,64.770000>}
box{<0,0,-0.254000><5.388154,0.035000,0.254000> rotate<0,-44.997030,0> translate<16.510000,-1.535000,60.960000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<12.700000,-1.535000,67.310000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<20.320000,-1.535000,67.310000>}
box{<0,0,-0.254000><7.620000,0.035000,0.254000> rotate<0,0.000000,0> translate<12.700000,-1.535000,67.310000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<22.860000,-1.535000,5.080000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<25.400000,-1.535000,2.540000>}
box{<0,0,-0.254000><3.592102,0.035000,0.254000> rotate<0,44.997030,0> translate<22.860000,-1.535000,5.080000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<20.320000,-1.535000,13.970000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<25.400000,-1.535000,8.890000>}
box{<0,0,-0.254000><7.184205,0.035000,0.254000> rotate<0,44.997030,0> translate<20.320000,-1.535000,13.970000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<25.400000,-1.535000,5.080000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<25.400000,-1.535000,8.890000>}
box{<0,0,-0.254000><3.810000,0.035000,0.254000> rotate<0,90.000000,0> translate<25.400000,-1.535000,8.890000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<15.240000,-1.535000,38.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<27.940000,-1.535000,25.400000>}
box{<0,0,-0.254000><17.960512,0.035000,0.254000> rotate<0,44.997030,0> translate<15.240000,-1.535000,38.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<27.940000,-1.535000,5.080000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<27.940000,-1.535000,25.400000>}
box{<0,0,-0.254000><20.320000,0.035000,0.254000> rotate<0,90.000000,0> translate<27.940000,-1.535000,25.400000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<25.400000,-1.535000,2.540000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<30.480000,-1.535000,2.540000>}
box{<0,0,-0.254000><5.080000,0.035000,0.254000> rotate<0,0.000000,0> translate<25.400000,-1.535000,2.540000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<27.940000,-1.535000,5.080000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<30.480000,-1.535000,5.080000>}
box{<0,0,-0.254000><2.540000,0.035000,0.254000> rotate<0,0.000000,0> translate<27.940000,-1.535000,5.080000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<30.480000,-1.535000,2.540000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<33.020000,-1.535000,5.080000>}
box{<0,0,-0.254000><3.592102,0.035000,0.254000> rotate<0,-44.997030,0> translate<30.480000,-1.535000,2.540000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<20.320000,-1.535000,67.310000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<33.020000,-1.535000,54.610000>}
box{<0,0,-0.254000><17.960512,0.035000,0.254000> rotate<0,44.997030,0> translate<20.320000,-1.535000,67.310000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<33.020000,-1.535000,5.080000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<33.020000,-1.535000,54.610000>}
box{<0,0,-0.254000><49.530000,0.035000,0.254000> rotate<0,90.000000,0> translate<33.020000,-1.535000,54.610000> }
//Text
//Rect
union{
texture{col_pds}
}
texture{col_wrs}
}
#end
#if(pcb_polygons=on)
union{
//Polygons
texture{col_pol}
}
#end
union{
cylinder{<30.480000,0.038000,5.080000><30.480000,-1.538000,5.080000>0.508000}
cylinder{<27.940000,0.038000,5.080000><27.940000,-1.538000,5.080000>0.508000}
cylinder{<25.400000,0.038000,5.080000><25.400000,-1.538000,5.080000>0.508000}
cylinder{<22.860000,0.038000,5.080000><22.860000,-1.538000,5.080000>0.508000}
cylinder{<20.320000,0.038000,5.080000><20.320000,-1.538000,5.080000>0.508000}
cylinder{<17.780000,0.038000,5.080000><17.780000,-1.538000,5.080000>0.508000}
cylinder{<15.240000,0.038000,5.080000><15.240000,-1.538000,5.080000>0.508000}
cylinder{<12.700000,0.038000,5.080000><12.700000,-1.538000,5.080000>0.508000}
cylinder{<10.160000,0.038000,5.080000><10.160000,-1.538000,5.080000>0.508000}
cylinder{<7.620000,0.038000,5.080000><7.620000,-1.538000,5.080000>0.508000}
cylinder{<10.160000,0.038000,30.480000><10.160000,-1.538000,30.480000>0.406400}
cylinder{<12.700000,0.038000,30.480000><12.700000,-1.538000,30.480000>0.406400}
cylinder{<15.240000,0.038000,30.480000><15.240000,-1.538000,30.480000>0.406400}
cylinder{<17.780000,0.038000,30.480000><17.780000,-1.538000,30.480000>0.406400}
cylinder{<20.320000,0.038000,30.480000><20.320000,-1.538000,30.480000>0.406400}
cylinder{<20.320000,0.038000,45.720000><20.320000,-1.538000,45.720000>0.406400}
cylinder{<17.780000,0.038000,45.720000><17.780000,-1.538000,45.720000>0.406400}
cylinder{<15.240000,0.038000,45.720000><15.240000,-1.538000,45.720000>0.406400}
cylinder{<12.700000,0.038000,45.720000><12.700000,-1.538000,45.720000>0.406400}
cylinder{<10.160000,0.038000,45.720000><10.160000,-1.538000,45.720000>0.406400}
cylinder{<17.780000,0.038000,64.770000><17.780000,-1.538000,64.770000>0.450000}
cylinder{<17.780000,0.038000,57.150000><17.780000,-1.538000,57.150000>0.450000}
cylinder{<20.320000,0.038000,64.770000><20.320000,-1.538000,64.770000>0.450000}
cylinder{<20.320000,0.038000,57.150000><20.320000,-1.538000,57.150000>0.450000}
cylinder{<17.780000,0.038000,13.970000><17.780000,-1.538000,13.970000>0.450000}
cylinder{<17.780000,0.038000,21.590000><17.780000,-1.538000,21.590000>0.450000}
cylinder{<12.700000,0.038000,13.970000><12.700000,-1.538000,13.970000>0.450000}
cylinder{<12.700000,0.038000,21.590000><12.700000,-1.538000,21.590000>0.450000}
cylinder{<20.320000,0.038000,13.970000><20.320000,-1.538000,13.970000>0.450000}
cylinder{<20.320000,0.038000,21.590000><20.320000,-1.538000,21.590000>0.450000}
cylinder{<10.160000,0.038000,13.970000><10.160000,-1.538000,13.970000>0.450000}
cylinder{<10.160000,0.038000,21.590000><10.160000,-1.538000,21.590000>0.450000}
cylinder{<12.700000,0.038000,64.770000><12.700000,-1.538000,64.770000>0.450000}
cylinder{<12.700000,0.038000,57.150000><12.700000,-1.538000,57.150000>0.450000}
cylinder{<10.160000,0.038000,64.770000><10.160000,-1.538000,64.770000>0.450000}
cylinder{<10.160000,0.038000,57.150000><10.160000,-1.538000,57.150000>0.450000}
//Holes(fast)/Vias
//Holes(fast)/Board
texture{col_hls}
}
#if(pcb_silkscreen=on)
//Silk Screen
union{
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<29.359500,0.000000,12.852400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<29.359500,0.000000,15.191100>}
box{<0,0,-0.152400><2.338700,0.036000,0.152400> rotate<0,90.000000,0> translate<29.359500,0.000000,15.191100> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<29.359500,0.000000,15.191100>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<29.944200,0.000000,15.191100>}
box{<0,0,-0.152400><0.584700,0.036000,0.152400> rotate<0,0.000000,0> translate<29.359500,0.000000,15.191100> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<29.944200,0.000000,15.191100>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.283000,0.000000,12.852400>}
box{<0,0,-0.152400><3.307492,0.036000,0.152400> rotate<0,44.995805,0> translate<29.944200,0.000000,15.191100> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.283000,0.000000,12.852400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,12.852400>}
box{<0,0,-0.152400><0.584600,0.036000,0.152400> rotate<0,0.000000,0> translate<32.283000,0.000000,12.852400> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,19.868600>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,21.622600>}
box{<0,0,-0.152400><1.754000,0.036000,0.152400> rotate<0,90.000000,0> translate<32.867600,0.000000,21.622600> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,21.622600>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.283000,0.000000,22.207300>}
box{<0,0,-0.152400><0.826820,0.036000,0.152400> rotate<0,45.001930,0> translate<32.283000,0.000000,22.207300> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.283000,0.000000,22.207300>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.698300,0.000000,21.622600>}
box{<0,0,-0.152400><0.826891,0.036000,0.152400> rotate<0,-44.997030,0> translate<31.698300,0.000000,21.622600> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.698300,0.000000,21.622600>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.698300,0.000000,20.453200>}
box{<0,0,-0.152400><1.169400,0.036000,0.152400> rotate<0,-90.000000,0> translate<31.698300,0.000000,20.453200> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.698300,0.000000,20.453200>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,19.868600>}
box{<0,0,-0.152400><0.826820,0.036000,0.152400> rotate<0,-44.992130,0> translate<31.113600,0.000000,19.868600> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,19.868600>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,20.453200>}
box{<0,0,-0.152400><0.826820,0.036000,0.152400> rotate<0,44.992130,0> translate<30.528900,0.000000,20.453200> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,20.453200>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,22.207300>}
box{<0,0,-0.152400><1.754100,0.036000,0.152400> rotate<0,90.000000,0> translate<30.528900,0.000000,22.207300> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,23.376700>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,23.961300>}
box{<0,0,-0.152400><0.584600,0.036000,0.152400> rotate<0,90.000000,0> translate<30.528900,0.000000,23.961300> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,23.961300>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,23.961300>}
box{<0,0,-0.152400><2.338700,0.036000,0.152400> rotate<0,0.000000,0> translate<30.528900,0.000000,23.961300> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,23.376700>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,24.546000>}
box{<0,0,-0.152400><1.169300,0.036000,0.152400> rotate<0,90.000000,0> translate<32.867600,0.000000,24.546000> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<28.774800,0.000000,23.961300>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<29.359500,0.000000,23.961300>}
box{<0,0,-0.152400><0.584700,0.036000,0.152400> rotate<0,0.000000,0> translate<28.774800,0.000000,23.961300> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<29.359500,0.000000,25.715400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<29.359500,0.000000,26.300000>}
box{<0,0,-0.152400><0.584600,0.036000,0.152400> rotate<0,90.000000,0> translate<29.359500,0.000000,26.300000> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<29.359500,0.000000,26.300000>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,26.300000>}
box{<0,0,-0.152400><3.508100,0.036000,0.152400> rotate<0,0.000000,0> translate<29.359500,0.000000,26.300000> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,25.715400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,26.884700>}
box{<0,0,-0.152400><1.169300,0.036000,0.152400> rotate<0,90.000000,0> translate<32.867600,0.000000,26.884700> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<29.359500,0.000000,28.054100>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<29.359500,0.000000,28.638700>}
box{<0,0,-0.152400><0.584600,0.036000,0.152400> rotate<0,90.000000,0> translate<29.359500,0.000000,28.638700> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<29.359500,0.000000,28.638700>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,28.638700>}
box{<0,0,-0.152400><3.508100,0.036000,0.152400> rotate<0,0.000000,0> translate<29.359500,0.000000,28.638700> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,28.054100>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,29.223400>}
box{<0,0,-0.152400><1.169300,0.036000,0.152400> rotate<0,90.000000,0> translate<32.867600,0.000000,29.223400> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,30.392800>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.283000,0.000000,30.392800>}
box{<0,0,-0.152400><1.754100,0.036000,0.152400> rotate<0,0.000000,0> translate<30.528900,0.000000,30.392800> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.283000,0.000000,30.392800>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,30.977400>}
box{<0,0,-0.152400><0.826749,0.036000,0.152400> rotate<0,-44.997030,0> translate<32.283000,0.000000,30.392800> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,30.977400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,32.731500>}
box{<0,0,-0.152400><1.754100,0.036000,0.152400> rotate<0,90.000000,0> translate<32.867600,0.000000,32.731500> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,32.731500>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<33.452200,0.000000,32.731500>}
box{<0,0,-0.152400><2.923300,0.036000,0.152400> rotate<0,0.000000,0> translate<30.528900,0.000000,32.731500> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<33.452200,0.000000,32.731500>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<34.036900,0.000000,32.146800>}
box{<0,0,-0.152400><0.826891,0.036000,0.152400> rotate<0,44.997030,0> translate<33.452200,0.000000,32.731500> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<34.036900,0.000000,32.146800>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<34.036900,0.000000,31.562100>}
box{<0,0,-0.152400><0.584700,0.036000,0.152400> rotate<0,-90.000000,0> translate<34.036900,0.000000,31.562100> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,37.409000>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,39.163000>}
box{<0,0,-0.152400><1.754000,0.036000,0.152400> rotate<0,90.000000,0> translate<32.867600,0.000000,39.163000> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,39.163000>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.283000,0.000000,39.747700>}
box{<0,0,-0.152400><0.826820,0.036000,0.152400> rotate<0,45.001930,0> translate<32.283000,0.000000,39.747700> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.283000,0.000000,39.747700>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.698300,0.000000,39.163000>}
box{<0,0,-0.152400><0.826891,0.036000,0.152400> rotate<0,-44.997030,0> translate<31.698300,0.000000,39.163000> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.698300,0.000000,39.163000>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.698300,0.000000,37.993600>}
box{<0,0,-0.152400><1.169400,0.036000,0.152400> rotate<0,-90.000000,0> translate<31.698300,0.000000,37.993600> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.698300,0.000000,37.993600>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,37.409000>}
box{<0,0,-0.152400><0.826820,0.036000,0.152400> rotate<0,-44.992130,0> translate<31.113600,0.000000,37.409000> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,37.409000>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,37.993600>}
box{<0,0,-0.152400><0.826820,0.036000,0.152400> rotate<0,44.992130,0> translate<30.528900,0.000000,37.993600> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,37.993600>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,39.747700>}
box{<0,0,-0.152400><1.754100,0.036000,0.152400> rotate<0,90.000000,0> translate<30.528900,0.000000,39.747700> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,42.671100>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,41.501700>}
box{<0,0,-0.152400><1.169400,0.036000,0.152400> rotate<0,-90.000000,0> translate<32.867600,0.000000,41.501700> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,41.501700>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.283000,0.000000,40.917100>}
box{<0,0,-0.152400><0.826749,0.036000,0.152400> rotate<0,-44.997030,0> translate<32.283000,0.000000,40.917100> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.283000,0.000000,40.917100>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,40.917100>}
box{<0,0,-0.152400><1.169400,0.036000,0.152400> rotate<0,0.000000,0> translate<31.113600,0.000000,40.917100> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,40.917100>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,41.501700>}
box{<0,0,-0.152400><0.826820,0.036000,0.152400> rotate<0,44.992130,0> translate<30.528900,0.000000,41.501700> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,41.501700>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,42.671100>}
box{<0,0,-0.152400><1.169400,0.036000,0.152400> rotate<0,90.000000,0> translate<30.528900,0.000000,42.671100> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,42.671100>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,43.255800>}
box{<0,0,-0.152400><0.826891,0.036000,0.152400> rotate<0,-44.997030,0> translate<30.528900,0.000000,42.671100> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,43.255800>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.698300,0.000000,43.255800>}
box{<0,0,-0.152400><0.584700,0.036000,0.152400> rotate<0,0.000000,0> translate<31.113600,0.000000,43.255800> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.698300,0.000000,43.255800>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.698300,0.000000,40.917100>}
box{<0,0,-0.152400><2.338700,0.036000,0.152400> rotate<0,-90.000000,0> translate<31.698300,0.000000,40.917100> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<34.036900,0.000000,45.594500>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<34.036900,0.000000,46.179200>}
box{<0,0,-0.152400><0.584700,0.036000,0.152400> rotate<0,90.000000,0> translate<34.036900,0.000000,46.179200> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<34.036900,0.000000,46.179200>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<33.452200,0.000000,46.763900>}
box{<0,0,-0.152400><0.826891,0.036000,0.152400> rotate<0,44.997030,0> translate<33.452200,0.000000,46.763900> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<33.452200,0.000000,46.763900>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,46.763900>}
box{<0,0,-0.152400><2.923300,0.036000,0.152400> rotate<0,0.000000,0> translate<30.528900,0.000000,46.763900> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,46.763900>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,45.009800>}
box{<0,0,-0.152400><1.754100,0.036000,0.152400> rotate<0,-90.000000,0> translate<30.528900,0.000000,45.009800> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,45.009800>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,44.425200>}
box{<0,0,-0.152400><0.826820,0.036000,0.152400> rotate<0,44.992130,0> translate<30.528900,0.000000,45.009800> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,44.425200>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.283000,0.000000,44.425200>}
box{<0,0,-0.152400><1.169400,0.036000,0.152400> rotate<0,0.000000,0> translate<31.113600,0.000000,44.425200> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.283000,0.000000,44.425200>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,45.009800>}
box{<0,0,-0.152400><0.826749,0.036000,0.152400> rotate<0,-44.997030,0> translate<32.283000,0.000000,44.425200> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,45.009800>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,46.763900>}
box{<0,0,-0.152400><1.754100,0.036000,0.152400> rotate<0,90.000000,0> translate<32.867600,0.000000,46.763900> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,47.933300>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,47.933300>}
box{<0,0,-0.152400><2.338700,0.036000,0.152400> rotate<0,0.000000,0> translate<30.528900,0.000000,47.933300> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,47.933300>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,48.517900>}
box{<0,0,-0.152400><0.584600,0.036000,0.152400> rotate<0,90.000000,0> translate<30.528900,0.000000,48.517900> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,48.517900>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,49.102600>}
box{<0,0,-0.152400><0.826891,0.036000,0.152400> rotate<0,-44.997030,0> translate<30.528900,0.000000,48.517900> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,49.102600>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,49.102600>}
box{<0,0,-0.152400><1.754000,0.036000,0.152400> rotate<0,0.000000,0> translate<31.113600,0.000000,49.102600> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,49.102600>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,49.687300>}
box{<0,0,-0.152400><0.826891,0.036000,0.152400> rotate<0,44.997030,0> translate<30.528900,0.000000,49.687300> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,49.687300>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,50.272000>}
box{<0,0,-0.152400><0.826891,0.036000,0.152400> rotate<0,-44.997030,0> translate<30.528900,0.000000,49.687300> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,50.272000>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,50.272000>}
box{<0,0,-0.152400><1.754000,0.036000,0.152400> rotate<0,0.000000,0> translate<31.113600,0.000000,50.272000> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,53.195400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,52.026000>}
box{<0,0,-0.152400><1.169400,0.036000,0.152400> rotate<0,-90.000000,0> translate<32.867600,0.000000,52.026000> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,52.026000>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.283000,0.000000,51.441400>}
box{<0,0,-0.152400><0.826749,0.036000,0.152400> rotate<0,-44.997030,0> translate<32.283000,0.000000,51.441400> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.283000,0.000000,51.441400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,51.441400>}
box{<0,0,-0.152400><1.169400,0.036000,0.152400> rotate<0,0.000000,0> translate<31.113600,0.000000,51.441400> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,51.441400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,52.026000>}
box{<0,0,-0.152400><0.826820,0.036000,0.152400> rotate<0,44.992130,0> translate<30.528900,0.000000,52.026000> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,52.026000>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,53.195400>}
box{<0,0,-0.152400><1.169400,0.036000,0.152400> rotate<0,90.000000,0> translate<30.528900,0.000000,53.195400> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,53.195400>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,53.780100>}
box{<0,0,-0.152400><0.826891,0.036000,0.152400> rotate<0,-44.997030,0> translate<30.528900,0.000000,53.195400> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,53.780100>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.698300,0.000000,53.780100>}
box{<0,0,-0.152400><0.584700,0.036000,0.152400> rotate<0,0.000000,0> translate<31.113600,0.000000,53.780100> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.698300,0.000000,53.780100>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.698300,0.000000,51.441400>}
box{<0,0,-0.152400><2.338700,0.036000,0.152400> rotate<0,-90.000000,0> translate<31.698300,0.000000,51.441400> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,54.949500>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,54.949500>}
box{<0,0,-0.152400><2.338700,0.036000,0.152400> rotate<0,0.000000,0> translate<30.528900,0.000000,54.949500> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,54.949500>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,56.703500>}
box{<0,0,-0.152400><1.754000,0.036000,0.152400> rotate<0,90.000000,0> translate<30.528900,0.000000,56.703500> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,56.703500>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,57.288200>}
box{<0,0,-0.152400><0.826891,0.036000,0.152400> rotate<0,-44.997030,0> translate<30.528900,0.000000,56.703500> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,57.288200>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,57.288200>}
box{<0,0,-0.152400><1.754000,0.036000,0.152400> rotate<0,0.000000,0> translate<31.113600,0.000000,57.288200> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<29.944200,0.000000,59.042200>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.283000,0.000000,59.042200>}
box{<0,0,-0.152400><2.338800,0.036000,0.152400> rotate<0,0.000000,0> translate<29.944200,0.000000,59.042200> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.283000,0.000000,59.042200>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,59.626900>}
box{<0,0,-0.152400><0.826820,0.036000,0.152400> rotate<0,-45.001930,0> translate<32.283000,0.000000,59.042200> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,58.457600>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,59.626900>}
box{<0,0,-0.152400><1.169300,0.036000,0.152400> rotate<0,90.000000,0> translate<30.528900,0.000000,59.626900> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,60.796300>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,62.550300>}
box{<0,0,-0.152400><1.754000,0.036000,0.152400> rotate<0,90.000000,0> translate<32.867600,0.000000,62.550300> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.867600,0.000000,62.550300>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.283000,0.000000,63.135000>}
box{<0,0,-0.152400><0.826820,0.036000,0.152400> rotate<0,45.001930,0> translate<32.283000,0.000000,63.135000> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<32.283000,0.000000,63.135000>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.698300,0.000000,62.550300>}
box{<0,0,-0.152400><0.826891,0.036000,0.152400> rotate<0,-44.997030,0> translate<31.698300,0.000000,62.550300> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.698300,0.000000,62.550300>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.698300,0.000000,61.380900>}
box{<0,0,-0.152400><1.169400,0.036000,0.152400> rotate<0,-90.000000,0> translate<31.698300,0.000000,61.380900> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.698300,0.000000,61.380900>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,60.796300>}
box{<0,0,-0.152400><0.826820,0.036000,0.152400> rotate<0,-44.992130,0> translate<31.113600,0.000000,60.796300> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<31.113600,0.000000,60.796300>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,61.380900>}
box{<0,0,-0.152400><0.826820,0.036000,0.152400> rotate<0,44.992130,0> translate<30.528900,0.000000,61.380900> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,61.380900>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<30.528900,0.000000,63.135000>}
box{<0,0,-0.152400><1.754100,0.036000,0.152400> rotate<0,90.000000,0> translate<30.528900,0.000000,63.135000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.205300,0.000000,2.938800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.747600,0.000000,2.938800>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<7.205300,0.000000,2.938800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.747600,0.000000,2.938800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.018800,0.000000,2.667600>}
box{<0,0,-0.076200><0.383535,0.036000,0.076200> rotate<0,44.997030,0> translate<7.747600,0.000000,2.938800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.018800,0.000000,2.667600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.018800,0.000000,1.854200>}
box{<0,0,-0.076200><0.813400,0.036000,0.076200> rotate<0,-90.000000,0> translate<8.018800,0.000000,1.854200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.018800,0.000000,1.854200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.205300,0.000000,1.854200>}
box{<0,0,-0.076200><0.813500,0.036000,0.076200> rotate<0,0.000000,0> translate<7.205300,0.000000,1.854200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.205300,0.000000,1.854200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.934200,0.000000,2.125300>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<6.934200,0.000000,2.125300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.934200,0.000000,2.125300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.205300,0.000000,2.396500>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-45.007595,0> translate<6.934200,0.000000,2.125300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.205300,0.000000,2.396500>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.018800,0.000000,2.396500>}
box{<0,0,-0.076200><0.813500,0.036000,0.076200> rotate<0,0.000000,0> translate<7.205300,0.000000,2.396500> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.474200,0.000000,3.481100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.474200,0.000000,1.854200>}
box{<0,0,-0.076200><1.626900,0.036000,0.076200> rotate<0,-90.000000,0> translate<9.474200,0.000000,1.854200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.474200,0.000000,1.854200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.287600,0.000000,1.854200>}
box{<0,0,-0.076200><0.813400,0.036000,0.076200> rotate<0,0.000000,0> translate<9.474200,0.000000,1.854200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.287600,0.000000,1.854200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.558800,0.000000,2.125300>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-44.986466,0> translate<10.287600,0.000000,1.854200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.558800,0.000000,2.125300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.558800,0.000000,2.667600>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,90.000000,0> translate<10.558800,0.000000,2.667600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.558800,0.000000,2.667600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.287600,0.000000,2.938800>}
box{<0,0,-0.076200><0.383535,0.036000,0.076200> rotate<0,44.997030,0> translate<10.287600,0.000000,2.938800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.287600,0.000000,2.938800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.474200,0.000000,2.938800>}
box{<0,0,-0.076200><0.813400,0.036000,0.076200> rotate<0,0.000000,0> translate<9.474200,0.000000,2.938800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.098800,0.000000,2.938800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.285300,0.000000,2.938800>}
box{<0,0,-0.076200><0.813500,0.036000,0.076200> rotate<0,0.000000,0> translate<12.285300,0.000000,2.938800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.285300,0.000000,2.938800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.014200,0.000000,2.667600>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-45.007595,0> translate<12.014200,0.000000,2.667600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.014200,0.000000,2.667600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.014200,0.000000,2.125300>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,-90.000000,0> translate<12.014200,0.000000,2.125300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.014200,0.000000,2.125300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.285300,0.000000,1.854200>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<12.014200,0.000000,2.125300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.285300,0.000000,1.854200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.098800,0.000000,1.854200>}
box{<0,0,-0.076200><0.813500,0.036000,0.076200> rotate<0,0.000000,0> translate<12.285300,0.000000,1.854200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.892800,0.000000,3.481100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.892800,0.000000,1.854200>}
box{<0,0,-0.076200><1.626900,0.036000,0.076200> rotate<0,-90.000000,0> translate<15.892800,0.000000,1.854200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.892800,0.000000,1.854200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.079300,0.000000,1.854200>}
box{<0,0,-0.076200><0.813500,0.036000,0.076200> rotate<0,0.000000,0> translate<15.079300,0.000000,1.854200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.079300,0.000000,1.854200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.808200,0.000000,2.125300>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<14.808200,0.000000,2.125300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.808200,0.000000,2.125300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.808200,0.000000,2.667600>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,90.000000,0> translate<14.808200,0.000000,2.667600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.808200,0.000000,2.667600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.079300,0.000000,2.938800>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-45.007595,0> translate<14.808200,0.000000,2.667600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.079300,0.000000,2.938800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.892800,0.000000,2.938800>}
box{<0,0,-0.076200><0.813500,0.036000,0.076200> rotate<0,0.000000,0> translate<15.079300,0.000000,2.938800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.161600,0.000000,1.854200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.619300,0.000000,1.854200>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<17.619300,0.000000,1.854200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.619300,0.000000,1.854200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.348200,0.000000,2.125300>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<17.348200,0.000000,2.125300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.348200,0.000000,2.125300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.348200,0.000000,2.667600>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,90.000000,0> translate<17.348200,0.000000,2.667600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.348200,0.000000,2.667600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.619300,0.000000,2.938800>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-45.007595,0> translate<17.348200,0.000000,2.667600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.619300,0.000000,2.938800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.161600,0.000000,2.938800>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<17.619300,0.000000,2.938800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.161600,0.000000,2.938800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.432800,0.000000,2.667600>}
box{<0,0,-0.076200><0.383535,0.036000,0.076200> rotate<0,44.997030,0> translate<18.161600,0.000000,2.938800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.432800,0.000000,2.667600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.432800,0.000000,2.396500>}
box{<0,0,-0.076200><0.271100,0.036000,0.076200> rotate<0,-90.000000,0> translate<18.432800,0.000000,2.396500> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.432800,0.000000,2.396500>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.348200,0.000000,2.396500>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<17.348200,0.000000,2.396500> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<20.413300,0.000000,1.854200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<20.413300,0.000000,3.210000>}
box{<0,0,-0.076200><1.355800,0.036000,0.076200> rotate<0,90.000000,0> translate<20.413300,0.000000,3.210000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<20.413300,0.000000,3.210000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<20.684500,0.000000,3.481100>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-44.986466,0> translate<20.413300,0.000000,3.210000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<20.142200,0.000000,2.667600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<20.684500,0.000000,2.667600>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<20.142200,0.000000,2.667600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.970500,0.000000,1.311900>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.241600,0.000000,1.311900>}
box{<0,0,-0.076200><0.271100,0.036000,0.076200> rotate<0,0.000000,0> translate<22.970500,0.000000,1.311900> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.241600,0.000000,1.311900>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.512800,0.000000,1.583100>}
box{<0,0,-0.076200><0.383535,0.036000,0.076200> rotate<0,-44.997030,0> translate<23.241600,0.000000,1.311900> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.512800,0.000000,1.583100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.512800,0.000000,2.938800>}
box{<0,0,-0.076200><1.355700,0.036000,0.076200> rotate<0,90.000000,0> translate<23.512800,0.000000,2.938800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.512800,0.000000,2.938800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.699300,0.000000,2.938800>}
box{<0,0,-0.076200><0.813500,0.036000,0.076200> rotate<0,0.000000,0> translate<22.699300,0.000000,2.938800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.699300,0.000000,2.938800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.428200,0.000000,2.667600>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-45.007595,0> translate<22.428200,0.000000,2.667600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.428200,0.000000,2.667600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.428200,0.000000,2.125300>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,-90.000000,0> translate<22.428200,0.000000,2.125300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.428200,0.000000,2.125300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.699300,0.000000,1.854200>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<22.428200,0.000000,2.125300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.699300,0.000000,1.854200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.512800,0.000000,1.854200>}
box{<0,0,-0.076200><0.813500,0.036000,0.076200> rotate<0,0.000000,0> translate<22.699300,0.000000,1.854200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.968200,0.000000,3.481100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.968200,0.000000,1.854200>}
box{<0,0,-0.076200><1.626900,0.036000,0.076200> rotate<0,-90.000000,0> translate<24.968200,0.000000,1.854200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.968200,0.000000,1.854200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<25.781600,0.000000,1.854200>}
box{<0,0,-0.076200><0.813400,0.036000,0.076200> rotate<0,0.000000,0> translate<24.968200,0.000000,1.854200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<25.781600,0.000000,1.854200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<26.052800,0.000000,2.125300>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-44.986466,0> translate<25.781600,0.000000,1.854200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<26.052800,0.000000,2.125300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<26.052800,0.000000,3.210000>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,90.000000,0> translate<26.052800,0.000000,3.210000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<26.052800,0.000000,3.210000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<25.781600,0.000000,3.481100>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,44.986466,0> translate<25.781600,0.000000,3.481100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<25.781600,0.000000,3.481100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.968200,0.000000,3.481100>}
box{<0,0,-0.076200><0.813400,0.036000,0.076200> rotate<0,0.000000,0> translate<24.968200,0.000000,3.481100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<27.508200,0.000000,3.481100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<27.508200,0.000000,1.854200>}
box{<0,0,-0.076200><1.626900,0.036000,0.076200> rotate<0,-90.000000,0> translate<27.508200,0.000000,1.854200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<27.508200,0.000000,2.396500>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<28.592800,0.000000,3.481100>}
box{<0,0,-0.076200><1.533856,0.036000,0.076200> rotate<0,-44.997030,0> translate<27.508200,0.000000,2.396500> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<27.779300,0.000000,2.667600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<28.592800,0.000000,1.854200>}
box{<0,0,-0.076200><1.150392,0.036000,0.076200> rotate<0,44.993509,0> translate<27.779300,0.000000,2.667600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.696200,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.509600,0.000000,68.656200>}
box{<0,0,-0.076200><0.813400,0.036000,0.076200> rotate<0,0.000000,0> translate<7.696200,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.509600,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.780800,0.000000,68.927300>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-44.986466,0> translate<8.509600,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.780800,0.000000,68.927300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.509600,0.000000,69.198500>}
box{<0,0,-0.076200><0.383535,0.036000,0.076200> rotate<0,44.997030,0> translate<8.509600,0.000000,69.198500> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.509600,0.000000,69.198500>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.967300,0.000000,69.198500>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<7.967300,0.000000,69.198500> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.967300,0.000000,69.198500>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.696200,0.000000,69.469600>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<7.696200,0.000000,69.469600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.696200,0.000000,69.469600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.967300,0.000000,69.740800>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-45.007595,0> translate<7.696200,0.000000,69.469600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.967300,0.000000,69.740800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.780800,0.000000,69.740800>}
box{<0,0,-0.076200><0.813500,0.036000,0.076200> rotate<0,0.000000,0> translate<7.967300,0.000000,69.740800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.604400,0.000000,69.740800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.146700,0.000000,69.740800>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<9.604400,0.000000,69.740800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.146700,0.000000,69.740800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.417900,0.000000,69.469600>}
box{<0,0,-0.076200><0.383535,0.036000,0.076200> rotate<0,44.997030,0> translate<10.146700,0.000000,69.740800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.417900,0.000000,69.469600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.417900,0.000000,68.656200>}
box{<0,0,-0.076200><0.813400,0.036000,0.076200> rotate<0,-90.000000,0> translate<10.417900,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.417900,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.604400,0.000000,68.656200>}
box{<0,0,-0.076200><0.813500,0.036000,0.076200> rotate<0,0.000000,0> translate<9.604400,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.604400,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.333300,0.000000,68.927300>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<9.333300,0.000000,68.927300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.333300,0.000000,68.927300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.604400,0.000000,69.198500>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-45.007595,0> translate<9.333300,0.000000,68.927300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.604400,0.000000,69.198500>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.417900,0.000000,69.198500>}
box{<0,0,-0.076200><0.813500,0.036000,0.076200> rotate<0,0.000000,0> translate<9.604400,0.000000,69.198500> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.970400,0.000000,70.283100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.241500,0.000000,70.283100>}
box{<0,0,-0.076200><0.271100,0.036000,0.076200> rotate<0,0.000000,0> translate<10.970400,0.000000,70.283100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.241500,0.000000,70.283100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.241500,0.000000,68.656200>}
box{<0,0,-0.076200><1.626900,0.036000,0.076200> rotate<0,-90.000000,0> translate<11.241500,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.970400,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.512700,0.000000,68.656200>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<10.970400,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.061800,0.000000,69.740800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.604100,0.000000,68.656200>}
box{<0,0,-0.076200><1.212620,0.036000,0.076200> rotate<0,63.430762,0> translate<12.061800,0.000000,69.740800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.604100,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.146400,0.000000,69.740800>}
box{<0,0,-0.076200><1.212620,0.036000,0.076200> rotate<0,-63.430762,0> translate<12.604100,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.698900,0.000000,69.740800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.970000,0.000000,69.740800>}
box{<0,0,-0.076200><0.271100,0.036000,0.076200> rotate<0,0.000000,0> translate<13.698900,0.000000,69.740800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.970000,0.000000,69.740800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.970000,0.000000,68.656200>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,-90.000000,0> translate<13.970000,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.698900,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.241200,0.000000,68.656200>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<13.698900,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.970000,0.000000,70.554300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.970000,0.000000,70.283100>}
box{<0,0,-0.076200><0.271200,0.036000,0.076200> rotate<0,-90.000000,0> translate<13.970000,0.000000,70.283100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.790300,0.000000,69.740800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.874900,0.000000,68.656200>}
box{<0,0,-0.076200><1.533856,0.036000,0.076200> rotate<0,44.997030,0> translate<14.790300,0.000000,69.740800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<14.790300,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.874900,0.000000,69.740800>}
box{<0,0,-0.076200><1.533856,0.036000,0.076200> rotate<0,-44.997030,0> translate<14.790300,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<19.149100,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.064500,0.000000,68.656200>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<18.064500,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.064500,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<19.149100,0.000000,69.740800>}
box{<0,0,-0.076200><1.533856,0.036000,0.076200> rotate<0,-44.997030,0> translate<18.064500,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<19.149100,0.000000,69.740800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<19.149100,0.000000,70.012000>}
box{<0,0,-0.076200><0.271200,0.036000,0.076200> rotate<0,90.000000,0> translate<19.149100,0.000000,70.012000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<19.149100,0.000000,70.012000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.877900,0.000000,70.283100>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,44.986466,0> translate<18.877900,0.000000,70.283100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.877900,0.000000,70.283100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.335600,0.000000,70.283100>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<18.335600,0.000000,70.283100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.335600,0.000000,70.283100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.064500,0.000000,70.012000>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,-44.997030,0> translate<18.064500,0.000000,70.012000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<19.701600,0.000000,68.927300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<19.701600,0.000000,70.012000>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,90.000000,0> translate<19.701600,0.000000,70.012000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<19.701600,0.000000,70.012000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<19.972700,0.000000,70.283100>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,-44.997030,0> translate<19.701600,0.000000,70.012000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<19.972700,0.000000,70.283100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<20.515000,0.000000,70.283100>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<19.972700,0.000000,70.283100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<20.515000,0.000000,70.283100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<20.786200,0.000000,70.012000>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,44.986466,0> translate<20.515000,0.000000,70.283100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<20.786200,0.000000,70.012000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<20.786200,0.000000,68.927300>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,-90.000000,0> translate<20.786200,0.000000,68.927300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<20.786200,0.000000,68.927300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<20.515000,0.000000,68.656200>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-44.986466,0> translate<20.515000,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<20.515000,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<19.972700,0.000000,68.656200>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<19.972700,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<19.972700,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<19.701600,0.000000,68.927300>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<19.701600,0.000000,68.927300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<19.701600,0.000000,68.927300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<20.786200,0.000000,70.012000>}
box{<0,0,-0.076200><1.533927,0.036000,0.076200> rotate<0,-44.999671,0> translate<19.701600,0.000000,68.927300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<21.338700,0.000000,69.740800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<21.881000,0.000000,70.283100>}
box{<0,0,-0.076200><0.766928,0.036000,0.076200> rotate<0,-44.997030,0> translate<21.338700,0.000000,69.740800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<21.881000,0.000000,70.283100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<21.881000,0.000000,68.656200>}
box{<0,0,-0.076200><1.626900,0.036000,0.076200> rotate<0,-90.000000,0> translate<21.881000,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<21.338700,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.423300,0.000000,68.656200>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<21.338700,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.975800,0.000000,70.012000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.246900,0.000000,70.283100>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,-44.997030,0> translate<22.975800,0.000000,70.012000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.246900,0.000000,70.283100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.789200,0.000000,70.283100>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<23.246900,0.000000,70.283100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.789200,0.000000,70.283100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.060400,0.000000,70.012000>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,44.986466,0> translate<23.789200,0.000000,70.283100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.060400,0.000000,70.012000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.060400,0.000000,69.740800>}
box{<0,0,-0.076200><0.271200,0.036000,0.076200> rotate<0,-90.000000,0> translate<24.060400,0.000000,69.740800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.060400,0.000000,69.740800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.789200,0.000000,69.469600>}
box{<0,0,-0.076200><0.383535,0.036000,0.076200> rotate<0,-44.997030,0> translate<23.789200,0.000000,69.469600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.789200,0.000000,69.469600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.518100,0.000000,69.469600>}
box{<0,0,-0.076200><0.271100,0.036000,0.076200> rotate<0,0.000000,0> translate<23.518100,0.000000,69.469600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.789200,0.000000,69.469600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.060400,0.000000,69.198500>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,44.986466,0> translate<23.789200,0.000000,69.469600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.060400,0.000000,69.198500>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.060400,0.000000,68.927300>}
box{<0,0,-0.076200><0.271200,0.036000,0.076200> rotate<0,-90.000000,0> translate<24.060400,0.000000,68.927300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.060400,0.000000,68.927300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.789200,0.000000,68.656200>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-44.986466,0> translate<23.789200,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.789200,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.246900,0.000000,68.656200>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<23.246900,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.246900,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.975800,0.000000,68.927300>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<22.975800,0.000000,68.927300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.612900,0.000000,69.469600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<25.697500,0.000000,69.469600>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<24.612900,0.000000,69.469600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<26.250000,0.000000,68.927300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<26.250000,0.000000,70.012000>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,90.000000,0> translate<26.250000,0.000000,70.012000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<26.250000,0.000000,70.012000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<26.521100,0.000000,70.283100>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,-44.997030,0> translate<26.250000,0.000000,70.012000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<26.521100,0.000000,70.283100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<27.063400,0.000000,70.283100>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<26.521100,0.000000,70.283100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<27.063400,0.000000,70.283100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<27.334600,0.000000,70.012000>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,44.986466,0> translate<27.063400,0.000000,70.283100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<27.334600,0.000000,70.012000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<27.334600,0.000000,68.927300>}
box{<0,0,-0.076200><1.084700,0.036000,0.076200> rotate<0,-90.000000,0> translate<27.334600,0.000000,68.927300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<27.334600,0.000000,68.927300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<27.063400,0.000000,68.656200>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-44.986466,0> translate<27.063400,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<27.063400,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<26.521100,0.000000,68.656200>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<26.521100,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<26.521100,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<26.250000,0.000000,68.927300>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<26.250000,0.000000,68.927300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<26.250000,0.000000,68.927300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<27.334600,0.000000,70.012000>}
box{<0,0,-0.076200><1.533927,0.036000,0.076200> rotate<0,-44.999671,0> translate<26.250000,0.000000,68.927300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<27.887100,0.000000,69.740800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<28.429400,0.000000,70.283100>}
box{<0,0,-0.076200><0.766928,0.036000,0.076200> rotate<0,-44.997030,0> translate<27.887100,0.000000,69.740800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<28.429400,0.000000,70.283100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<28.429400,0.000000,68.656200>}
box{<0,0,-0.076200><1.626900,0.036000,0.076200> rotate<0,-90.000000,0> translate<28.429400,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<27.887100,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<28.971700,0.000000,68.656200>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<27.887100,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<29.524200,0.000000,69.469600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<30.608800,0.000000,69.469600>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<29.524200,0.000000,69.469600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<31.161300,0.000000,69.740800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<31.703600,0.000000,70.283100>}
box{<0,0,-0.076200><0.766928,0.036000,0.076200> rotate<0,-44.997030,0> translate<31.161300,0.000000,69.740800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<31.703600,0.000000,70.283100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<31.703600,0.000000,68.656200>}
box{<0,0,-0.076200><1.626900,0.036000,0.076200> rotate<0,-90.000000,0> translate<31.703600,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<31.161300,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<32.245900,0.000000,68.656200>}
box{<0,0,-0.076200><1.084600,0.036000,0.076200> rotate<0,0.000000,0> translate<31.161300,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<32.798400,0.000000,70.012000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.069500,0.000000,70.283100>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,-44.997030,0> translate<32.798400,0.000000,70.012000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.069500,0.000000,70.283100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.611800,0.000000,70.283100>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<33.069500,0.000000,70.283100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.611800,0.000000,70.283100>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.883000,0.000000,70.012000>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,44.986466,0> translate<33.611800,0.000000,70.283100> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.883000,0.000000,70.012000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.883000,0.000000,69.740800>}
box{<0,0,-0.076200><0.271200,0.036000,0.076200> rotate<0,-90.000000,0> translate<33.883000,0.000000,69.740800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.883000,0.000000,69.740800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.611800,0.000000,69.469600>}
box{<0,0,-0.076200><0.383535,0.036000,0.076200> rotate<0,-44.997030,0> translate<33.611800,0.000000,69.469600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.611800,0.000000,69.469600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.340700,0.000000,69.469600>}
box{<0,0,-0.076200><0.271100,0.036000,0.076200> rotate<0,0.000000,0> translate<33.340700,0.000000,69.469600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.611800,0.000000,69.469600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.883000,0.000000,69.198500>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,44.986466,0> translate<33.611800,0.000000,69.469600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.883000,0.000000,69.198500>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.883000,0.000000,68.927300>}
box{<0,0,-0.076200><0.271200,0.036000,0.076200> rotate<0,-90.000000,0> translate<33.883000,0.000000,68.927300> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.883000,0.000000,68.927300>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.611800,0.000000,68.656200>}
box{<0,0,-0.076200><0.383464,0.036000,0.076200> rotate<0,-44.986466,0> translate<33.611800,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.611800,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.069500,0.000000,68.656200>}
box{<0,0,-0.076200><0.542300,0.036000,0.076200> rotate<0,0.000000,0> translate<33.069500,0.000000,68.656200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.069500,0.000000,68.656200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<32.798400,0.000000,68.927300>}
box{<0,0,-0.076200><0.383393,0.036000,0.076200> rotate<0,44.997030,0> translate<32.798400,0.000000,68.927300> }
//CONN silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<15.875000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<14.605000,0.000000,3.810000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<14.605000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<14.605000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<13.970000,0.000000,4.445000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<13.970000,0.000000,4.445000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<13.970000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<14.605000,0.000000,6.350000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<13.970000,0.000000,5.715000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.050000,0.000000,4.445000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<18.415000,0.000000,3.810000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<18.415000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<18.415000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.145000,0.000000,3.810000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<17.145000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.145000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<16.510000,0.000000,4.445000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<16.510000,0.000000,4.445000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<16.510000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.145000,0.000000,6.350000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<16.510000,0.000000,5.715000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.145000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<18.415000,0.000000,6.350000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<17.145000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<18.415000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.050000,0.000000,5.715000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<18.415000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<15.875000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<16.510000,0.000000,4.445000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<15.875000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<16.510000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<15.875000,0.000000,6.350000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<15.875000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<14.605000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<15.875000,0.000000,6.350000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<14.605000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<23.495000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<22.225000,0.000000,3.810000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<22.225000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<22.225000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.590000,0.000000,4.445000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<21.590000,0.000000,4.445000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.590000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<22.225000,0.000000,6.350000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<21.590000,0.000000,5.715000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.590000,0.000000,4.445000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.955000,0.000000,3.810000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<20.955000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.955000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.685000,0.000000,3.810000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<19.685000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.685000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.050000,0.000000,4.445000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<19.050000,0.000000,4.445000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.050000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.685000,0.000000,6.350000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<19.050000,0.000000,5.715000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.685000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.955000,0.000000,6.350000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<19.685000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.955000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.590000,0.000000,5.715000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<20.955000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.670000,0.000000,4.445000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.035000,0.000000,3.810000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<26.035000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.035000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<24.765000,0.000000,3.810000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<24.765000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<24.765000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<24.130000,0.000000,4.445000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<24.130000,0.000000,4.445000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<24.130000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<24.765000,0.000000,6.350000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<24.130000,0.000000,5.715000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<24.765000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.035000,0.000000,6.350000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<24.765000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.035000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.670000,0.000000,5.715000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<26.035000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<23.495000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<24.130000,0.000000,4.445000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<23.495000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<24.130000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<23.495000,0.000000,6.350000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<23.495000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<22.225000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<23.495000,0.000000,6.350000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<22.225000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<31.115000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.845000,0.000000,3.810000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<29.845000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.845000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.210000,0.000000,4.445000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<29.210000,0.000000,4.445000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.210000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.845000,0.000000,6.350000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<29.210000,0.000000,5.715000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.210000,0.000000,4.445000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<28.575000,0.000000,3.810000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<28.575000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<28.575000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<27.305000,0.000000,3.810000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<27.305000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<27.305000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.670000,0.000000,4.445000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<26.670000,0.000000,4.445000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.670000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<27.305000,0.000000,6.350000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<26.670000,0.000000,5.715000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<27.305000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<28.575000,0.000000,6.350000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<27.305000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<28.575000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.210000,0.000000,5.715000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<28.575000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<31.750000,0.000000,4.445000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<31.750000,0.000000,5.715000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,90.000000,0> translate<31.750000,0.000000,5.715000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<31.115000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<31.750000,0.000000,4.445000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<31.115000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<31.750000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<31.115000,0.000000,6.350000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<31.115000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.845000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<31.115000,0.000000,6.350000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<29.845000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<13.335000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<12.065000,0.000000,3.810000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<12.065000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<12.065000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<11.430000,0.000000,4.445000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<11.430000,0.000000,4.445000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<11.430000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<12.065000,0.000000,6.350000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<11.430000,0.000000,5.715000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<13.335000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<13.970000,0.000000,4.445000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<13.335000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<13.970000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<13.335000,0.000000,6.350000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<13.335000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<12.065000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<13.335000,0.000000,6.350000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<12.065000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.795000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.525000,0.000000,3.810000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<9.525000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.525000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.890000,0.000000,4.445000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<8.890000,0.000000,4.445000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.890000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.525000,0.000000,6.350000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<8.890000,0.000000,5.715000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.795000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<11.430000,0.000000,4.445000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<10.795000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<11.430000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.795000,0.000000,6.350000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<10.795000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.525000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.795000,0.000000,6.350000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<9.525000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.255000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<6.985000,0.000000,3.810000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<6.985000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<6.985000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<6.350000,0.000000,4.445000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<6.350000,0.000000,4.445000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<6.350000,0.000000,4.445000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<6.350000,0.000000,5.715000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,90.000000,0> translate<6.350000,0.000000,5.715000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<6.350000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<6.985000,0.000000,6.350000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<6.350000,0.000000,5.715000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.255000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.890000,0.000000,4.445000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,-44.997030,0> translate<8.255000,0.000000,3.810000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.890000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.255000,0.000000,6.350000>}
box{<0,0,-0.101600><0.898026,0.036000,0.101600> rotate<0,44.997030,0> translate<8.255000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<6.985000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.255000,0.000000,6.350000>}
box{<0,0,-0.101600><1.270000,0.036000,0.101600> rotate<0,0.000000,0> translate<6.985000,0.000000,6.350000> }
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-180.000000,0> translate<15.240000,0.000000,5.080000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-180.000000,0> translate<17.780000,0.000000,5.080000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-180.000000,0> translate<20.320000,0.000000,5.080000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-180.000000,0> translate<22.860000,0.000000,5.080000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-180.000000,0> translate<25.400000,0.000000,5.080000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-180.000000,0> translate<27.940000,0.000000,5.080000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-180.000000,0> translate<30.480000,0.000000,5.080000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-180.000000,0> translate<12.700000,0.000000,5.080000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-180.000000,0> translate<10.160000,0.000000,5.080000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-180.000000,0> translate<7.620000,0.000000,5.080000>}
//DIS1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<21.463000,0.000000,29.591000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<21.463000,0.000000,46.609000>}
box{<0,0,-0.076200><17.018000,0.036000,0.076200> rotate<0,90.000000,0> translate<21.463000,0.000000,46.609000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.017000,0.000000,46.609000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<21.463000,0.000000,46.609000>}
box{<0,0,-0.076200><12.446000,0.036000,0.076200> rotate<0,0.000000,0> translate<9.017000,0.000000,46.609000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.017000,0.000000,46.609000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.017000,0.000000,29.591000>}
box{<0,0,-0.076200><17.018000,0.036000,0.076200> rotate<0,-90.000000,0> translate<9.017000,0.000000,29.591000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<21.463000,0.000000,29.591000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.017000,0.000000,29.591000>}
box{<0,0,-0.076200><12.446000,0.036000,0.076200> rotate<0,0.000000,0> translate<9.017000,0.000000,29.591000> }
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<19.710400,0.000000,31.750000>}
cylinder{<0,0,0><0,0.036000,0>0.152400 translate<19.888200,0.000000,31.750000>}
box{<0,0,-0.152400><0.177800,0.036000,0.152400> rotate<0,0.000000,0> translate<19.710400,0.000000,31.750000> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<19.989800,0.000000,44.373800>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<19.304000,0.000000,43.688000>}
box{<0,0,-0.203200><0.969868,0.036000,0.203200> rotate<0,-44.997030,0> translate<19.304000,0.000000,43.688000> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<19.304000,0.000000,43.688000>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<18.465800,0.000000,38.887400>}
box{<0,0,-0.203200><4.873227,0.036000,0.203200> rotate<0,-80.090531,0> translate<18.465800,0.000000,38.887400> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<18.465800,0.000000,38.887400>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<18.948400,0.000000,38.404800>}
box{<0,0,-0.203200><0.682499,0.036000,0.203200> rotate<0,44.997030,0> translate<18.465800,0.000000,38.887400> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<18.948400,0.000000,38.404800>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<19.558000,0.000000,39.014400>}
box{<0,0,-0.203200><0.862105,0.036000,0.203200> rotate<0,-44.997030,0> translate<18.948400,0.000000,38.404800> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<19.558000,0.000000,39.014400>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<20.396200,0.000000,43.967400>}
box{<0,0,-0.203200><5.023424,0.036000,0.203200> rotate<0,-80.389490,0> translate<19.558000,0.000000,39.014400> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<20.396200,0.000000,43.967400>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<19.989800,0.000000,44.373800>}
box{<0,0,-0.203200><0.574736,0.036000,0.203200> rotate<0,44.997030,0> translate<19.989800,0.000000,44.373800> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<19.532600,0.000000,44.831000>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<18.897600,0.000000,44.196000>}
box{<0,0,-0.203200><0.898026,0.036000,0.203200> rotate<0,-44.997030,0> translate<18.897600,0.000000,44.196000> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<18.897600,0.000000,44.196000>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<13.614400,0.000000,44.196000>}
box{<0,0,-0.203200><5.283200,0.036000,0.203200> rotate<0,0.000000,0> translate<13.614400,0.000000,44.196000> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<13.614400,0.000000,44.196000>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<12.928600,0.000000,44.881800>}
box{<0,0,-0.203200><0.969868,0.036000,0.203200> rotate<0,44.997030,0> translate<12.928600,0.000000,44.881800> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<12.928600,0.000000,44.881800>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<13.258800,0.000000,45.212000>}
box{<0,0,-0.203200><0.466973,0.036000,0.203200> rotate<0,-44.997030,0> translate<12.928600,0.000000,44.881800> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<13.258800,0.000000,45.212000>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<19.151600,0.000000,45.212000>}
box{<0,0,-0.203200><5.892800,0.036000,0.203200> rotate<0,0.000000,0> translate<13.258800,0.000000,45.212000> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<19.151600,0.000000,45.212000>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<19.532600,0.000000,44.831000>}
box{<0,0,-0.203200><0.538815,0.036000,0.203200> rotate<0,44.997030,0> translate<19.151600,0.000000,45.212000> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<12.471400,0.000000,44.424600>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<13.106400,0.000000,43.789600>}
box{<0,0,-0.203200><0.898026,0.036000,0.203200> rotate<0,44.997030,0> translate<12.471400,0.000000,44.424600> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<13.106400,0.000000,43.789600>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<12.268200,0.000000,38.912800>}
box{<0,0,-0.203200><4.948309,0.036000,0.203200> rotate<0,-80.242279,0> translate<12.268200,0.000000,38.912800> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<12.268200,0.000000,38.912800>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<11.684000,0.000000,38.328600>}
box{<0,0,-0.203200><0.826184,0.036000,0.203200> rotate<0,-44.997030,0> translate<11.684000,0.000000,38.328600> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<11.684000,0.000000,38.328600>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<11.201400,0.000000,38.811200>}
box{<0,0,-0.203200><0.682499,0.036000,0.203200> rotate<0,44.997030,0> translate<11.201400,0.000000,38.811200> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<11.201400,0.000000,38.811200>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<12.115800,0.000000,44.069000>}
box{<0,0,-0.203200><5.336721,0.036000,0.203200> rotate<0,-80.128905,0> translate<11.201400,0.000000,38.811200> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<12.115800,0.000000,44.069000>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<12.471400,0.000000,44.424600>}
box{<0,0,-0.203200><0.502894,0.036000,0.203200> rotate<0,-44.997030,0> translate<12.115800,0.000000,44.069000> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<12.166600,0.000000,37.846000>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<12.801600,0.000000,38.481000>}
box{<0,0,-0.203200><0.898026,0.036000,0.203200> rotate<0,-44.997030,0> translate<12.166600,0.000000,37.846000> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<12.801600,0.000000,38.481000>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<17.932400,0.000000,38.481000>}
box{<0,0,-0.203200><5.130800,0.036000,0.203200> rotate<0,0.000000,0> translate<12.801600,0.000000,38.481000> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<17.932400,0.000000,38.481000>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<18.440400,0.000000,37.973000>}
box{<0,0,-0.203200><0.718420,0.036000,0.203200> rotate<0,44.997030,0> translate<17.932400,0.000000,38.481000> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<18.440400,0.000000,37.973000>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<17.932400,0.000000,37.465000>}
box{<0,0,-0.203200><0.718420,0.036000,0.203200> rotate<0,-44.997030,0> translate<17.932400,0.000000,37.465000> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<17.932400,0.000000,37.465000>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<12.547600,0.000000,37.465000>}
box{<0,0,-0.203200><5.384800,0.036000,0.203200> rotate<0,0.000000,0> translate<12.547600,0.000000,37.465000> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<12.547600,0.000000,37.465000>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<12.166600,0.000000,37.846000>}
box{<0,0,-0.203200><0.538815,0.036000,0.203200> rotate<0,44.997030,0> translate<12.166600,0.000000,37.846000> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<11.684000,0.000000,37.363400>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<11.938000,0.000000,37.109400>}
box{<0,0,-0.203200><0.359210,0.036000,0.203200> rotate<0,44.997030,0> translate<11.684000,0.000000,37.363400> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<10.490200,0.000000,31.546800>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<11.074400,0.000000,32.131000>}
box{<0,0,-0.203200><0.826184,0.036000,0.203200> rotate<0,-44.997030,0> translate<10.490200,0.000000,31.546800> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<11.074400,0.000000,32.131000>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<11.938000,0.000000,37.109400>}
box{<0,0,-0.203200><5.052749,0.036000,0.203200> rotate<0,-80.153578,0> translate<11.074400,0.000000,32.131000> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<11.684000,0.000000,37.363400>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<10.795000,0.000000,36.474400>}
box{<0,0,-0.203200><1.257236,0.036000,0.203200> rotate<0,-44.997030,0> translate<10.795000,0.000000,36.474400> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<10.795000,0.000000,36.474400>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<10.007600,0.000000,32.029400>}
box{<0,0,-0.203200><4.514202,0.036000,0.203200> rotate<0,-79.949393,0> translate<10.007600,0.000000,32.029400> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<10.007600,0.000000,32.029400>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<10.490200,0.000000,31.546800>}
box{<0,0,-0.203200><0.682499,0.036000,0.203200> rotate<0,44.997030,0> translate<10.007600,0.000000,32.029400> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<10.972800,0.000000,31.064200>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<11.658600,0.000000,31.750000>}
box{<0,0,-0.203200><0.969868,0.036000,0.203200> rotate<0,-44.997030,0> translate<10.972800,0.000000,31.064200> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<11.658600,0.000000,31.750000>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<16.916400,0.000000,31.750000>}
box{<0,0,-0.203200><5.257800,0.036000,0.203200> rotate<0,0.000000,0> translate<11.658600,0.000000,31.750000> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<16.916400,0.000000,31.750000>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<17.602200,0.000000,31.064200>}
box{<0,0,-0.203200><0.969868,0.036000,0.203200> rotate<0,44.997030,0> translate<16.916400,0.000000,31.750000> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<17.602200,0.000000,31.064200>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<17.272000,0.000000,30.734000>}
box{<0,0,-0.203200><0.466973,0.036000,0.203200> rotate<0,-44.997030,0> translate<17.272000,0.000000,30.734000> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<17.272000,0.000000,30.734000>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<11.303000,0.000000,30.734000>}
box{<0,0,-0.203200><5.969000,0.036000,0.203200> rotate<0,0.000000,0> translate<11.303000,0.000000,30.734000> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<11.303000,0.000000,30.734000>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<10.972800,0.000000,31.064200>}
box{<0,0,-0.203200><0.466973,0.036000,0.203200> rotate<0,44.997030,0> translate<10.972800,0.000000,31.064200> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<18.923000,0.000000,37.515800>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<18.084800,0.000000,36.677600>}
box{<0,0,-0.203200><1.185394,0.036000,0.203200> rotate<0,-44.997030,0> translate<18.084800,0.000000,36.677600> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<18.084800,0.000000,36.677600>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<17.322800,0.000000,32.283400>}
box{<0,0,-0.203200><4.459780,0.036000,0.203200> rotate<0,-80.156859,0> translate<17.322800,0.000000,32.283400> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<17.322800,0.000000,32.283400>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<18.084800,0.000000,31.521400>}
box{<0,0,-0.203200><1.077631,0.036000,0.203200> rotate<0,44.997030,0> translate<17.322800,0.000000,32.283400> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<18.084800,0.000000,31.521400>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<18.288000,0.000000,31.724600>}
box{<0,0,-0.203200><0.287368,0.036000,0.203200> rotate<0,-44.997030,0> translate<18.084800,0.000000,31.521400> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<18.288000,0.000000,31.724600>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<19.202400,0.000000,37.236400>}
box{<0,0,-0.203200><5.587134,0.036000,0.203200> rotate<0,-80.575182,0> translate<18.288000,0.000000,31.724600> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<19.202400,0.000000,37.236400>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<18.923000,0.000000,37.515800>}
box{<0,0,-0.203200><0.395131,0.036000,0.203200> rotate<0,44.997030,0> translate<18.923000,0.000000,37.515800> }
cylinder{<0,0,0><0,0.036000,0>0.406400 translate<18.719800,0.000000,36.931600>}
cylinder{<0,0,0><0,0.036000,0>0.406400 translate<17.881600,0.000000,32.105600>}
box{<0,0,-0.406400><4.898250,0.036000,0.406400> rotate<0,-80.141630,0> translate<17.881600,0.000000,32.105600> }
cylinder{<0,0,0><0,0.036000,0>0.406400 translate<17.018000,0.000000,31.242000>}
cylinder{<0,0,0><0,0.036000,0>0.406400 translate<11.557000,0.000000,31.242000>}
box{<0,0,-0.406400><5.461000,0.036000,0.406400> rotate<0,0.000000,0> translate<11.557000,0.000000,31.242000> }
cylinder{<0,0,0><0,0.036000,0>0.406400 translate<10.591800,0.000000,32.156400>}
cylinder{<0,0,0><0,0.036000,0>0.406400 translate<11.455400,0.000000,36.804600>}
box{<0,0,-0.406400><4.727745,0.036000,0.406400> rotate<0,-79.469647,0> translate<10.591800,0.000000,32.156400> }
cylinder{<0,0,0><0,0.036000,0>0.406400 translate<11.709400,0.000000,38.938200>}
cylinder{<0,0,0><0,0.036000,0>0.406400 translate<12.573000,0.000000,43.815000>}
box{<0,0,-0.406400><4.952674,0.036000,0.406400> rotate<0,-79.952699,0> translate<11.709400,0.000000,38.938200> }
cylinder{<0,0,0><0,0.036000,0>0.406400 translate<13.487400,0.000000,44.704000>}
cylinder{<0,0,0><0,0.036000,0>0.406400 translate<19.050000,0.000000,44.704000>}
box{<0,0,-0.406400><5.562600,0.036000,0.406400> rotate<0,0.000000,0> translate<13.487400,0.000000,44.704000> }
cylinder{<0,0,0><0,0.036000,0>0.406400 translate<19.812000,0.000000,43.865800>}
cylinder{<0,0,0><0,0.036000,0>0.406400 translate<18.999200,0.000000,39.014400>}
box{<0,0,-0.406400><4.919017,0.036000,0.406400> rotate<0,-80.483728,0> translate<18.999200,0.000000,39.014400> }
cylinder{<0,0,0><0,0.036000,0>0.406400 translate<17.907000,0.000000,37.973000>}
cylinder{<0,0,0><0,0.036000,0>0.406400 translate<12.725400,0.000000,37.973000>}
box{<0,0,-0.406400><5.181600,0.036000,0.406400> rotate<0,0.000000,0> translate<12.725400,0.000000,37.973000> }
difference{
cylinder{<19.812000,0,31.750000><19.812000,0.036000,31.750000>0.762000 translate<0,0.000000,0>}
cylinder{<19.812000,-0.1,31.750000><19.812000,0.135000,31.750000>0.000000 translate<0,0.000000,0>}}
//R_A silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<18.542000,0.000000,63.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<18.542000,0.000000,58.420000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,-90.000000,0> translate<18.542000,0.000000,58.420000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<18.542000,0.000000,58.420000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.780000,0.000000,58.420000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<17.780000,0.000000,58.420000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.780000,0.000000,58.420000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.018000,0.000000,58.420000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<17.018000,0.000000,58.420000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.018000,0.000000,58.420000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.018000,0.000000,63.500000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,90.000000,0> translate<17.018000,0.000000,63.500000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.018000,0.000000,63.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.780000,0.000000,63.500000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<17.018000,0.000000,63.500000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.780000,0.000000,63.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<18.542000,0.000000,63.500000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<17.780000,0.000000,63.500000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.780000,0.000000,58.420000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.780000,0.000000,58.166000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,-90.000000,0> translate<17.780000,0.000000,58.166000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.780000,0.000000,63.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.780000,0.000000,63.754000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,90.000000,0> translate<17.780000,0.000000,63.754000> }
//R_B silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.082000,0.000000,63.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.082000,0.000000,58.420000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,-90.000000,0> translate<21.082000,0.000000,58.420000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.082000,0.000000,58.420000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,58.420000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<20.320000,0.000000,58.420000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,58.420000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.558000,0.000000,58.420000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<19.558000,0.000000,58.420000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.558000,0.000000,58.420000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.558000,0.000000,63.500000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,90.000000,0> translate<19.558000,0.000000,63.500000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.558000,0.000000,63.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,63.500000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<19.558000,0.000000,63.500000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,63.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.082000,0.000000,63.500000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<20.320000,0.000000,63.500000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,58.420000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,58.166000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,-90.000000,0> translate<20.320000,0.000000,58.166000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,63.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,63.754000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,90.000000,0> translate<20.320000,0.000000,63.754000> }
//R_C silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.018000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.018000,0.000000,20.320000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,90.000000,0> translate<17.018000,0.000000,20.320000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.018000,0.000000,20.320000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.780000,0.000000,20.320000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<17.018000,0.000000,20.320000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.780000,0.000000,20.320000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<18.542000,0.000000,20.320000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<17.780000,0.000000,20.320000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<18.542000,0.000000,20.320000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<18.542000,0.000000,15.240000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,-90.000000,0> translate<18.542000,0.000000,15.240000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<18.542000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.780000,0.000000,15.240000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<17.780000,0.000000,15.240000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.780000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.018000,0.000000,15.240000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<17.018000,0.000000,15.240000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.780000,0.000000,20.320000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.780000,0.000000,20.574000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,90.000000,0> translate<17.780000,0.000000,20.574000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.780000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.780000,0.000000,14.986000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,-90.000000,0> translate<17.780000,0.000000,14.986000> }
//R_D silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<11.938000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<11.938000,0.000000,20.320000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,90.000000,0> translate<11.938000,0.000000,20.320000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<11.938000,0.000000,20.320000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<12.700000,0.000000,20.320000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<11.938000,0.000000,20.320000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<12.700000,0.000000,20.320000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<13.462000,0.000000,20.320000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<12.700000,0.000000,20.320000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<13.462000,0.000000,20.320000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<13.462000,0.000000,15.240000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,-90.000000,0> translate<13.462000,0.000000,15.240000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<13.462000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<12.700000,0.000000,15.240000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<12.700000,0.000000,15.240000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<12.700000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<11.938000,0.000000,15.240000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<11.938000,0.000000,15.240000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<12.700000,0.000000,20.320000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<12.700000,0.000000,20.574000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,90.000000,0> translate<12.700000,0.000000,20.574000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<12.700000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<12.700000,0.000000,14.986000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,-90.000000,0> translate<12.700000,0.000000,14.986000> }
//R_DOT silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.558000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.558000,0.000000,20.320000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,90.000000,0> translate<19.558000,0.000000,20.320000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.558000,0.000000,20.320000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,20.320000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<19.558000,0.000000,20.320000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,20.320000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.082000,0.000000,20.320000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<20.320000,0.000000,20.320000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.082000,0.000000,20.320000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.082000,0.000000,15.240000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,-90.000000,0> translate<21.082000,0.000000,15.240000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.082000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,15.240000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<20.320000,0.000000,15.240000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.558000,0.000000,15.240000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<19.558000,0.000000,15.240000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,20.320000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,20.574000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,90.000000,0> translate<20.320000,0.000000,20.574000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.320000,0.000000,14.986000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,-90.000000,0> translate<20.320000,0.000000,14.986000> }
//R_E silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.398000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.398000,0.000000,20.320000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,90.000000,0> translate<9.398000,0.000000,20.320000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.398000,0.000000,20.320000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,20.320000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<9.398000,0.000000,20.320000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,20.320000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.922000,0.000000,20.320000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<10.160000,0.000000,20.320000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.922000,0.000000,20.320000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.922000,0.000000,15.240000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,-90.000000,0> translate<10.922000,0.000000,15.240000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.922000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,15.240000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<10.160000,0.000000,15.240000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.398000,0.000000,15.240000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<9.398000,0.000000,15.240000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,20.320000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,20.574000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,90.000000,0> translate<10.160000,0.000000,20.574000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,14.986000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,-90.000000,0> translate<10.160000,0.000000,14.986000> }
//R_F silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<13.462000,0.000000,63.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<13.462000,0.000000,58.420000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,-90.000000,0> translate<13.462000,0.000000,58.420000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<13.462000,0.000000,58.420000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<12.700000,0.000000,58.420000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<12.700000,0.000000,58.420000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<12.700000,0.000000,58.420000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<11.938000,0.000000,58.420000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<11.938000,0.000000,58.420000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<11.938000,0.000000,58.420000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<11.938000,0.000000,63.500000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,90.000000,0> translate<11.938000,0.000000,63.500000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<11.938000,0.000000,63.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<12.700000,0.000000,63.500000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<11.938000,0.000000,63.500000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<12.700000,0.000000,63.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<13.462000,0.000000,63.500000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<12.700000,0.000000,63.500000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<12.700000,0.000000,58.420000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<12.700000,0.000000,58.166000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,-90.000000,0> translate<12.700000,0.000000,58.166000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<12.700000,0.000000,63.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<12.700000,0.000000,63.754000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,90.000000,0> translate<12.700000,0.000000,63.754000> }
//R_G silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.922000,0.000000,63.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.922000,0.000000,58.420000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,-90.000000,0> translate<10.922000,0.000000,58.420000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.922000,0.000000,58.420000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,58.420000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<10.160000,0.000000,58.420000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,58.420000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.398000,0.000000,58.420000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<9.398000,0.000000,58.420000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.398000,0.000000,58.420000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.398000,0.000000,63.500000>}
box{<0,0,-0.101600><5.080000,0.036000,0.101600> rotate<0,90.000000,0> translate<9.398000,0.000000,63.500000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.398000,0.000000,63.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,63.500000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<9.398000,0.000000,63.500000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,63.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.922000,0.000000,63.500000>}
box{<0,0,-0.101600><0.762000,0.036000,0.101600> rotate<0,0.000000,0> translate<10.160000,0.000000,63.500000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,58.420000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,58.166000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,-90.000000,0> translate<10.160000,0.000000,58.166000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,63.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<10.160000,0.000000,63.754000>}
box{<0,0,-0.101600><0.254000,0.036000,0.101600> rotate<0,90.000000,0> translate<10.160000,0.000000,63.754000> }
texture{col_slk}
}
#end
translate<mac_x_ver,mac_y_ver,mac_z_ver>
rotate<mac_x_rot,mac_y_rot,mac_z_rot>
}//End union
#end

#if(use_file_as_inc=off)
object{  Z_SEG_V1(-17.780000,0,-36.830000,pcb_rotate_x,pcb_rotate_y,pcb_rotate_z)
#if(pcb_upsidedown=on)
rotate pcb_rotdir*180
#end
}
#end


//Parts not found in 3dpack.dat or 3dusrpac.dat are:
//DIS1	HD-H101	HDSP-M
