//POVRay-File creato da 3d41.ulp v20110101
///home/salvix/Dev/vcc_dev/TinyAVRRandom/tiny13-test.brd
//11/6/12 11:23 PM

#version 3.5;

//Set to on if the file should be used as .inc
#local use_file_as_inc = off;
#if(use_file_as_inc=off)


//changes the apperance of resistors (1 Blob / 0 real)
#declare global_res_shape = 1;
//randomize color of resistors 1=random 0=same color
#declare global_res_colselect = 0;
//Number of the color for the resistors
//0=Green, 1="normal color" 2=Blue 3=Brown
#declare global_res_col = 1;
//Set to on if you want to render the PCB upside-down
#declare pcb_upsidedown = off;
//Set to x or z to rotate around the corresponding axis (referring to pcb_upsidedown)
#declare pcb_rotdir = x;
//Set the length off short pins over the PCB
#declare pin_length = 2.5;
#declare global_diode_bend_radius = 1;
#declare global_res_bend_radius = 1;
#declare global_solder = on;

#declare global_show_screws = on;
#declare global_show_washers = on;
#declare global_show_nuts = on;

#declare global_use_radiosity = on;

#declare global_ambient_mul = 1;
#declare global_ambient_mul_emit = 0;

//Animation
#declare global_anim = off;
#local global_anim_showcampath = no;

#declare global_fast_mode = off;

#declare col_preset = 2;
#declare pin_short = on;

#declare e3d_environment = off;

#local cam_x = 0;
#local cam_y = 185;
#local cam_z = -99;
#local cam_a = 20;
#local cam_look_x = 0;
#local cam_look_y = -4;
#local cam_look_z = 0;

#local pcb_rotate_x = 0;
#local pcb_rotate_y = 0;
#local pcb_rotate_z = 0;

#local pcb_board = on;
#local pcb_parts = on;
#local pcb_wire_bridges = off;
#if(global_fast_mode=off)
	#local pcb_polygons = on;
	#local pcb_silkscreen = on;
	#local pcb_wires = on;
	#local pcb_pads_smds = on;
#else
	#local pcb_polygons = off;
	#local pcb_silkscreen = off;
	#local pcb_wires = off;
	#local pcb_pads_smds = off;
#end

#local lgt1_pos_x = 21;
#local lgt1_pos_y = 31;
#local lgt1_pos_z = 22;
#local lgt1_intense = 0.734470;
#local lgt2_pos_x = -21;
#local lgt2_pos_y = 31;
#local lgt2_pos_z = 22;
#local lgt2_intense = 0.734470;
#local lgt3_pos_x = 21;
#local lgt3_pos_y = 31;
#local lgt3_pos_z = -15;
#local lgt3_intense = 0.734470;
#local lgt4_pos_x = -21;
#local lgt4_pos_y = 31;
#local lgt4_pos_z = -15;
#local lgt4_intense = 0.734470;

//Do not change these values
#declare pcb_height = 1.500000;
#declare pcb_cuheight = 0.035000;
#declare pcb_x_size = 55.880000;
#declare pcb_y_size = 43.180000;
#declare pcb_layer1_used = 0;
#declare pcb_layer16_used = 1;
#declare inc_testmode = off;
#declare global_seed=seed(922);
#declare global_pcb_layer_dis = array[16]
{
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	1.535000,
}
#declare global_pcb_real_hole = 2.000000;

#include "e3d_tools.inc"
#include "e3d_user.inc"

global_settings{charset utf8}

#if(e3d_environment=on)
sky_sphere {pigment {Navy}
pigment {bozo turbulence 0.65 octaves 7 omega 0.7 lambda 2
color_map {
[0.0 0.1 color rgb <0.85, 0.85, 0.85> color rgb <0.75, 0.75, 0.75>]
[0.1 0.5 color rgb <0.75, 0.75, 0.75> color rgbt <1, 1, 1, 1>]
[0.5 1.0 color rgbt <1, 1, 1, 1> color rgbt <1, 1, 1, 1>]}
scale <0.1, 0.5, 0.1>} rotate -90*x}
plane{y, -10.0-max(pcb_x_size,pcb_y_size)*abs(max(sin((pcb_rotate_x/180)*pi),sin((pcb_rotate_z/180)*pi)))
texture{T_Chrome_2D
normal{waves 0.1 frequency 3000.0 scale 3000.0}} translate<0,0,0>}
#end

//Animation data
#if(global_anim=on)
#declare global_anim_showcampath = no;
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#declare global_anim_npoints_cam_flight=0;
#warning "No/not enough Animation Data available (min. 3 points) (Flight path)"
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#declare global_anim_npoints_cam_view=0;
#warning "No/not enough Animation Data available (min. 3 points) (View path)"
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#end

#if((global_anim_showcampath=yes)&(global_anim=off))
#end
#if(global_anim=on)
camera
{
	location global_anim_spline_cam_flight(clock)
	#if(global_anim_npoints_cam_view>2)
		look_at global_anim_spline_cam_view(clock)
	#else
		look_at global_anim_spline_cam_flight(clock+0.01)-<0,-0.01,0>
	#end
	angle 45
}
light_source
{
	global_anim_spline_cam_flight(clock)
	color rgb <1,1,1>
	spotlight point_at 
	#if(global_anim_npoints_cam_view>2)
		global_anim_spline_cam_view(clock)
	#else
		global_anim_spline_cam_flight(clock+0.01)-<0,-0.01,0>
	#end
	radius 35 falloff  40
}
#else
camera
{
	location <cam_x,cam_y,cam_z>
	look_at <cam_look_x,cam_look_y,cam_look_z>
	angle cam_a
	//translates the camera that <0,0,0> is over the Eagle <0,0>
	//translate<-27.940000,0,-21.590000>
}
#end

background{col_bgr}
light_source{<lgt1_pos_x,lgt1_pos_y,lgt1_pos_z> White*lgt1_intense}
light_source{<lgt2_pos_x,lgt2_pos_y,lgt2_pos_z> White*lgt2_intense}
light_source{<lgt3_pos_x,lgt3_pos_y,lgt3_pos_z> White*lgt3_intense}
light_source{<lgt4_pos_x,lgt4_pos_y,lgt4_pos_z> White*lgt4_intense}
#end


#macro TINY13_TEST(mac_x_ver,mac_y_ver,mac_z_ver,mac_x_rot,mac_y_rot,mac_z_rot)
union{
#if(pcb_board = on)
difference{
union{
//Board
prism{-1.500000,0.000000,8
<0.000000,0.000000><55.880000,0.000000>
<55.880000,0.000000><55.880000,43.180000>
<55.880000,43.180000><0.000000,43.180000>
<0.000000,43.180000><0.000000,0.000000>
texture{col_brd}}
}//End union(PCB)
//Holes(real)/Parts
//Holes(real)/Board
cylinder{<2.540000,1,40.640000><2.540000,-5,40.640000>1.651000 texture{col_hls}}
cylinder{<2.540000,1,2.540000><2.540000,-5,2.540000>1.651000 texture{col_hls}}
cylinder{<53.340000,1,2.540000><53.340000,-5,2.540000>1.651000 texture{col_hls}}
cylinder{<53.340000,1,40.640000><53.340000,-5,40.640000>1.651000 texture{col_hls}}
//Holes(real)/Vias
}//End difference(reale Bohrungen/Durchbrüche)
#end
#if(pcb_parts=on)//Parts
union{
#ifndef(pack_C1) #declare global_pack_C1=yes; object {CAP_DIS_WIMA_25_025_046_075("22pF",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<7.620000,0.000000,10.160000>}#end		//WIMA-Capacitor Grid 2.54 (capacitator-wima.lib) C1 22pF C2.5-2
#ifndef(pack_C2) #declare global_pack_C2=yes; object {CAP_DIS_WIMA_25_025_046_075("22pF",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<7.620000,0.000000,15.240000>}#end		//WIMA-Capacitor Grid 2.54 (capacitator-wima.lib) C2 22pF C2.5-2
#ifndef(pack_IC1) #declare global_pack_IC1=yes; object {IC_DIS_DIP8("TINY13-20PU","ATMEL",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<22.860000,0.000000,12.700000>translate<0,3.000000,0> }#end		//DIP8 IC1 TINY13-20PU DIL08
#ifndef(pack_IC1) object{SOCKET_DIP8()rotate<0,0.000000,0> rotate<0,0,0> translate<22.860000,0.000000,12.700000>}#end					//IC-Sockel 8Pin IC1 TINY13-20PU
#ifndef(pack_LED1) #declare global_pack_LED1=yes; object {DIODE_DIS_LED_5MM(Red,0.500000,0.000000,)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<40.640000,0.000000,35.560000>}#end		//Diskrete 5MM LED LED1 A LED5MM
#ifndef(pack_LED2) #declare global_pack_LED2=yes; object {DIODE_DIS_LED_5MM(Red,0.500000,0.000000,)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<40.640000,0.000000,25.400000>}#end		//Diskrete 5MM LED LED2 B LED5MM
#ifndef(pack_Q1) #declare global_pack_Q1=yes; object {SPC_XTAL_5MM("1Mhz",3,)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<43.180000,0.000000,12.700000>}#end		//Quarz 4,9MM Q1 1Mhz HC49/S
#ifndef(pack_R1) #declare global_pack_R1=yes; object {RES_DIS_PR_01_15MM("10K",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<22.860000,0.000000,22.860000>}#end		//1W Res D2.5 x L8.5 R1 10K AC01
#ifndef(pack_R2) #declare global_pack_R2=yes; object {RES_DIS_PR_01_15MM("470",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<22.860000,0.000000,30.480000>}#end		//1W Res D2.5 x L8.5 R2 470 AC01
#ifndef(pack_R3) #declare global_pack_R3=yes; object {RES_DIS_PR_01_15MM("470",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<22.860000,0.000000,38.100000>}#end		//1W Res D2.5 x L8.5 R3 470 AC01
#ifndef(pack_S1) #declare global_pack_S1=yes; object {SWITCH_B3F_10XX1()translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<5.080000,0.000000,30.480000>}#end		//Tactile Switch-Omron S1 RESET B3F-10XX
}//End union
#end
#if(pcb_pads_smds=on)
//Pads&SMD/Parts
#ifndef(global_pack_C1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.600200,0.812800,1,16,2+global_tmp,0) rotate<0,-0.000000,0>translate<6.350000,0,10.160000> texture{col_thl}}
#ifndef(global_pack_C1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.600200,0.812800,1,16,2+global_tmp,0) rotate<0,-0.000000,0>translate<8.890000,0,10.160000> texture{col_thl}}
#ifndef(global_pack_C2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.600200,0.812800,1,16,2+global_tmp,0) rotate<0,-0.000000,0>translate<6.350000,0,15.240000> texture{col_thl}}
#ifndef(global_pack_C2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.600200,0.812800,1,16,2+global_tmp,0) rotate<0,-0.000000,0>translate<8.890000,0,15.240000> texture{col_thl}}
#ifndef(global_pack_IC1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<19.050000,0,8.890000> texture{col_thl}}
#ifndef(global_pack_IC1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<21.590000,0,8.890000> texture{col_thl}}
#ifndef(global_pack_IC1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<24.130000,0,8.890000> texture{col_thl}}
#ifndef(global_pack_IC1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<26.670000,0,8.890000> texture{col_thl}}
#ifndef(global_pack_IC1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<26.670000,0,16.510000> texture{col_thl}}
#ifndef(global_pack_IC1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<24.130000,0,16.510000> texture{col_thl}}
#ifndef(global_pack_IC1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<21.590000,0,16.510000> texture{col_thl}}
#ifndef(global_pack_IC1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<19.050000,0,16.510000> texture{col_thl}}
#ifndef(global_pack_LED1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,2+global_tmp,0) rotate<0,-0.000000,0>translate<39.370000,0,35.560000> texture{col_thl}}
#ifndef(global_pack_LED1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,2+global_tmp,0) rotate<0,-0.000000,0>translate<41.910000,0,35.560000> texture{col_thl}}
#ifndef(global_pack_LED2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,2+global_tmp,0) rotate<0,-0.000000,0>translate<39.370000,0,25.400000> texture{col_thl}}
#ifndef(global_pack_LED2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,2+global_tmp,0) rotate<0,-0.000000,0>translate<41.910000,0,25.400000> texture{col_thl}}
#ifndef(global_pack_Q1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,1+global_tmp,0) rotate<0,-0.000000,0>translate<40.767000,0,12.700000> texture{col_thl}}
#ifndef(global_pack_Q1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,1+global_tmp,0) rotate<0,-0.000000,0>translate<45.593000,0,12.700000> texture{col_thl}}
#ifndef(global_pack_R1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(2.000000,1.100000,1,16,1+global_tmp,0) rotate<0,-0.000000,0>translate<13.960000,0,22.860000> texture{col_thl}}
#ifndef(global_pack_R1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(2.000000,1.100000,1,16,1+global_tmp,0) rotate<0,-0.000000,0>translate<31.760000,0,22.860000> texture{col_thl}}
#ifndef(global_pack_R2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(2.000000,1.100000,1,16,1+global_tmp,0) rotate<0,-0.000000,0>translate<13.960000,0,30.480000> texture{col_thl}}
#ifndef(global_pack_R2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(2.000000,1.100000,1,16,1+global_tmp,0) rotate<0,-0.000000,0>translate<31.760000,0,30.480000> texture{col_thl}}
#ifndef(global_pack_R3) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(2.000000,1.100000,1,16,1+global_tmp,0) rotate<0,-0.000000,0>translate<13.960000,0,38.100000> texture{col_thl}}
#ifndef(global_pack_R3) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(2.000000,1.100000,1,16,1+global_tmp,0) rotate<0,-0.000000,0>translate<31.760000,0,38.100000> texture{col_thl}}
#ifndef(global_pack_S1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-0.000000,0>translate<1.828800,0,32.740600> texture{col_thl}}
#ifndef(global_pack_S1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-0.000000,0>translate<8.331200,0,32.740600> texture{col_thl}}
#ifndef(global_pack_S1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-0.000000,0>translate<1.828800,0,28.219400> texture{col_thl}}
#ifndef(global_pack_S1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-0.000000,0>translate<8.331200,0,28.219400> texture{col_thl}}
//Pads/Vias
#end
#if(pcb_wires=on)
union{
//Signals
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<1.828800,-1.535000,28.219400>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.540000,-1.535000,27.940000>}
box{<0,0,-0.254000><0.764114,0.035000,0.254000> rotate<0,21.446321,0> translate<1.828800,-1.535000,28.219400> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<1.828800,-1.535000,32.740600>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.540000,-1.535000,33.020000>}
box{<0,0,-0.254000><0.764114,0.035000,0.254000> rotate<0,-21.446321,0> translate<1.828800,-1.535000,32.740600> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.350000,-1.535000,8.890000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.350000,-1.535000,10.160000>}
box{<0,0,-0.254000><1.270000,0.035000,0.254000> rotate<0,90.000000,0> translate<6.350000,-1.535000,10.160000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.350000,-1.535000,10.160000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.350000,-1.535000,15.240000>}
box{<0,0,-0.254000><5.080000,0.035000,0.254000> rotate<0,90.000000,0> translate<6.350000,-1.535000,15.240000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.540000,-1.535000,27.940000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.350000,-1.535000,27.940000>}
box{<0,0,-0.254000><3.810000,0.035000,0.254000> rotate<0,0.000000,0> translate<2.540000,-1.535000,27.940000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.350000,-1.535000,15.240000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.350000,-1.535000,27.940000>}
box{<0,0,-0.254000><12.700000,0.035000,0.254000> rotate<0,90.000000,0> translate<6.350000,-1.535000,27.940000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.620000,-1.535000,8.890000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.620000,-1.535000,11.430000>}
box{<0,0,-0.254000><2.540000,0.035000,0.254000> rotate<0,90.000000,0> translate<7.620000,-1.535000,11.430000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.620000,-1.535000,11.430000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.620000,-1.535000,20.320000>}
box{<0,0,-0.254000><8.890000,0.035000,0.254000> rotate<0,90.000000,0> translate<7.620000,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.350000,-1.535000,27.940000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.620000,-1.535000,27.940000>}
box{<0,0,-0.254000><1.270000,0.035000,0.254000> rotate<0,0.000000,0> translate<6.350000,-1.535000,27.940000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.540000,-1.535000,33.020000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.620000,-1.535000,33.020000>}
box{<0,0,-0.254000><5.080000,0.035000,0.254000> rotate<0,0.000000,0> translate<2.540000,-1.535000,33.020000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.620000,-1.535000,27.940000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<8.331200,-1.535000,28.219400>}
box{<0,0,-0.254000><0.764114,0.035000,0.254000> rotate<0,-21.446321,0> translate<7.620000,-1.535000,27.940000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.620000,-1.535000,33.020000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<8.331200,-1.535000,32.740600>}
box{<0,0,-0.254000><0.764114,0.035000,0.254000> rotate<0,21.446321,0> translate<7.620000,-1.535000,33.020000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<8.890000,-1.535000,15.240000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<8.890000,-1.535000,20.320000>}
box{<0,0,-0.254000><5.080000,0.035000,0.254000> rotate<0,90.000000,0> translate<8.890000,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<8.331200,-1.535000,32.740600>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<8.890000,-1.535000,33.020000>}
box{<0,0,-0.254000><0.624757,0.035000,0.254000> rotate<0,-26.563298,0> translate<8.331200,-1.535000,32.740600> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<8.890000,-1.535000,30.480000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<8.890000,-1.535000,33.020000>}
box{<0,0,-0.254000><2.540000,0.035000,0.254000> rotate<0,90.000000,0> translate<8.890000,-1.535000,33.020000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.350000,-1.535000,8.890000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<10.160000,-1.535000,5.080000>}
box{<0,0,-0.254000><5.388154,0.035000,0.254000> rotate<0,44.997030,0> translate<6.350000,-1.535000,8.890000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<8.890000,-1.535000,10.160000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<10.160000,-1.535000,11.430000>}
box{<0,0,-0.254000><1.796051,0.035000,0.254000> rotate<0,-44.997030,0> translate<8.890000,-1.535000,10.160000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.620000,-1.535000,20.320000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<11.430000,-1.535000,24.130000>}
box{<0,0,-0.254000><5.388154,0.035000,0.254000> rotate<0,-44.997030,0> translate<7.620000,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<8.890000,-1.535000,30.480000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<11.430000,-1.535000,27.940000>}
box{<0,0,-0.254000><3.592102,0.035000,0.254000> rotate<0,44.997030,0> translate<8.890000,-1.535000,30.480000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<11.430000,-1.535000,24.130000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<11.430000,-1.535000,27.940000>}
box{<0,0,-0.254000><3.810000,0.035000,0.254000> rotate<0,90.000000,0> translate<11.430000,-1.535000,27.940000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.620000,-1.535000,11.430000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<13.970000,-1.535000,17.780000>}
box{<0,0,-0.254000><8.980256,0.035000,0.254000> rotate<0,-44.997030,0> translate<7.620000,-1.535000,11.430000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<13.960000,-1.535000,22.860000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<13.970000,-1.535000,22.860000>}
box{<0,0,-0.254000><0.010000,0.035000,0.254000> rotate<0,0.000000,0> translate<13.960000,-1.535000,22.860000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<13.970000,-1.535000,17.780000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<13.970000,-1.535000,22.860000>}
box{<0,0,-0.254000><5.080000,0.035000,0.254000> rotate<0,90.000000,0> translate<13.970000,-1.535000,22.860000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<8.890000,-1.535000,20.320000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<13.970000,-1.535000,25.400000>}
box{<0,0,-0.254000><7.184205,0.035000,0.254000> rotate<0,-44.997030,0> translate<8.890000,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<13.960000,-1.535000,30.480000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<13.970000,-1.535000,30.480000>}
box{<0,0,-0.254000><0.010000,0.035000,0.254000> rotate<0,0.000000,0> translate<13.960000,-1.535000,30.480000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<13.960000,-1.535000,38.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<13.970000,-1.535000,38.100000>}
box{<0,0,-0.254000><0.010000,0.035000,0.254000> rotate<0,0.000000,0> translate<13.960000,-1.535000,38.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<17.780000,-1.535000,21.590000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<17.780000,-1.535000,15.240000>}
box{<0,0,-0.254000><6.350000,0.035000,0.254000> rotate<0,-90.000000,0> translate<17.780000,-1.535000,15.240000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<13.970000,-1.535000,25.400000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<17.780000,-1.535000,21.590000>}
box{<0,0,-0.254000><5.388154,0.035000,0.254000> rotate<0,44.997030,0> translate<13.970000,-1.535000,25.400000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<13.970000,-1.535000,38.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<17.780000,-1.535000,41.910000>}
box{<0,0,-0.254000><5.388154,0.035000,0.254000> rotate<0,-44.997030,0> translate<13.970000,-1.535000,38.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.620000,-1.535000,8.890000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<19.050000,-1.535000,8.890000>}
box{<0,0,-0.254000><11.430000,0.035000,0.254000> rotate<0,0.000000,0> translate<7.620000,-1.535000,8.890000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<17.780000,-1.535000,15.240000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<19.050000,-1.535000,13.970000>}
box{<0,0,-0.254000><1.796051,0.035000,0.254000> rotate<0,44.997030,0> translate<17.780000,-1.535000,15.240000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<20.320000,-1.535000,11.430000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<20.320000,-1.535000,7.620000>}
box{<0,0,-0.254000><3.810000,0.035000,0.254000> rotate<0,-90.000000,0> translate<20.320000,-1.535000,7.620000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<10.160000,-1.535000,11.430000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<20.320000,-1.535000,11.430000>}
box{<0,0,-0.254000><10.160000,0.035000,0.254000> rotate<0,0.000000,0> translate<10.160000,-1.535000,11.430000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<20.320000,-1.535000,7.620000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<21.590000,-1.535000,6.350000>}
box{<0,0,-0.254000><1.796051,0.035000,0.254000> rotate<0,44.997030,0> translate<20.320000,-1.535000,7.620000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<20.320000,-1.535000,11.430000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<21.590000,-1.535000,11.430000>}
box{<0,0,-0.254000><1.270000,0.035000,0.254000> rotate<0,0.000000,0> translate<20.320000,-1.535000,11.430000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<19.050000,-1.535000,13.970000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<21.590000,-1.535000,13.970000>}
box{<0,0,-0.254000><2.540000,0.035000,0.254000> rotate<0,0.000000,0> translate<19.050000,-1.535000,13.970000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<21.590000,-1.535000,8.890000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<24.130000,-1.535000,11.430000>}
box{<0,0,-0.254000><3.592102,0.035000,0.254000> rotate<0,-44.997030,0> translate<21.590000,-1.535000,8.890000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<21.590000,-1.535000,13.970000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<24.130000,-1.535000,16.510000>}
box{<0,0,-0.254000><3.592102,0.035000,0.254000> rotate<0,-44.997030,0> translate<21.590000,-1.535000,13.970000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<13.970000,-1.535000,30.480000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<24.130000,-1.535000,40.640000>}
box{<0,0,-0.254000><14.368410,0.035000,0.254000> rotate<0,-44.997030,0> translate<13.970000,-1.535000,30.480000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<21.590000,-1.535000,16.510000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<25.400000,-1.535000,20.320000>}
box{<0,0,-0.254000><5.388154,0.035000,0.254000> rotate<0,-44.997030,0> translate<21.590000,-1.535000,16.510000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<19.050000,-1.535000,16.510000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<25.400000,-1.535000,22.860000>}
box{<0,0,-0.254000><8.980256,0.035000,0.254000> rotate<0,-44.997030,0> translate<19.050000,-1.535000,16.510000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<21.590000,-1.535000,11.430000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<26.670000,-1.535000,16.510000>}
box{<0,0,-0.254000><7.184205,0.035000,0.254000> rotate<0,-44.997030,0> translate<21.590000,-1.535000,11.430000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<24.130000,-1.535000,16.510000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<26.670000,-1.535000,19.050000>}
box{<0,0,-0.254000><3.592102,0.035000,0.254000> rotate<0,-44.997030,0> translate<24.130000,-1.535000,16.510000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<25.400000,-1.535000,20.320000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<31.750000,-1.535000,20.320000>}
box{<0,0,-0.254000><6.350000,0.035000,0.254000> rotate<0,0.000000,0> translate<25.400000,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<25.400000,-1.535000,22.860000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<31.750000,-1.535000,22.860000>}
box{<0,0,-0.254000><6.350000,0.035000,0.254000> rotate<0,0.000000,0> translate<25.400000,-1.535000,22.860000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<24.130000,-1.535000,40.640000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<31.750000,-1.535000,40.640000>}
box{<0,0,-0.254000><7.620000,0.035000,0.254000> rotate<0,0.000000,0> translate<24.130000,-1.535000,40.640000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<31.750000,-1.535000,22.860000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<31.760000,-1.535000,22.860000>}
box{<0,0,-0.254000><0.010000,0.035000,0.254000> rotate<0,0.000000,0> translate<31.750000,-1.535000,22.860000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<31.750000,-1.535000,30.480000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<31.760000,-1.535000,30.480000>}
box{<0,0,-0.254000><0.010000,0.035000,0.254000> rotate<0,0.000000,0> translate<31.750000,-1.535000,30.480000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<31.750000,-1.535000,38.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<31.760000,-1.535000,38.100000>}
box{<0,0,-0.254000><0.010000,0.035000,0.254000> rotate<0,0.000000,0> translate<31.750000,-1.535000,38.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<26.670000,-1.535000,19.050000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<33.020000,-1.535000,12.700000>}
box{<0,0,-0.254000><8.980256,0.035000,0.254000> rotate<0,44.997030,0> translate<26.670000,-1.535000,19.050000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<31.750000,-1.535000,20.320000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<34.290000,-1.535000,22.860000>}
box{<0,0,-0.254000><3.592102,0.035000,0.254000> rotate<0,-44.997030,0> translate<31.750000,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<31.750000,-1.535000,30.480000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<34.290000,-1.535000,27.940000>}
box{<0,0,-0.254000><3.592102,0.035000,0.254000> rotate<0,44.997030,0> translate<31.750000,-1.535000,30.480000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<34.290000,-1.535000,22.860000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<34.290000,-1.535000,27.940000>}
box{<0,0,-0.254000><5.080000,0.035000,0.254000> rotate<0,90.000000,0> translate<34.290000,-1.535000,27.940000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<31.750000,-1.535000,38.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<35.560000,-1.535000,34.290000>}
box{<0,0,-0.254000><5.388154,0.035000,0.254000> rotate<0,44.997030,0> translate<31.750000,-1.535000,38.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<35.560000,-1.535000,26.670000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<35.560000,-1.535000,34.290000>}
box{<0,0,-0.254000><7.620000,0.035000,0.254000> rotate<0,90.000000,0> translate<35.560000,-1.535000,34.290000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<17.780000,-1.535000,41.910000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<35.560000,-1.535000,41.910000>}
box{<0,0,-0.254000><17.780000,0.035000,0.254000> rotate<0,0.000000,0> translate<17.780000,-1.535000,41.910000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<31.750000,-1.535000,40.640000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<36.830000,-1.535000,35.560000>}
box{<0,0,-0.254000><7.184205,0.035000,0.254000> rotate<0,44.997030,0> translate<31.750000,-1.535000,40.640000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<21.590000,-1.535000,6.350000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<39.370000,-1.535000,6.350000>}
box{<0,0,-0.254000><17.780000,0.035000,0.254000> rotate<0,0.000000,0> translate<21.590000,-1.535000,6.350000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<39.370000,-1.535000,33.020000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<39.370000,-1.535000,25.400000>}
box{<0,0,-0.254000><7.620000,0.035000,0.254000> rotate<0,-90.000000,0> translate<39.370000,-1.535000,25.400000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<36.830000,-1.535000,35.560000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<39.370000,-1.535000,35.560000>}
box{<0,0,-0.254000><2.540000,0.035000,0.254000> rotate<0,0.000000,0> translate<36.830000,-1.535000,35.560000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<10.160000,-1.535000,5.080000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<40.640000,-1.535000,5.080000>}
box{<0,0,-0.254000><30.480000,0.035000,0.254000> rotate<0,0.000000,0> translate<10.160000,-1.535000,5.080000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<26.670000,-1.535000,8.890000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<40.640000,-1.535000,8.890000>}
box{<0,0,-0.254000><13.970000,0.035000,0.254000> rotate<0,0.000000,0> translate<26.670000,-1.535000,8.890000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<33.020000,-1.535000,12.700000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<40.640000,-1.535000,12.700000>}
box{<0,0,-0.254000><7.620000,0.035000,0.254000> rotate<0,0.000000,0> translate<33.020000,-1.535000,12.700000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<39.370000,-1.535000,33.020000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<40.640000,-1.535000,34.290000>}
box{<0,0,-0.254000><1.796051,0.035000,0.254000> rotate<0,-44.997030,0> translate<39.370000,-1.535000,33.020000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<40.640000,-1.535000,36.830000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<40.640000,-1.535000,34.290000>}
box{<0,0,-0.254000><2.540000,0.035000,0.254000> rotate<0,-90.000000,0> translate<40.640000,-1.535000,34.290000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<35.560000,-1.535000,41.910000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<40.640000,-1.535000,36.830000>}
box{<0,0,-0.254000><7.184205,0.035000,0.254000> rotate<0,44.997030,0> translate<35.560000,-1.535000,41.910000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<40.640000,-1.535000,12.700000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<40.767000,-1.535000,12.700000>}
box{<0,0,-0.254000><0.127000,0.035000,0.254000> rotate<0,0.000000,0> translate<40.640000,-1.535000,12.700000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<24.130000,-1.535000,11.430000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<41.910000,-1.535000,11.430000>}
box{<0,0,-0.254000><17.780000,0.035000,0.254000> rotate<0,0.000000,0> translate<24.130000,-1.535000,11.430000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<35.560000,-1.535000,26.670000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<41.910000,-1.535000,20.320000>}
box{<0,0,-0.254000><8.980256,0.035000,0.254000> rotate<0,44.997030,0> translate<35.560000,-1.535000,26.670000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<41.910000,-1.535000,11.430000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<41.910000,-1.535000,20.320000>}
box{<0,0,-0.254000><8.890000,0.035000,0.254000> rotate<0,90.000000,0> translate<41.910000,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<41.910000,-1.535000,25.400000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<41.910000,-1.535000,35.560000>}
box{<0,0,-0.254000><10.160000,0.035000,0.254000> rotate<0,90.000000,0> translate<41.910000,-1.535000,35.560000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<40.640000,-1.535000,8.890000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<43.180000,-1.535000,11.430000>}
box{<0,0,-0.254000><3.592102,0.035000,0.254000> rotate<0,-44.997030,0> translate<40.640000,-1.535000,8.890000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<43.180000,-1.535000,17.780000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<43.180000,-1.535000,11.430000>}
box{<0,0,-0.254000><6.350000,0.035000,0.254000> rotate<0,-90.000000,0> translate<43.180000,-1.535000,11.430000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<41.910000,-1.535000,25.400000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<43.180000,-1.535000,24.130000>}
box{<0,0,-0.254000><1.796051,0.035000,0.254000> rotate<0,44.997030,0> translate<41.910000,-1.535000,25.400000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<43.180000,-1.535000,17.780000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<43.180000,-1.535000,24.130000>}
box{<0,0,-0.254000><6.350000,0.035000,0.254000> rotate<0,90.000000,0> translate<43.180000,-1.535000,24.130000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<39.370000,-1.535000,6.350000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<45.720000,-1.535000,12.700000>}
box{<0,0,-0.254000><8.980256,0.035000,0.254000> rotate<0,-44.997030,0> translate<39.370000,-1.535000,6.350000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<45.593000,-1.535000,12.700000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<45.720000,-1.535000,12.700000>}
box{<0,0,-0.254000><0.127000,0.035000,0.254000> rotate<0,0.000000,0> translate<45.593000,-1.535000,12.700000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<40.640000,-1.535000,5.080000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<46.990000,-1.535000,11.430000>}
box{<0,0,-0.254000><8.980256,0.035000,0.254000> rotate<0,-44.997030,0> translate<40.640000,-1.535000,5.080000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<43.180000,-1.535000,17.780000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<46.990000,-1.535000,13.970000>}
box{<0,0,-0.254000><5.388154,0.035000,0.254000> rotate<0,44.997030,0> translate<43.180000,-1.535000,17.780000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<46.990000,-1.535000,11.430000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<46.990000,-1.535000,13.970000>}
box{<0,0,-0.254000><2.540000,0.035000,0.254000> rotate<0,90.000000,0> translate<46.990000,-1.535000,13.970000> }
//Text
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.170300,-1.535000,15.290800>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.560100,-1.535000,15.680500>}
box{<0,0,-0.050800><0.551190,0.035000,0.050800> rotate<0,-44.989680,0> translate<54.170300,-1.535000,15.290800> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.560100,-1.535000,15.680500>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.390800,-1.535000,15.680500>}
box{<0,0,-0.050800><1.169300,0.035000,0.050800> rotate<0,0.000000,0> translate<53.390800,-1.535000,15.680500> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.390800,-1.535000,15.290800>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.390800,-1.535000,16.070300>}
box{<0,0,-0.050800><0.779500,0.035000,0.050800> rotate<0,90.000000,0> translate<53.390800,-1.535000,16.070300> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.390800,-1.535000,17.239600>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.390800,-1.535000,16.460100>}
box{<0,0,-0.050800><0.779500,0.035000,0.050800> rotate<0,-90.000000,0> translate<53.390800,-1.535000,16.460100> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.390800,-1.535000,16.460100>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.170300,-1.535000,17.239600>}
box{<0,0,-0.050800><1.102379,0.035000,0.050800> rotate<0,-44.997030,0> translate<53.390800,-1.535000,16.460100> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.170300,-1.535000,17.239600>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.365200,-1.535000,17.239600>}
box{<0,0,-0.050800><0.194900,0.035000,0.050800> rotate<0,0.000000,0> translate<54.170300,-1.535000,17.239600> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.365200,-1.535000,17.239600>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.560100,-1.535000,17.044700>}
box{<0,0,-0.050800><0.275630,0.035000,0.050800> rotate<0,44.997030,0> translate<54.365200,-1.535000,17.239600> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.560100,-1.535000,17.044700>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.560100,-1.535000,16.654900>}
box{<0,0,-0.050800><0.389800,0.035000,0.050800> rotate<0,-90.000000,0> translate<54.560100,-1.535000,16.654900> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.560100,-1.535000,16.654900>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.365200,-1.535000,16.460100>}
box{<0,0,-0.050800><0.275560,0.035000,0.050800> rotate<0,-44.982329,0> translate<54.365200,-1.535000,16.460100> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.975400,-1.535000,17.629400>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.975400,-1.535000,18.408900>}
box{<0,0,-0.050800><0.779500,0.035000,0.050800> rotate<0,90.000000,0> translate<53.975400,-1.535000,18.408900> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.170300,-1.535000,18.798700>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.560100,-1.535000,19.188400>}
box{<0,0,-0.050800><0.551190,0.035000,0.050800> rotate<0,-44.989680,0> translate<54.170300,-1.535000,18.798700> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.560100,-1.535000,19.188400>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.390800,-1.535000,19.188400>}
box{<0,0,-0.050800><1.169300,0.035000,0.050800> rotate<0,0.000000,0> translate<53.390800,-1.535000,19.188400> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.390800,-1.535000,18.798700>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.390800,-1.535000,19.578200>}
box{<0,0,-0.050800><0.779500,0.035000,0.050800> rotate<0,90.000000,0> translate<53.390800,-1.535000,19.578200> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.170300,-1.535000,19.968000>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.560100,-1.535000,20.357700>}
box{<0,0,-0.050800><0.551190,0.035000,0.050800> rotate<0,-44.989680,0> translate<54.170300,-1.535000,19.968000> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.560100,-1.535000,20.357700>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.390800,-1.535000,20.357700>}
box{<0,0,-0.050800><1.169300,0.035000,0.050800> rotate<0,0.000000,0> translate<53.390800,-1.535000,20.357700> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.390800,-1.535000,19.968000>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.390800,-1.535000,20.747500>}
box{<0,0,-0.050800><0.779500,0.035000,0.050800> rotate<0,90.000000,0> translate<53.390800,-1.535000,20.747500> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.975400,-1.535000,21.137300>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.975400,-1.535000,21.916800>}
box{<0,0,-0.050800><0.779500,0.035000,0.050800> rotate<0,90.000000,0> translate<53.975400,-1.535000,21.916800> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.585600,-1.535000,22.306600>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.365200,-1.535000,22.306600>}
box{<0,0,-0.050800><0.779600,0.035000,0.050800> rotate<0,0.000000,0> translate<53.585600,-1.535000,22.306600> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.365200,-1.535000,22.306600>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.560100,-1.535000,22.501400>}
box{<0,0,-0.050800><0.275560,0.035000,0.050800> rotate<0,-44.982329,0> translate<54.365200,-1.535000,22.306600> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.560100,-1.535000,22.501400>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.560100,-1.535000,22.891200>}
box{<0,0,-0.050800><0.389800,0.035000,0.050800> rotate<0,90.000000,0> translate<54.560100,-1.535000,22.891200> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.560100,-1.535000,22.891200>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.365200,-1.535000,23.086100>}
box{<0,0,-0.050800><0.275630,0.035000,0.050800> rotate<0,44.997030,0> translate<54.365200,-1.535000,23.086100> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.365200,-1.535000,23.086100>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.585600,-1.535000,23.086100>}
box{<0,0,-0.050800><0.779600,0.035000,0.050800> rotate<0,0.000000,0> translate<53.585600,-1.535000,23.086100> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.585600,-1.535000,23.086100>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.390800,-1.535000,22.891200>}
box{<0,0,-0.050800><0.275560,0.035000,0.050800> rotate<0,-45.011732,0> translate<53.390800,-1.535000,22.891200> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.390800,-1.535000,22.891200>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.390800,-1.535000,22.501400>}
box{<0,0,-0.050800><0.389800,0.035000,0.050800> rotate<0,-90.000000,0> translate<53.390800,-1.535000,22.501400> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.390800,-1.535000,22.501400>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.585600,-1.535000,22.306600>}
box{<0,0,-0.050800><0.275489,0.035000,0.050800> rotate<0,44.997030,0> translate<53.390800,-1.535000,22.501400> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.585600,-1.535000,22.306600>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.365200,-1.535000,23.086100>}
box{<0,0,-0.050800><1.102450,0.035000,0.050800> rotate<0,-44.993355,0> translate<53.585600,-1.535000,22.306600> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.390800,-1.535000,24.255400>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.390800,-1.535000,23.475900>}
box{<0,0,-0.050800><0.779500,0.035000,0.050800> rotate<0,-90.000000,0> translate<53.390800,-1.535000,23.475900> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<53.390800,-1.535000,23.475900>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.170300,-1.535000,24.255400>}
box{<0,0,-0.050800><1.102379,0.035000,0.050800> rotate<0,-44.997030,0> translate<53.390800,-1.535000,23.475900> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.170300,-1.535000,24.255400>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.365200,-1.535000,24.255400>}
box{<0,0,-0.050800><0.194900,0.035000,0.050800> rotate<0,0.000000,0> translate<54.170300,-1.535000,24.255400> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.365200,-1.535000,24.255400>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.560100,-1.535000,24.060500>}
box{<0,0,-0.050800><0.275630,0.035000,0.050800> rotate<0,44.997030,0> translate<54.365200,-1.535000,24.255400> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.560100,-1.535000,24.060500>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.560100,-1.535000,23.670700>}
box{<0,0,-0.050800><0.389800,0.035000,0.050800> rotate<0,-90.000000,0> translate<54.560100,-1.535000,23.670700> }
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.560100,-1.535000,23.670700>}
cylinder{<0,0,0><0,0.035000,0>0.050800 translate<54.365200,-1.535000,23.475900>}
box{<0,0,-0.050800><0.275560,0.035000,0.050800> rotate<0,-44.982329,0> translate<54.365200,-1.535000,23.475900> }
//Rect
union{
texture{col_pds}
}
texture{col_wrs}
}
#end
#if(pcb_polygons=on)
union{
//Polygons
texture{col_pol}
}
#end
union{
cylinder{<6.350000,0.038000,10.160000><6.350000,-1.538000,10.160000>0.406400}
cylinder{<8.890000,0.038000,10.160000><8.890000,-1.538000,10.160000>0.406400}
cylinder{<6.350000,0.038000,15.240000><6.350000,-1.538000,15.240000>0.406400}
cylinder{<8.890000,0.038000,15.240000><8.890000,-1.538000,15.240000>0.406400}
cylinder{<19.050000,0.038000,8.890000><19.050000,-1.538000,8.890000>0.406400}
cylinder{<21.590000,0.038000,8.890000><21.590000,-1.538000,8.890000>0.406400}
cylinder{<24.130000,0.038000,8.890000><24.130000,-1.538000,8.890000>0.406400}
cylinder{<26.670000,0.038000,8.890000><26.670000,-1.538000,8.890000>0.406400}
cylinder{<26.670000,0.038000,16.510000><26.670000,-1.538000,16.510000>0.406400}
cylinder{<24.130000,0.038000,16.510000><24.130000,-1.538000,16.510000>0.406400}
cylinder{<21.590000,0.038000,16.510000><21.590000,-1.538000,16.510000>0.406400}
cylinder{<19.050000,0.038000,16.510000><19.050000,-1.538000,16.510000>0.406400}
cylinder{<39.370000,0.038000,35.560000><39.370000,-1.538000,35.560000>0.406400}
cylinder{<41.910000,0.038000,35.560000><41.910000,-1.538000,35.560000>0.406400}
cylinder{<39.370000,0.038000,25.400000><39.370000,-1.538000,25.400000>0.406400}
cylinder{<41.910000,0.038000,25.400000><41.910000,-1.538000,25.400000>0.406400}
cylinder{<40.767000,0.038000,12.700000><40.767000,-1.538000,12.700000>0.406400}
cylinder{<45.593000,0.038000,12.700000><45.593000,-1.538000,12.700000>0.406400}
cylinder{<13.960000,0.038000,22.860000><13.960000,-1.538000,22.860000>0.550000}
cylinder{<31.760000,0.038000,22.860000><31.760000,-1.538000,22.860000>0.550000}
cylinder{<13.960000,0.038000,30.480000><13.960000,-1.538000,30.480000>0.550000}
cylinder{<31.760000,0.038000,30.480000><31.760000,-1.538000,30.480000>0.550000}
cylinder{<13.960000,0.038000,38.100000><13.960000,-1.538000,38.100000>0.550000}
cylinder{<31.760000,0.038000,38.100000><31.760000,-1.538000,38.100000>0.550000}
cylinder{<1.828800,0.038000,32.740600><1.828800,-1.538000,32.740600>0.508000}
cylinder{<8.331200,0.038000,32.740600><8.331200,-1.538000,32.740600>0.508000}
cylinder{<1.828800,0.038000,28.219400><1.828800,-1.538000,28.219400>0.508000}
cylinder{<8.331200,0.038000,28.219400><8.331200,-1.538000,28.219400>0.508000}
//Holes(fast)/Vias
//Holes(fast)/Board
texture{col_hls}
}
#if(pcb_silkscreen=on)
//Silk Screen
union{
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.590800,0.000000,25.450800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.590800,0.000000,26.620100>}
box{<0,0,-0.050800><1.169300,0.036000,0.050800> rotate<0,90.000000,0> translate<2.590800,0.000000,26.620100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.590800,0.000000,26.620100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<3.175400,0.000000,26.620100>}
box{<0,0,-0.050800><0.584600,0.036000,0.050800> rotate<0,0.000000,0> translate<2.590800,0.000000,26.620100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<3.175400,0.000000,26.620100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<3.370300,0.000000,26.425200>}
box{<0,0,-0.050800><0.275630,0.036000,0.050800> rotate<0,44.997030,0> translate<3.175400,0.000000,26.620100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<3.370300,0.000000,26.425200>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<3.370300,0.000000,26.035400>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,-90.000000,0> translate<3.370300,0.000000,26.035400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<3.370300,0.000000,26.035400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<3.175400,0.000000,25.840500>}
box{<0,0,-0.050800><0.275630,0.036000,0.050800> rotate<0,-44.997030,0> translate<3.175400,0.000000,25.840500> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<3.175400,0.000000,25.840500>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.590800,0.000000,25.840500>}
box{<0,0,-0.050800><0.584600,0.036000,0.050800> rotate<0,0.000000,0> translate<2.590800,0.000000,25.840500> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.980500,0.000000,25.840500>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<3.370300,0.000000,25.450800>}
box{<0,0,-0.050800><0.551190,0.036000,0.050800> rotate<0,44.989680,0> translate<2.980500,0.000000,25.840500> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<4.539600,0.000000,26.620100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<3.760100,0.000000,26.620100>}
box{<0,0,-0.050800><0.779500,0.036000,0.050800> rotate<0,0.000000,0> translate<3.760100,0.000000,26.620100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<3.760100,0.000000,26.620100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<3.760100,0.000000,25.450800>}
box{<0,0,-0.050800><1.169300,0.036000,0.050800> rotate<0,-90.000000,0> translate<3.760100,0.000000,25.450800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<3.760100,0.000000,25.450800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<4.539600,0.000000,25.450800>}
box{<0,0,-0.050800><0.779500,0.036000,0.050800> rotate<0,0.000000,0> translate<3.760100,0.000000,25.450800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<3.760100,0.000000,26.035400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<4.149800,0.000000,26.035400>}
box{<0,0,-0.050800><0.389700,0.036000,0.050800> rotate<0,0.000000,0> translate<3.760100,0.000000,26.035400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<5.708900,0.000000,26.425200>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<5.514000,0.000000,26.620100>}
box{<0,0,-0.050800><0.275630,0.036000,0.050800> rotate<0,44.997030,0> translate<5.514000,0.000000,26.620100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<5.514000,0.000000,26.620100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<5.124200,0.000000,26.620100>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,0.000000,0> translate<5.124200,0.000000,26.620100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<5.124200,0.000000,26.620100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<4.929400,0.000000,26.425200>}
box{<0,0,-0.050800><0.275560,0.036000,0.050800> rotate<0,-45.011732,0> translate<4.929400,0.000000,26.425200> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<4.929400,0.000000,26.425200>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<4.929400,0.000000,26.230300>}
box{<0,0,-0.050800><0.194900,0.036000,0.050800> rotate<0,-90.000000,0> translate<4.929400,0.000000,26.230300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<4.929400,0.000000,26.230300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<5.124200,0.000000,26.035400>}
box{<0,0,-0.050800><0.275560,0.036000,0.050800> rotate<0,45.011732,0> translate<4.929400,0.000000,26.230300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<5.124200,0.000000,26.035400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<5.514000,0.000000,26.035400>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,0.000000,0> translate<5.124200,0.000000,26.035400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<5.514000,0.000000,26.035400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<5.708900,0.000000,25.840500>}
box{<0,0,-0.050800><0.275630,0.036000,0.050800> rotate<0,44.997030,0> translate<5.514000,0.000000,26.035400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<5.708900,0.000000,25.840500>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<5.708900,0.000000,25.645600>}
box{<0,0,-0.050800><0.194900,0.036000,0.050800> rotate<0,-90.000000,0> translate<5.708900,0.000000,25.645600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<5.708900,0.000000,25.645600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<5.514000,0.000000,25.450800>}
box{<0,0,-0.050800><0.275560,0.036000,0.050800> rotate<0,-44.982329,0> translate<5.514000,0.000000,25.450800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<5.514000,0.000000,25.450800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<5.124200,0.000000,25.450800>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,0.000000,0> translate<5.124200,0.000000,25.450800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<5.124200,0.000000,25.450800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<4.929400,0.000000,25.645600>}
box{<0,0,-0.050800><0.275489,0.036000,0.050800> rotate<0,44.997030,0> translate<4.929400,0.000000,25.645600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<6.878200,0.000000,26.620100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<6.098700,0.000000,26.620100>}
box{<0,0,-0.050800><0.779500,0.036000,0.050800> rotate<0,0.000000,0> translate<6.098700,0.000000,26.620100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<6.098700,0.000000,26.620100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<6.098700,0.000000,25.450800>}
box{<0,0,-0.050800><1.169300,0.036000,0.050800> rotate<0,-90.000000,0> translate<6.098700,0.000000,25.450800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<6.098700,0.000000,25.450800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<6.878200,0.000000,25.450800>}
box{<0,0,-0.050800><0.779500,0.036000,0.050800> rotate<0,0.000000,0> translate<6.098700,0.000000,25.450800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<6.098700,0.000000,26.035400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<6.488400,0.000000,26.035400>}
box{<0,0,-0.050800><0.389700,0.036000,0.050800> rotate<0,0.000000,0> translate<6.098700,0.000000,26.035400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<7.657700,0.000000,25.450800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<7.657700,0.000000,26.620100>}
box{<0,0,-0.050800><1.169300,0.036000,0.050800> rotate<0,90.000000,0> translate<7.657700,0.000000,26.620100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<7.268000,0.000000,26.620100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.047500,0.000000,26.620100>}
box{<0,0,-0.050800><0.779500,0.036000,0.050800> rotate<0,0.000000,0> translate<7.268000,0.000000,26.620100> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.421600,0.000000,2.641600>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.590900,0.000000,2.641600>}
box{<0,0,-0.101600><1.169300,0.036000,0.101600> rotate<0,0.000000,0> translate<20.421600,0.000000,2.641600> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.590900,0.000000,2.641600>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.980700,0.000000,3.031300>}
box{<0,0,-0.101600><0.551190,0.036000,0.101600> rotate<0,-44.989680,0> translate<21.590900,0.000000,2.641600> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.980700,0.000000,3.031300>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.590900,0.000000,3.421100>}
box{<0,0,-0.101600><0.551260,0.036000,0.101600> rotate<0,44.997030,0> translate<21.590900,0.000000,3.421100> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.590900,0.000000,3.421100>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.811300,0.000000,3.421100>}
box{<0,0,-0.101600><0.779600,0.036000,0.101600> rotate<0,0.000000,0> translate<20.811300,0.000000,3.421100> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.811300,0.000000,3.421100>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.421600,0.000000,3.810900>}
box{<0,0,-0.101600><0.551190,0.036000,0.101600> rotate<0,45.004380,0> translate<20.421600,0.000000,3.810900> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.421600,0.000000,3.810900>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.811300,0.000000,4.200700>}
box{<0,0,-0.101600><0.551190,0.036000,0.101600> rotate<0,-45.004380,0> translate<20.421600,0.000000,3.810900> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<20.811300,0.000000,4.200700>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<21.980700,0.000000,4.200700>}
box{<0,0,-0.101600><1.169400,0.036000,0.101600> rotate<0,0.000000,0> translate<20.811300,0.000000,4.200700> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<23.150000,0.000000,4.200700>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<23.929600,0.000000,4.200700>}
box{<0,0,-0.101600><0.779600,0.036000,0.101600> rotate<0,0.000000,0> translate<23.150000,0.000000,4.200700> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<23.929600,0.000000,4.200700>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<24.319400,0.000000,3.810900>}
box{<0,0,-0.101600><0.551260,0.036000,0.101600> rotate<0,44.997030,0> translate<23.929600,0.000000,4.200700> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<24.319400,0.000000,3.810900>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<24.319400,0.000000,2.641600>}
box{<0,0,-0.101600><1.169300,0.036000,0.101600> rotate<0,-90.000000,0> translate<24.319400,0.000000,2.641600> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<24.319400,0.000000,2.641600>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<23.150000,0.000000,2.641600>}
box{<0,0,-0.101600><1.169400,0.036000,0.101600> rotate<0,0.000000,0> translate<23.150000,0.000000,2.641600> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<23.150000,0.000000,2.641600>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<22.760300,0.000000,3.031300>}
box{<0,0,-0.101600><0.551119,0.036000,0.101600> rotate<0,44.997030,0> translate<22.760300,0.000000,3.031300> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<22.760300,0.000000,3.031300>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<23.150000,0.000000,3.421100>}
box{<0,0,-0.101600><0.551190,0.036000,0.101600> rotate<0,-45.004380,0> translate<22.760300,0.000000,3.031300> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<23.150000,0.000000,3.421100>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<24.319400,0.000000,3.421100>}
box{<0,0,-0.101600><1.169400,0.036000,0.101600> rotate<0,0.000000,0> translate<23.150000,0.000000,3.421100> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<25.099000,0.000000,4.980300>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<25.488700,0.000000,4.980300>}
box{<0,0,-0.101600><0.389700,0.036000,0.101600> rotate<0,0.000000,0> translate<25.099000,0.000000,4.980300> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<25.488700,0.000000,4.980300>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<25.488700,0.000000,2.641600>}
box{<0,0,-0.101600><2.338700,0.036000,0.101600> rotate<0,-90.000000,0> translate<25.488700,0.000000,2.641600> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<25.099000,0.000000,2.641600>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<25.878500,0.000000,2.641600>}
box{<0,0,-0.101600><0.779500,0.036000,0.101600> rotate<0,0.000000,0> translate<25.099000,0.000000,2.641600> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<26.658100,0.000000,4.200700>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<27.437600,0.000000,2.641600>}
box{<0,0,-0.101600><1.743104,0.036000,0.101600> rotate<0,63.432232,0> translate<26.658100,0.000000,4.200700> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<27.437600,0.000000,2.641600>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<28.217200,0.000000,4.200700>}
box{<0,0,-0.101600><1.743149,0.036000,0.101600> rotate<0,-63.429293,0> translate<27.437600,0.000000,2.641600> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<28.996800,0.000000,4.200700>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.386500,0.000000,4.200700>}
box{<0,0,-0.101600><0.389700,0.036000,0.101600> rotate<0,0.000000,0> translate<28.996800,0.000000,4.200700> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.386500,0.000000,4.200700>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.386500,0.000000,2.641600>}
box{<0,0,-0.101600><1.559100,0.036000,0.101600> rotate<0,-90.000000,0> translate<29.386500,0.000000,2.641600> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<28.996800,0.000000,2.641600>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.776300,0.000000,2.641600>}
box{<0,0,-0.101600><0.779500,0.036000,0.101600> rotate<0,0.000000,0> translate<28.996800,0.000000,2.641600> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.386500,0.000000,5.370100>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<29.386500,0.000000,4.980300>}
box{<0,0,-0.101600><0.389800,0.036000,0.101600> rotate<0,-90.000000,0> translate<29.386500,0.000000,4.980300> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<30.555900,0.000000,4.200700>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<32.115000,0.000000,2.641600>}
box{<0,0,-0.101600><2.204900,0.036000,0.101600> rotate<0,44.997030,0> translate<30.555900,0.000000,4.200700> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<30.555900,0.000000,2.641600>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<32.115000,0.000000,4.200700>}
box{<0,0,-0.101600><2.204900,0.036000,0.101600> rotate<0,-44.997030,0> translate<30.555900,0.000000,2.641600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.010300,0.000000,2.590800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<43.230800,0.000000,2.590800>}
box{<0,0,-0.050800><0.779500,0.036000,0.050800> rotate<0,0.000000,0> translate<43.230800,0.000000,2.590800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<43.230800,0.000000,2.590800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.010300,0.000000,3.370300>}
box{<0,0,-0.050800><1.102379,0.036000,0.050800> rotate<0,-44.997030,0> translate<43.230800,0.000000,2.590800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.010300,0.000000,3.370300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.010300,0.000000,3.565200>}
box{<0,0,-0.050800><0.194900,0.036000,0.050800> rotate<0,90.000000,0> translate<44.010300,0.000000,3.565200> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.010300,0.000000,3.565200>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<43.815400,0.000000,3.760100>}
box{<0,0,-0.050800><0.275630,0.036000,0.050800> rotate<0,44.997030,0> translate<43.815400,0.000000,3.760100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<43.815400,0.000000,3.760100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<43.425600,0.000000,3.760100>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,0.000000,0> translate<43.425600,0.000000,3.760100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<43.425600,0.000000,3.760100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<43.230800,0.000000,3.565200>}
box{<0,0,-0.050800><0.275560,0.036000,0.050800> rotate<0,-45.011732,0> translate<43.230800,0.000000,3.565200> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.400100,0.000000,2.785600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.400100,0.000000,3.565200>}
box{<0,0,-0.050800><0.779600,0.036000,0.050800> rotate<0,90.000000,0> translate<44.400100,0.000000,3.565200> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.400100,0.000000,3.565200>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.594900,0.000000,3.760100>}
box{<0,0,-0.050800><0.275560,0.036000,0.050800> rotate<0,-45.011732,0> translate<44.400100,0.000000,3.565200> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.594900,0.000000,3.760100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.984700,0.000000,3.760100>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,0.000000,0> translate<44.594900,0.000000,3.760100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.984700,0.000000,3.760100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<45.179600,0.000000,3.565200>}
box{<0,0,-0.050800><0.275630,0.036000,0.050800> rotate<0,44.997030,0> translate<44.984700,0.000000,3.760100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<45.179600,0.000000,3.565200>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<45.179600,0.000000,2.785600>}
box{<0,0,-0.050800><0.779600,0.036000,0.050800> rotate<0,-90.000000,0> translate<45.179600,0.000000,2.785600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<45.179600,0.000000,2.785600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.984700,0.000000,2.590800>}
box{<0,0,-0.050800><0.275560,0.036000,0.050800> rotate<0,-44.982329,0> translate<44.984700,0.000000,2.590800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.984700,0.000000,2.590800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.594900,0.000000,2.590800>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,0.000000,0> translate<44.594900,0.000000,2.590800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.594900,0.000000,2.590800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.400100,0.000000,2.785600>}
box{<0,0,-0.050800><0.275489,0.036000,0.050800> rotate<0,44.997030,0> translate<44.400100,0.000000,2.785600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.400100,0.000000,2.785600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<45.179600,0.000000,3.565200>}
box{<0,0,-0.050800><1.102450,0.036000,0.050800> rotate<0,-45.000705,0> translate<44.400100,0.000000,2.785600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<45.569400,0.000000,3.370300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<45.959100,0.000000,3.760100>}
box{<0,0,-0.050800><0.551190,0.036000,0.050800> rotate<0,-45.004380,0> translate<45.569400,0.000000,3.370300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<45.959100,0.000000,3.760100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<45.959100,0.000000,2.590800>}
box{<0,0,-0.050800><1.169300,0.036000,0.050800> rotate<0,-90.000000,0> translate<45.959100,0.000000,2.590800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<45.569400,0.000000,2.590800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<46.348900,0.000000,2.590800>}
box{<0,0,-0.050800><0.779500,0.036000,0.050800> rotate<0,0.000000,0> translate<45.569400,0.000000,2.590800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<47.518200,0.000000,2.590800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<46.738700,0.000000,2.590800>}
box{<0,0,-0.050800><0.779500,0.036000,0.050800> rotate<0,0.000000,0> translate<46.738700,0.000000,2.590800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<46.738700,0.000000,2.590800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<47.518200,0.000000,3.370300>}
box{<0,0,-0.050800><1.102379,0.036000,0.050800> rotate<0,-44.997030,0> translate<46.738700,0.000000,2.590800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<47.518200,0.000000,3.370300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<47.518200,0.000000,3.565200>}
box{<0,0,-0.050800><0.194900,0.036000,0.050800> rotate<0,90.000000,0> translate<47.518200,0.000000,3.565200> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<47.518200,0.000000,3.565200>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<47.323300,0.000000,3.760100>}
box{<0,0,-0.050800><0.275630,0.036000,0.050800> rotate<0,44.997030,0> translate<47.323300,0.000000,3.760100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<47.323300,0.000000,3.760100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<46.933500,0.000000,3.760100>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,0.000000,0> translate<46.933500,0.000000,3.760100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<46.933500,0.000000,3.760100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<46.738700,0.000000,3.565200>}
box{<0,0,-0.050800><0.275560,0.036000,0.050800> rotate<0,-45.011732,0> translate<46.738700,0.000000,3.565200> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<38.150800,0.000000,5.910300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<38.540500,0.000000,6.300100>}
box{<0,0,-0.050800><0.551190,0.036000,0.050800> rotate<0,-45.004380,0> translate<38.150800,0.000000,5.910300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<38.540500,0.000000,6.300100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<38.540500,0.000000,5.130800>}
box{<0,0,-0.050800><1.169300,0.036000,0.050800> rotate<0,-90.000000,0> translate<38.540500,0.000000,5.130800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<38.150800,0.000000,5.130800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<38.930300,0.000000,5.130800>}
box{<0,0,-0.050800><0.779500,0.036000,0.050800> rotate<0,0.000000,0> translate<38.150800,0.000000,5.130800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<39.320100,0.000000,5.130800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<39.320100,0.000000,6.300100>}
box{<0,0,-0.050800><1.169300,0.036000,0.050800> rotate<0,90.000000,0> translate<39.320100,0.000000,6.300100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<39.320100,0.000000,6.300100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<39.709800,0.000000,5.910300>}
box{<0,0,-0.050800><0.551190,0.036000,0.050800> rotate<0,45.004380,0> translate<39.320100,0.000000,6.300100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<39.709800,0.000000,5.910300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<40.099600,0.000000,6.300100>}
box{<0,0,-0.050800><0.551260,0.036000,0.050800> rotate<0,-44.997030,0> translate<39.709800,0.000000,5.910300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<40.099600,0.000000,6.300100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<40.099600,0.000000,5.130800>}
box{<0,0,-0.050800><1.169300,0.036000,0.050800> rotate<0,-90.000000,0> translate<40.099600,0.000000,5.130800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<40.489400,0.000000,6.300100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<40.489400,0.000000,5.130800>}
box{<0,0,-0.050800><1.169300,0.036000,0.050800> rotate<0,-90.000000,0> translate<40.489400,0.000000,5.130800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<40.489400,0.000000,5.715400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<40.684200,0.000000,5.910300>}
box{<0,0,-0.050800><0.275560,0.036000,0.050800> rotate<0,-45.011732,0> translate<40.489400,0.000000,5.715400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<40.684200,0.000000,5.910300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<41.074000,0.000000,5.910300>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,0.000000,0> translate<40.684200,0.000000,5.910300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<41.074000,0.000000,5.910300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<41.268900,0.000000,5.715400>}
box{<0,0,-0.050800><0.275630,0.036000,0.050800> rotate<0,44.997030,0> translate<41.074000,0.000000,5.910300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<41.268900,0.000000,5.715400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<41.268900,0.000000,5.130800>}
box{<0,0,-0.050800><0.584600,0.036000,0.050800> rotate<0,-90.000000,0> translate<41.268900,0.000000,5.130800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<41.658700,0.000000,5.910300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<42.438200,0.000000,5.910300>}
box{<0,0,-0.050800><0.779500,0.036000,0.050800> rotate<0,0.000000,0> translate<41.658700,0.000000,5.910300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<42.438200,0.000000,5.910300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<41.658700,0.000000,5.130800>}
box{<0,0,-0.050800><1.102379,0.036000,0.050800> rotate<0,-44.997030,0> translate<41.658700,0.000000,5.130800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<41.658700,0.000000,5.130800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<42.438200,0.000000,5.130800>}
box{<0,0,-0.050800><0.779500,0.036000,0.050800> rotate<0,0.000000,0> translate<41.658700,0.000000,5.130800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<45.965600,0.000000,34.045200>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<45.965600,0.000000,33.265600>}
box{<0,0,-0.050800><0.779600,0.036000,0.050800> rotate<0,-90.000000,0> translate<45.965600,0.000000,33.265600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<45.965600,0.000000,33.265600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<46.160500,0.000000,33.070800>}
box{<0,0,-0.050800><0.275560,0.036000,0.050800> rotate<0,44.982329,0> translate<45.965600,0.000000,33.265600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<45.770800,0.000000,33.850300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<46.160500,0.000000,33.850300>}
box{<0,0,-0.050800><0.389700,0.036000,0.050800> rotate<0,0.000000,0> translate<45.770800,0.000000,33.850300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<46.550300,0.000000,33.850300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<47.329800,0.000000,33.070800>}
box{<0,0,-0.050800><1.102379,0.036000,0.050800> rotate<0,44.997030,0> translate<46.550300,0.000000,33.850300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<46.550300,0.000000,33.070800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<47.329800,0.000000,33.850300>}
box{<0,0,-0.050800><1.102379,0.036000,0.050800> rotate<0,-44.997030,0> translate<46.550300,0.000000,33.070800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<45.770800,0.000000,22.910800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<45.770800,0.000000,23.690300>}
box{<0,0,-0.050800><0.779500,0.036000,0.050800> rotate<0,90.000000,0> translate<45.770800,0.000000,23.690300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<45.770800,0.000000,23.300500>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<46.160500,0.000000,23.690300>}
box{<0,0,-0.050800><0.551190,0.036000,0.050800> rotate<0,-45.004380,0> translate<45.770800,0.000000,23.300500> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<46.160500,0.000000,23.690300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<46.355400,0.000000,23.690300>}
box{<0,0,-0.050800><0.194900,0.036000,0.050800> rotate<0,0.000000,0> translate<46.160500,0.000000,23.690300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<46.745200,0.000000,23.690300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<47.524700,0.000000,22.910800>}
box{<0,0,-0.050800><1.102379,0.036000,0.050800> rotate<0,44.997030,0> translate<46.745200,0.000000,23.690300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<46.745200,0.000000,22.910800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<47.524700,0.000000,23.690300>}
box{<0,0,-0.050800><1.102379,0.036000,0.050800> rotate<0,-44.997030,0> translate<46.745200,0.000000,22.910800> }
//C1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.461000,0.000000,11.430000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.779000,0.000000,11.430000>}
box{<0,0,-0.076200><4.318000,0.036000,0.076200> rotate<0,0.000000,0> translate<5.461000,0.000000,11.430000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.779000,0.000000,8.890000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.461000,0.000000,8.890000>}
box{<0,0,-0.076200><4.318000,0.036000,0.076200> rotate<0,0.000000,0> translate<5.461000,0.000000,8.890000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.033000,0.000000,11.176000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.033000,0.000000,9.144000>}
box{<0,0,-0.076200><2.032000,0.036000,0.076200> rotate<0,-90.000000,0> translate<10.033000,0.000000,9.144000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.207000,0.000000,11.176000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.207000,0.000000,9.144000>}
box{<0,0,-0.076200><2.032000,0.036000,0.076200> rotate<0,-90.000000,0> translate<5.207000,0.000000,9.144000> }
object{ARC(0.254000,0.152400,0.000000,90.000000,0.036000) translate<9.779000,0.000000,11.176000>}
object{ARC(0.254000,0.152400,90.000000,180.000000,0.036000) translate<5.461000,0.000000,11.176000>}
object{ARC(0.254000,0.152400,270.000000,360.000000,0.036000) translate<9.779000,0.000000,9.144000>}
object{ARC(0.254000,0.152400,180.000000,270.000000,0.036000) translate<5.461000,0.000000,9.144000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.890000,0.000000,10.160000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.001000,0.000000,10.160000>}
box{<0,0,-0.076200><0.889000,0.036000,0.076200> rotate<0,0.000000,0> translate<8.001000,0.000000,10.160000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.001000,0.000000,10.160000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.874000,0.000000,10.160000>}
box{<0,0,-0.076200><0.127000,0.036000,0.076200> rotate<0,0.000000,0> translate<7.874000,0.000000,10.160000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<7.874000,0.000000,10.160000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<7.874000,0.000000,10.922000>}
box{<0,0,-0.127000><0.762000,0.036000,0.127000> rotate<0,90.000000,0> translate<7.874000,0.000000,10.922000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<7.874000,0.000000,10.160000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<7.874000,0.000000,9.398000>}
box{<0,0,-0.127000><0.762000,0.036000,0.127000> rotate<0,-90.000000,0> translate<7.874000,0.000000,9.398000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<7.366000,0.000000,10.922000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<7.366000,0.000000,10.160000>}
box{<0,0,-0.127000><0.762000,0.036000,0.127000> rotate<0,-90.000000,0> translate<7.366000,0.000000,10.160000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<7.366000,0.000000,10.160000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<7.366000,0.000000,9.398000>}
box{<0,0,-0.127000><0.762000,0.036000,0.127000> rotate<0,-90.000000,0> translate<7.366000,0.000000,9.398000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.366000,0.000000,10.160000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.239000,0.000000,10.160000>}
box{<0,0,-0.076200><0.127000,0.036000,0.076200> rotate<0,0.000000,0> translate<7.239000,0.000000,10.160000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.239000,0.000000,10.160000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.350000,0.000000,10.160000>}
box{<0,0,-0.076200><0.889000,0.036000,0.076200> rotate<0,0.000000,0> translate<6.350000,0.000000,10.160000> }
//C2 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.461000,0.000000,16.510000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.779000,0.000000,16.510000>}
box{<0,0,-0.076200><4.318000,0.036000,0.076200> rotate<0,0.000000,0> translate<5.461000,0.000000,16.510000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.779000,0.000000,13.970000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.461000,0.000000,13.970000>}
box{<0,0,-0.076200><4.318000,0.036000,0.076200> rotate<0,0.000000,0> translate<5.461000,0.000000,13.970000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.033000,0.000000,16.256000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.033000,0.000000,14.224000>}
box{<0,0,-0.076200><2.032000,0.036000,0.076200> rotate<0,-90.000000,0> translate<10.033000,0.000000,14.224000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.207000,0.000000,16.256000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.207000,0.000000,14.224000>}
box{<0,0,-0.076200><2.032000,0.036000,0.076200> rotate<0,-90.000000,0> translate<5.207000,0.000000,14.224000> }
object{ARC(0.254000,0.152400,0.000000,90.000000,0.036000) translate<9.779000,0.000000,16.256000>}
object{ARC(0.254000,0.152400,90.000000,180.000000,0.036000) translate<5.461000,0.000000,16.256000>}
object{ARC(0.254000,0.152400,270.000000,360.000000,0.036000) translate<9.779000,0.000000,14.224000>}
object{ARC(0.254000,0.152400,180.000000,270.000000,0.036000) translate<5.461000,0.000000,14.224000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.890000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.001000,0.000000,15.240000>}
box{<0,0,-0.076200><0.889000,0.036000,0.076200> rotate<0,0.000000,0> translate<8.001000,0.000000,15.240000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.001000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.874000,0.000000,15.240000>}
box{<0,0,-0.076200><0.127000,0.036000,0.076200> rotate<0,0.000000,0> translate<7.874000,0.000000,15.240000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<7.874000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<7.874000,0.000000,16.002000>}
box{<0,0,-0.127000><0.762000,0.036000,0.127000> rotate<0,90.000000,0> translate<7.874000,0.000000,16.002000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<7.874000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<7.874000,0.000000,14.478000>}
box{<0,0,-0.127000><0.762000,0.036000,0.127000> rotate<0,-90.000000,0> translate<7.874000,0.000000,14.478000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<7.366000,0.000000,16.002000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<7.366000,0.000000,15.240000>}
box{<0,0,-0.127000><0.762000,0.036000,0.127000> rotate<0,-90.000000,0> translate<7.366000,0.000000,15.240000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<7.366000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<7.366000,0.000000,14.478000>}
box{<0,0,-0.127000><0.762000,0.036000,0.127000> rotate<0,-90.000000,0> translate<7.366000,0.000000,14.478000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.366000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.239000,0.000000,15.240000>}
box{<0,0,-0.076200><0.127000,0.036000,0.076200> rotate<0,0.000000,0> translate<7.239000,0.000000,15.240000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.239000,0.000000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.350000,0.000000,15.240000>}
box{<0,0,-0.076200><0.889000,0.036000,0.076200> rotate<0,0.000000,0> translate<6.350000,0.000000,15.240000> }
//IC1 silk screen
object{ARC(0.635000,0.152400,270.000000,450.000000,0.036000) translate<17.780000,0.000000,12.700000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.780000,0.000000,12.065000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.780000,0.000000,9.906000>}
box{<0,0,-0.076200><2.159000,0.036000,0.076200> rotate<0,-90.000000,0> translate<17.780000,0.000000,9.906000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.780000,0.000000,9.906000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<27.940000,0.000000,9.906000>}
box{<0,0,-0.076200><10.160000,0.036000,0.076200> rotate<0,0.000000,0> translate<17.780000,0.000000,9.906000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<27.940000,0.000000,9.906000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<27.940000,0.000000,15.494000>}
box{<0,0,-0.076200><5.588000,0.036000,0.076200> rotate<0,90.000000,0> translate<27.940000,0.000000,15.494000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<27.940000,0.000000,15.494000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.780000,0.000000,15.494000>}
box{<0,0,-0.076200><10.160000,0.036000,0.076200> rotate<0,0.000000,0> translate<17.780000,0.000000,15.494000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.780000,0.000000,15.494000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.780000,0.000000,13.335000>}
box{<0,0,-0.076200><2.159000,0.036000,0.076200> rotate<0,-90.000000,0> translate<17.780000,0.000000,13.335000> }
//LED1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<43.180000,0.000000,33.655000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<43.180000,0.000000,37.465000>}
box{<0,0,-0.101600><3.810000,0.036000,0.101600> rotate<0,90.000000,0> translate<43.180000,0.000000,37.465000> }
object{ARC(3.175000,0.254000,36.869898,323.130102,0.036000) translate<40.640000,0.000000,35.560000>}
object{ARC(1.143000,0.152400,90.000000,180.000000,0.036000) translate<40.640000,0.000000,35.560000>}
object{ARC(1.143000,0.152400,270.000000,360.000000,0.036000) translate<40.640000,0.000000,35.560000>}
object{ARC(1.651000,0.152400,90.000000,180.000000,0.036000) translate<40.640000,0.000000,35.560000>}
object{ARC(1.651000,0.152400,270.000000,360.000000,0.036000) translate<40.640000,0.000000,35.560000>}
object{ARC(2.159000,0.152400,90.000000,180.000000,0.036000) translate<40.640000,0.000000,35.560000>}
object{ARC(2.159000,0.152400,270.000000,360.000000,0.036000) translate<40.640000,0.000000,35.560000>}
difference{
cylinder{<40.640000,0,35.560000><40.640000,0.036000,35.560000>2.616200 translate<0,0.000000,0>}
cylinder{<40.640000,-0.1,35.560000><40.640000,0.135000,35.560000>2.463800 translate<0,0.000000,0>}}
//LED2 silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<43.180000,0.000000,23.495000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<43.180000,0.000000,27.305000>}
box{<0,0,-0.101600><3.810000,0.036000,0.101600> rotate<0,90.000000,0> translate<43.180000,0.000000,27.305000> }
object{ARC(3.175000,0.254000,36.869898,323.130102,0.036000) translate<40.640000,0.000000,25.400000>}
object{ARC(1.143000,0.152400,90.000000,180.000000,0.036000) translate<40.640000,0.000000,25.400000>}
object{ARC(1.143000,0.152400,270.000000,360.000000,0.036000) translate<40.640000,0.000000,25.400000>}
object{ARC(1.651000,0.152400,90.000000,180.000000,0.036000) translate<40.640000,0.000000,25.400000>}
object{ARC(1.651000,0.152400,270.000000,360.000000,0.036000) translate<40.640000,0.000000,25.400000>}
object{ARC(2.159000,0.152400,90.000000,180.000000,0.036000) translate<40.640000,0.000000,25.400000>}
object{ARC(2.159000,0.152400,270.000000,360.000000,0.036000) translate<40.640000,0.000000,25.400000>}
difference{
cylinder{<40.640000,0,25.400000><40.640000,0.036000,25.400000>2.616200 translate<0,0.000000,0>}
cylinder{<40.640000,-0.1,25.400000><40.640000,0.135000,25.400000>2.463800 translate<0,0.000000,0>}}
//Q1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<40.132000,0.000000,10.541000>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<46.228000,0.000000,10.541000>}
box{<0,0,-0.203200><6.096000,0.036000,0.203200> rotate<0,0.000000,0> translate<40.132000,0.000000,10.541000> }
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<40.132000,0.000000,14.859000>}
cylinder{<0,0,0><0,0.036000,0>0.203200 translate<46.228000,0.000000,14.859000>}
box{<0,0,-0.203200><6.096000,0.036000,0.203200> rotate<0,0.000000,0> translate<40.132000,0.000000,14.859000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<40.132000,0.000000,11.049000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.228000,0.000000,11.049000>}
box{<0,0,-0.076200><6.096000,0.036000,0.076200> rotate<0,0.000000,0> translate<40.132000,0.000000,11.049000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.228000,0.000000,14.351000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<40.132000,0.000000,14.351000>}
box{<0,0,-0.076200><6.096000,0.036000,0.076200> rotate<0,0.000000,0> translate<40.132000,0.000000,14.351000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<42.926000,0.000000,13.462000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<43.434000,0.000000,13.462000>}
box{<0,0,-0.076200><0.508000,0.036000,0.076200> rotate<0,0.000000,0> translate<42.926000,0.000000,13.462000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<43.434000,0.000000,13.462000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<43.434000,0.000000,11.938000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,-90.000000,0> translate<43.434000,0.000000,11.938000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<43.434000,0.000000,11.938000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<42.926000,0.000000,11.938000>}
box{<0,0,-0.076200><0.508000,0.036000,0.076200> rotate<0,0.000000,0> translate<42.926000,0.000000,11.938000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<42.926000,0.000000,11.938000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<42.926000,0.000000,13.462000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,90.000000,0> translate<42.926000,0.000000,13.462000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<43.815000,0.000000,13.462000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<43.815000,0.000000,12.700000>}
box{<0,0,-0.076200><0.762000,0.036000,0.076200> rotate<0,-90.000000,0> translate<43.815000,0.000000,12.700000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<43.815000,0.000000,12.700000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<43.815000,0.000000,11.938000>}
box{<0,0,-0.076200><0.762000,0.036000,0.076200> rotate<0,-90.000000,0> translate<43.815000,0.000000,11.938000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<42.545000,0.000000,13.462000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<42.545000,0.000000,12.700000>}
box{<0,0,-0.076200><0.762000,0.036000,0.076200> rotate<0,-90.000000,0> translate<42.545000,0.000000,12.700000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<42.545000,0.000000,12.700000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<42.545000,0.000000,11.938000>}
box{<0,0,-0.076200><0.762000,0.036000,0.076200> rotate<0,-90.000000,0> translate<42.545000,0.000000,11.938000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<43.815000,0.000000,12.700000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<44.450000,0.000000,12.700000>}
box{<0,0,-0.076200><0.635000,0.036000,0.076200> rotate<0,0.000000,0> translate<43.815000,0.000000,12.700000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<42.545000,0.000000,12.700000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<41.910000,0.000000,12.700000>}
box{<0,0,-0.076200><0.635000,0.036000,0.076200> rotate<0,0.000000,0> translate<41.910000,0.000000,12.700000> }
object{ARC(2.159000,0.406400,90.000000,270.000000,0.036000) translate<40.132000,0.000000,12.700000>}
object{ARC(2.159000,0.406400,270.000000,450.000000,0.036000) translate<46.228000,0.000000,12.700000>}
object{ARC(1.651000,0.152400,90.000000,270.000000,0.036000) translate<40.132000,0.000000,12.700000>}
object{ARC(1.651000,0.152400,270.000000,450.000000,0.036000) translate<46.228000,0.000000,12.700000>}
//R1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.455000,0.000000,20.785000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<28.290000,0.000000,20.785000>}
box{<0,0,-0.076200><10.835000,0.036000,0.076200> rotate<0,0.000000,0> translate<17.455000,0.000000,20.785000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<28.290000,0.000000,24.935000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.455000,0.000000,24.935000>}
box{<0,0,-0.076200><10.835000,0.036000,0.076200> rotate<0,0.000000,0> translate<17.455000,0.000000,24.935000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.455000,0.000000,24.935000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.455000,0.000000,20.785000>}
box{<0,0,-0.076200><4.150000,0.036000,0.076200> rotate<0,-90.000000,0> translate<17.455000,0.000000,20.785000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<28.290000,0.000000,20.785000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<28.290000,0.000000,24.935000>}
box{<0,0,-0.076200><4.150000,0.036000,0.076200> rotate<0,90.000000,0> translate<28.290000,0.000000,24.935000> }
cylinder{<0,0,0><0,0.036000,0>0.406500 translate<13.960000,0.000000,22.860000>}
cylinder{<0,0,0><0,0.036000,0>0.406500 translate<15.240000,0.000000,22.860000>}
box{<0,0,-0.406500><1.280000,0.036000,0.406500> rotate<0,0.000000,0> translate<13.960000,0.000000,22.860000> }
cylinder{<0,0,0><0,0.036000,0>0.406500 translate<30.480000,0.000000,22.860000>}
cylinder{<0,0,0><0,0.036000,0>0.406500 translate<31.760000,0.000000,22.860000>}
box{<0,0,-0.406500><1.280000,0.036000,0.406500> rotate<0,0.000000,0> translate<30.480000,0.000000,22.860000> }
box{<-1.070000,0,-0.406400><1.070000,0.036000,0.406400> rotate<0,-0.000000,0> translate<16.310000,0.000000,22.860000>}
box{<-1.057500,0,-0.406400><1.057500,0.036000,0.406400> rotate<0,-0.000000,0> translate<29.422500,0.000000,22.860000>}
//R2 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.455000,0.000000,28.405000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<28.290000,0.000000,28.405000>}
box{<0,0,-0.076200><10.835000,0.036000,0.076200> rotate<0,0.000000,0> translate<17.455000,0.000000,28.405000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<28.290000,0.000000,32.555000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.455000,0.000000,32.555000>}
box{<0,0,-0.076200><10.835000,0.036000,0.076200> rotate<0,0.000000,0> translate<17.455000,0.000000,32.555000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.455000,0.000000,32.555000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.455000,0.000000,28.405000>}
box{<0,0,-0.076200><4.150000,0.036000,0.076200> rotate<0,-90.000000,0> translate<17.455000,0.000000,28.405000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<28.290000,0.000000,28.405000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<28.290000,0.000000,32.555000>}
box{<0,0,-0.076200><4.150000,0.036000,0.076200> rotate<0,90.000000,0> translate<28.290000,0.000000,32.555000> }
cylinder{<0,0,0><0,0.036000,0>0.406500 translate<13.960000,0.000000,30.480000>}
cylinder{<0,0,0><0,0.036000,0>0.406500 translate<15.240000,0.000000,30.480000>}
box{<0,0,-0.406500><1.280000,0.036000,0.406500> rotate<0,0.000000,0> translate<13.960000,0.000000,30.480000> }
cylinder{<0,0,0><0,0.036000,0>0.406500 translate<30.480000,0.000000,30.480000>}
cylinder{<0,0,0><0,0.036000,0>0.406500 translate<31.760000,0.000000,30.480000>}
box{<0,0,-0.406500><1.280000,0.036000,0.406500> rotate<0,0.000000,0> translate<30.480000,0.000000,30.480000> }
box{<-1.070000,0,-0.406400><1.070000,0.036000,0.406400> rotate<0,-0.000000,0> translate<16.310000,0.000000,30.480000>}
box{<-1.057500,0,-0.406400><1.057500,0.036000,0.406400> rotate<0,-0.000000,0> translate<29.422500,0.000000,30.480000>}
//R3 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.455000,0.000000,36.025000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<28.290000,0.000000,36.025000>}
box{<0,0,-0.076200><10.835000,0.036000,0.076200> rotate<0,0.000000,0> translate<17.455000,0.000000,36.025000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<28.290000,0.000000,40.175000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.455000,0.000000,40.175000>}
box{<0,0,-0.076200><10.835000,0.036000,0.076200> rotate<0,0.000000,0> translate<17.455000,0.000000,40.175000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.455000,0.000000,40.175000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.455000,0.000000,36.025000>}
box{<0,0,-0.076200><4.150000,0.036000,0.076200> rotate<0,-90.000000,0> translate<17.455000,0.000000,36.025000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<28.290000,0.000000,36.025000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<28.290000,0.000000,40.175000>}
box{<0,0,-0.076200><4.150000,0.036000,0.076200> rotate<0,90.000000,0> translate<28.290000,0.000000,40.175000> }
cylinder{<0,0,0><0,0.036000,0>0.406500 translate<13.960000,0.000000,38.100000>}
cylinder{<0,0,0><0,0.036000,0>0.406500 translate<15.240000,0.000000,38.100000>}
box{<0,0,-0.406500><1.280000,0.036000,0.406500> rotate<0,0.000000,0> translate<13.960000,0.000000,38.100000> }
cylinder{<0,0,0><0,0.036000,0>0.406500 translate<30.480000,0.000000,38.100000>}
cylinder{<0,0,0><0,0.036000,0>0.406500 translate<31.760000,0.000000,38.100000>}
box{<0,0,-0.406500><1.280000,0.036000,0.406500> rotate<0,0.000000,0> translate<30.480000,0.000000,38.100000> }
box{<-1.070000,0,-0.406400><1.070000,0.036000,0.406400> rotate<0,-0.000000,0> translate<16.310000,0.000000,38.100000>}
box{<-1.057500,0,-0.406400><1.057500,0.036000,0.406400> rotate<0,-0.000000,0> translate<29.422500,0.000000,38.100000>}
//S1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.382000,0.000000,29.718000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.128000,0.000000,29.718000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,0.000000,0> translate<8.128000,0.000000,29.718000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.382000,0.000000,29.718000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.382000,0.000000,31.242000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,90.000000,0> translate<8.382000,0.000000,31.242000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.128000,0.000000,31.242000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.382000,0.000000,31.242000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,0.000000,0> translate<8.128000,0.000000,31.242000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.128000,0.000000,31.496000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.128000,0.000000,33.020000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,90.000000,0> translate<8.128000,0.000000,33.020000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.778000,0.000000,31.242000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.032000,0.000000,31.242000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,0.000000,0> translate<1.778000,0.000000,31.242000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.778000,0.000000,31.242000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.778000,0.000000,29.718000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,-90.000000,0> translate<1.778000,0.000000,29.718000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.032000,0.000000,29.718000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.778000,0.000000,29.718000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,0.000000,0> translate<1.778000,0.000000,29.718000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.128000,0.000000,33.020000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.620000,0.000000,33.528000>}
box{<0,0,-0.076200><0.718420,0.036000,0.076200> rotate<0,44.997030,0> translate<7.620000,0.000000,33.528000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.620000,0.000000,27.432000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.128000,0.000000,27.940000>}
box{<0,0,-0.076200><0.718420,0.036000,0.076200> rotate<0,-44.997030,0> translate<7.620000,0.000000,27.432000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.128000,0.000000,27.940000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.128000,0.000000,29.464000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,90.000000,0> translate<8.128000,0.000000,29.464000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.540000,0.000000,33.528000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.032000,0.000000,33.020000>}
box{<0,0,-0.076200><0.718420,0.036000,0.076200> rotate<0,-44.997030,0> translate<2.032000,0.000000,33.020000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.032000,0.000000,33.020000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.032000,0.000000,31.496000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,-90.000000,0> translate<2.032000,0.000000,31.496000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.540000,0.000000,27.432000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.032000,0.000000,27.940000>}
box{<0,0,-0.076200><0.718420,0.036000,0.076200> rotate<0,44.997030,0> translate<2.032000,0.000000,27.940000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.032000,0.000000,27.940000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.032000,0.000000,29.464000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,90.000000,0> translate<2.032000,0.000000,29.464000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<3.810000,0.000000,31.750000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<3.810000,0.000000,29.210000>}
box{<0,0,-0.025400><2.540000,0.036000,0.025400> rotate<0,-90.000000,0> translate<3.810000,0.000000,29.210000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<6.350000,0.000000,29.210000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<3.810000,0.000000,29.210000>}
box{<0,0,-0.025400><2.540000,0.036000,0.025400> rotate<0,0.000000,0> translate<3.810000,0.000000,29.210000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<6.350000,0.000000,29.210000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<6.350000,0.000000,31.750000>}
box{<0,0,-0.025400><2.540000,0.036000,0.025400> rotate<0,90.000000,0> translate<6.350000,0.000000,31.750000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<3.810000,0.000000,31.750000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<6.350000,0.000000,31.750000>}
box{<0,0,-0.025400><2.540000,0.036000,0.025400> rotate<0,0.000000,0> translate<3.810000,0.000000,31.750000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<3.810000,0.000000,33.528000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<3.810000,0.000000,33.274000>}
box{<0,0,-0.025400><0.254000,0.036000,0.025400> rotate<0,-90.000000,0> translate<3.810000,0.000000,33.274000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<6.350000,0.000000,33.274000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<3.810000,0.000000,33.274000>}
box{<0,0,-0.025400><2.540000,0.036000,0.025400> rotate<0,0.000000,0> translate<3.810000,0.000000,33.274000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<6.350000,0.000000,33.274000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<6.350000,0.000000,33.528000>}
box{<0,0,-0.025400><0.254000,0.036000,0.025400> rotate<0,90.000000,0> translate<6.350000,0.000000,33.528000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<6.223000,0.000000,27.686000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<3.810000,0.000000,27.686000>}
box{<0,0,-0.025400><2.413000,0.036000,0.025400> rotate<0,0.000000,0> translate<3.810000,0.000000,27.686000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<6.223000,0.000000,27.686000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<6.223000,0.000000,27.432000>}
box{<0,0,-0.025400><0.254000,0.036000,0.025400> rotate<0,-90.000000,0> translate<6.223000,0.000000,27.432000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<3.810000,0.000000,27.686000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<3.810000,0.000000,27.432000>}
box{<0,0,-0.025400><0.254000,0.036000,0.025400> rotate<0,-90.000000,0> translate<3.810000,0.000000,27.432000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.620000,0.000000,27.432000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.239000,0.000000,27.432000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,0.000000,0> translate<7.239000,0.000000,27.432000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.540000,0.000000,27.432000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.921000,0.000000,27.432000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,0.000000,0> translate<2.540000,0.000000,27.432000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.921000,0.000000,27.432000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.810000,0.000000,27.432000>}
box{<0,0,-0.076200><0.889000,0.036000,0.076200> rotate<0,0.000000,0> translate<2.921000,0.000000,27.432000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.540000,0.000000,33.528000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.921000,0.000000,33.528000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,0.000000,0> translate<2.540000,0.000000,33.528000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.620000,0.000000,33.528000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.239000,0.000000,33.528000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,0.000000,0> translate<7.239000,0.000000,33.528000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.239000,0.000000,33.528000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.350000,0.000000,33.528000>}
box{<0,0,-0.076200><0.889000,0.036000,0.076200> rotate<0,0.000000,0> translate<6.350000,0.000000,33.528000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.350000,0.000000,33.528000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.810000,0.000000,33.528000>}
box{<0,0,-0.076200><2.540000,0.036000,0.076200> rotate<0,0.000000,0> translate<3.810000,0.000000,33.528000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.810000,0.000000,33.528000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.921000,0.000000,33.528000>}
box{<0,0,-0.076200><0.889000,0.036000,0.076200> rotate<0,0.000000,0> translate<2.921000,0.000000,33.528000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.810000,0.000000,27.432000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.223000,0.000000,27.432000>}
box{<0,0,-0.076200><2.413000,0.036000,0.076200> rotate<0,0.000000,0> translate<3.810000,0.000000,27.432000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.223000,0.000000,27.432000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.239000,0.000000,27.432000>}
box{<0,0,-0.076200><1.016000,0.036000,0.076200> rotate<0,0.000000,0> translate<6.223000,0.000000,27.432000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.128000,0.000000,29.718000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.128000,0.000000,29.464000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,-90.000000,0> translate<8.128000,0.000000,29.464000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.128000,0.000000,31.242000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.128000,0.000000,31.496000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,90.000000,0> translate<8.128000,0.000000,31.496000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.032000,0.000000,29.718000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.032000,0.000000,29.464000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,-90.000000,0> translate<2.032000,0.000000,29.464000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.032000,0.000000,31.242000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.032000,0.000000,31.496000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,90.000000,0> translate<2.032000,0.000000,31.496000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.810000,0.000000,28.321000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.350000,0.000000,28.321000>}
box{<0,0,-0.076200><2.540000,0.036000,0.076200> rotate<0,0.000000,0> translate<3.810000,0.000000,28.321000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.350000,0.000000,32.766000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.810000,0.000000,32.766000>}
box{<0,0,-0.076200><2.540000,0.036000,0.076200> rotate<0,0.000000,0> translate<3.810000,0.000000,32.766000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.667000,0.000000,31.750000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.667000,0.000000,30.988000>}
box{<0,0,-0.076200><0.762000,0.036000,0.076200> rotate<0,-90.000000,0> translate<2.667000,0.000000,30.988000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.667000,0.000000,29.972000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.667000,0.000000,29.210000>}
box{<0,0,-0.076200><0.762000,0.036000,0.076200> rotate<0,-90.000000,0> translate<2.667000,0.000000,29.210000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.667000,0.000000,30.988000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.921000,0.000000,30.099000>}
box{<0,0,-0.076200><0.924574,0.036000,0.076200> rotate<0,74.049717,0> translate<2.667000,0.000000,30.988000> }
difference{
cylinder{<5.080000,0,30.480000><5.080000,0.036000,30.480000>1.854200 translate<0,0.000000,0>}
cylinder{<5.080000,-0.1,30.480000><5.080000,0.135000,30.480000>1.701800 translate<0,0.000000,0>}}
difference{
cylinder{<2.921000,0,28.321000><2.921000,0.036000,28.321000>0.584200 translate<0,0.000000,0>}
cylinder{<2.921000,-0.1,28.321000><2.921000,0.135000,28.321000>0.431800 translate<0,0.000000,0>}}
difference{
cylinder{<7.239000,0,28.448000><7.239000,0.036000,28.448000>0.584200 translate<0,0.000000,0>}
cylinder{<7.239000,-0.1,28.448000><7.239000,0.135000,28.448000>0.431800 translate<0,0.000000,0>}}
difference{
cylinder{<7.239000,0,32.639000><7.239000,0.036000,32.639000>0.584200 translate<0,0.000000,0>}
cylinder{<7.239000,-0.1,32.639000><7.239000,0.135000,32.639000>0.431800 translate<0,0.000000,0>}}
difference{
cylinder{<2.921000,0,32.639000><2.921000,0.036000,32.639000>0.584200 translate<0,0.000000,0>}
cylinder{<2.921000,-0.1,32.639000><2.921000,0.135000,32.639000>0.431800 translate<0,0.000000,0>}}
difference{
cylinder{<5.080000,0,30.480000><5.080000,0.036000,30.480000>0.660400 translate<0,0.000000,0>}
cylinder{<5.080000,-0.1,30.480000><5.080000,0.135000,30.480000>0.609600 translate<0,0.000000,0>}}
difference{
cylinder{<5.080000,0,30.480000><5.080000,0.036000,30.480000>0.330200 translate<0,0.000000,0>}
cylinder{<5.080000,-0.1,30.480000><5.080000,0.135000,30.480000>0.177800 translate<0,0.000000,0>}}
texture{col_slk}
}
#end
translate<mac_x_ver,mac_y_ver,mac_z_ver>
rotate<mac_x_rot,mac_y_rot,mac_z_rot>
}//End union
#end

#if(use_file_as_inc=off)
object{  TINY13_TEST(-27.940000,0,-21.590000,pcb_rotate_x,pcb_rotate_y,pcb_rotate_z)
#if(pcb_upsidedown=on)
rotate pcb_rotdir*180
#end
}
#end


//Parts not found in 3dpack.dat or 3dusrpac.dat are:
//R1	10K	AC01
